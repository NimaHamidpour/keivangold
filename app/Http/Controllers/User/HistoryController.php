<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\FinancialHistory;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=User::firstWhere('id',Auth::user()->id);

        $response['page_title']='تاریخچه مالی';

        $response['wallet'] = Auth::user()->wallet;

        $response['history']=FinancialHistory::where([['user',Auth::user()->id],['key','wallet']])->orderBy('id','DESC')->paginate(15);

        return view('user.page.history.list',compact('response'));
    }




}
