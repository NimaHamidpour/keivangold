<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\User;
use App\Models\UserAgency;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // EDIT NAME AND MOBILE AND AVATAR
    public function edit()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=User::firstWhere('id',Auth::user()->id);

        $response['page_title']='ویرایش مشخصات';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;

        return view('user.page.user.edit',compact('response'));
    }

    public function update(Request $request)
    {

        $user=User::firstWhere('id',Auth::user()->id);

        $request->validate([
            'name' => ['required', 'string','max:255'],
            'codemeli' => ['required', 'numeric', 'digits:10', 'unique:users,codemeli,'.$user->id],
            'mobile' => ['required', 'numeric', 'digits:11', 'unique:users,mobile,'.$user->id],
            'tell' => ['required', 'numeric', 'digits:11', 'unique:users,tell,'.$user->id],
            'bankcard' => ['required', 'numeric', 'digits:16', 'unique:users,bankcard,'.$user->id],
            'sheba' => ['required', 'unique:users,sheba,'.$user->id],
            'bank' => ['required'],

        ]);


        if (Auth::user()->active !== 'yes')
        {
            if ($request->hasFile('kartmeli')) {

                $image = $request->file('kartmeli');
                if(filesize($image) / 1000 > 1000 )
                {
                    return redirect()->back()->with('success','سایز کارت ملی بیشتر از حد مجاز می باشد.');
                }
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                // $destinationPath = public_path('storage/user/kartmeli/');
                $destinationPath = 'storage/user/kartmeli/';
                $img = Image::make($image->getRealPath());
                $img->resize(600,400)->save($destinationPath. $input['imagename']);

                $kartmeli='storage/user/kartmeli/'.$input['imagename'];
                $user->kartmeli=$kartmeli;

            }


            $user->name             =   $request->name;
            $user->codemeli         =   $request->codemeli;
            $user->mobile           =   $request->mobile;
            $user->sheba            =   $request->sheba;
            $user->bankcard         =   $request->bankcard;
            $user->tell             =   $request->tell;
            $user->bank             =   $request->bank;
            $user->active           =   'waiting';
            $user->friendShip       =   $request->friendShip;
            $user->timestamps       =   false;
            try
            {
                $user->save();
            }
            catch (Exception $exception)
            {
                return redirect()->back()->with('warning',$exception->getCode());
            }

            return redirect()->back()->with('success','ویرایش با موفقیت انجام شد.');
        }
        else
        {
            return redirect()->back()->with('success','اطلاعات شما قابل ویرایش نمی باشد.');
        }


    }


    // EDIT PASSWORD
    public function edit_passwrod()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='ویرایش کلمه عبور';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        return view('user.page.user.change-password',compact('response'));
    }

    public function update_passwrod(Request $request)
    {
        $request->validate([
            'old_password'=>'required',
            'password'=>'required|confirmed',
        ]);

        if (!Hash::check($request->old_password,Auth::user()->password))
        {
            return redirect()->back()->with('warning','کلمه عبور قبلی اشتباه می باشد.لطفا دوباره تلاش کنید');
        }

        $user=User::firstWhere('id',Auth::user()->id);

        $user->password     = Hash::make($request->password);
        $user->timestamps   =   false;

        $user->save();

        return redirect()->route('logout')->with('کلمه عبور با موفقیت تغییر یافت');
    }
}
