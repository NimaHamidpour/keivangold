<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\Visit;
use Illuminate\Support\Facades\Auth;


class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;

        $response['page_title']='لیست اطلاعیه ها';
        $response['notification']=Notification::orderBy('id','DESC')->paginate(10);

        return view('user.page.notification.list',compact('response'));
    }


    public function show(Notification $notification)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        
        $response['wallet']=Auth::user()->wallet;

        $response['page_title']='اطلاعیه : '.$notification->title;
        $response['notification']=$notification;


        // VISITS
        $visit = new Visit();
        $visit->ip = Auth::user()->id;
        $visit->useragent = \Request::header('User-Agent');
        $visit->session_id =\Request::getSession()->getId();
        $visit->url = \Request::url();
        $visit->model ='Notification';
        $visit->model_id = $response['notification']->id;
        $visit->save();

        return view('user.page.notification.show',compact('response'));
    }

}
