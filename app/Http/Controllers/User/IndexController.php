<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Visit;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;


        $response['page_title'] = 'داشبورد';


        $response['wallet'] = Auth::user()->wallet;

        $response['unread_message_count'] = Ticket::where([['status', 'user-answer'], ['user', Auth::user()->id]])->get()->count();

        $notification = Visit::where([['model', 'Notification'], ['ip', Auth::user()->id]])->get()->pluck('model_id');

        $response['notification_count'] = Notification::whereNotIn('id', $notification)->count();


        return view('user.page.dashboard.dashboard', compact('response'));
    }
}
