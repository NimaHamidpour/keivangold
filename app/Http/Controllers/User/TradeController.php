<?php
namespace App\Http\Controllers\User;

use App\CustomClass\Financial;
use App\Http\Controllers\Controller;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Trade;
use App\Models\TradeList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TradeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /*  نمایش لیست خرید و فروش جهت انجام معاملات لحظه ای  */
    public function tradeList()
    {
        /*  ساعت باز و بسته شدن معاملات  */
        $trade_start_time = (int)Setting::firstWhere('key','trade_start_time')->value;
        $trade_end_time = (int)Setting::firstWhere('key','trade_end_time')->value;

        /*  بررسی زمان معامله و باز و بسته بودن بازار  */
        $hour = jdate()->format('H');
        $trade_close = optional(Setting::firstWhere('key', 'trade_close'))->value;
        $portfo = Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);


        if ($portfo !== null)
        {

            if ($trade_close === 'no')
            {
                if ($hour >= $trade_start_time && $hour < $trade_end_time)
                {

                    $response = [];
                    $response['portfo'] = $portfo;
                    $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
                    $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
                    $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

                    // AMOUNT CAN USE
                    $response['wallet'] = Auth::user()->wallet;


                    $response['portfo'] = Portfo::firstWhere([['end_date',null],['user',Auth::user()->id]]);

                    $response['page_title'] = 'معاملات در جریان';


                    /* لیست خریداران  */
                    $response['buyer'] = TradeList::where([['active', 'yes'], ['type', 'buyer'], ['weight', '>', 0]])
                        ->orderBy('amount', 'DESC')->limit(5)->get();

                    /* لیست فروشندگان  */
                    $response['seller'] = TradeList::where([['active', 'yes'], ['type', 'seller'], ['weight', '>', 0]])
                        ->orderBy('amount', 'ASC')->limit(5)->get();


                    $response['trade_last_amount']              = Setting::firstWhere('key','trade_last_amount')->value;
                    $response['trade_online_amount']            = Setting::firstWhere('key','trade_online_amount')->value;
                    $response['trade_active_online_api_show']   = Setting::firstWhere('key','trade_active_online_api_show')->value;



                    if ($response['portfo'] !== null)
                    {
                        $response['last_offer'] = TradeList::where('portfo', $response['portfo']->id)->orderBy('id', 'DESC')->first();
                    }


                    return view('user.page.trade.trade-list', compact('response'));
                }
                else
                {
                    return redirect()->route('user.dashboard')->with('warning', 'ساعت کار بازار به پایان رسیده است.');
                }
            }
            else
            {
                return redirect()->route('user.dashboard')->with('warning', 'در حال حاضر بازار تعطیل می باشد و معامله ای انجام نمیشود.');
            }
        }
        else
        {
            return redirect()->back()->with('warning', 'ابتدا باید یک پرتفو فعال جهت انجام معاملات باز کنید');
        }

    }



    /*  لیست معاملات انجام شده هر شخص بر اسا شماره پرتفو  */
    public function index(Portfo $portfo)
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;

        $response['page_title'] = 'لیست معاملات پرتفو شماره : ' . $portfo->id;

        $response['unit'] = Setting::firstWhere('key','trade_unit')->value;
        $response['trade'] = Trade::where('portfo', $portfo->id)->orderBy('id', 'DESC')->get();



        return view('user.page.trade.list', compact('response'));
    }



    /*  ثبت معامله جدید  */
    public function store(Request $request,$tradeListId)
    {

        $tradeList = TradeList::firstWhere('id',$tradeListId);

        /*  بررسی موجود بودن معامله  */
        if ($tradeList === null)
        {
            return  redirect()->back()->with('warning','معامله مورد نظر از جدول معاملات حذف گردیده است');
        }


        /*  ساعت باز و بسته شدن معاملات  */
        $trade_start_time   = (int)Setting::firstWhere('key','trade_start_time')->value;
        $trade_end_time     = (int)Setting::firstWhere('key','trade_end_time')->value;

        /*  بررسی زمان معامله و باز و بسته بودن بازار  */
        $hour = jdate()->format('H');
        $trade_close = optional(Setting::firstWhere('key', 'trade_close'))->value;

        if ($trade_close === 'no')
        {
            if ($hour >= $trade_start_time && $hour < $trade_end_time)
            {

                /*    والد طرف اول    */
                $first_portfo = Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);


                /*    والد طرف دوم    */
                $second_portfo = Portfo::firstWhere('id', $tradeList->portfo);


                /*    بررسی وجود معامله باز   */
                if ($first_portfo === null)
                {
                    return redirect()->back()->with('warning', 'ابتدا باید یک پرتفو فعال جهت انجام معاملات باز کنید');
                }


                /*    بررسی طرف های معامله    */
                if ($first_portfo->id === $second_portfo->id)
                {
                    return redirect()->back()->with('warning', 'دو طرف معامله نباید یک شخص باشد');
                }


                /*    طرف اول فروشنده ، طرف دوم خریدار    */
                if ($tradeList->type === 'buyer')
                {

                    /* اعتبارسنجی */
                    $request->validate([
                        'order_weight_buyer' => 'required|numeric'
                    ]);

                    /*   بررسی تعداد واحد مجاز فروش  برای طرف اول    */
                    if ($request->order_weight_buyer > $first_portfo->weight_sell)
                    {
                        return redirect()->back()->with('warning', 'تعداد واحد وارد شده بیشتر از حد مجاز می باشد.');
                    }

                    /*   میزان باقی مانده تعداد واحد در لیست موقت    */
                    if ($request->order_weight_buyer > $tradeList->weight)
                    {
                        return redirect()->back()
                            ->with('warning', 'واحد وارد شده بیشتر از واحد موجود فروشنده/خریدار  می باشد');
                    }

                    /*   بررسی تعداد واحد مجاز خرید  برای طرف دوم    */
                    if ($request->order_weight_buyer > $second_portfo->weight_buy)
                    {
                        return redirect()->back()->with('warning', 'معامله قابل انجام نمی باشد.');
                    }


                    /*   ویرایش لیست موقت : کم کردن تعداد واحدی که الان معامله شده    */
                    $tradeList->weight -= $request->order_weight_buyer;
                    $tradeList->save();


                    /*   بستن معاملات باز طرف دوم که خریدار می باشد   */
                    $this->CloseOpenTrade($second_portfo, $tradeList, $request->order_weight_buyer, 'buyer');


                    /*   بستن معاملات باز طرف اول که فروشنده می باشد   */
                    $this->CloseOpenTrade($first_portfo, $tradeList, $request->order_weight_buyer, 'seller');


                }
                else/*    طرف اول خریدار ، طرف دوم فروشنده    */
                 {


                    /* اعتبارسنجی */
                    $request->validate([
                        'order_weight_seller' => 'required|numeric'
                    ]);

                    /*   بررسی تعداد واحد مجاز خرید  برای طرف اول    */
                    if ($request->order_weight_seller > $first_portfo->weight_buy)
                    {
                        return redirect()->back()->with('warning', 'تعداد واحد وارد شده بیشتر از حد مجاز می باشد.');
                    }

                    /*   میزان باقی مانده تعداد واحد در لیست موقت    */
                    if ($request->order_weight_seller > $tradeList->weight)
                    {
                        return redirect()->back()
                            ->with('warning', 'واحد وارد شده بیشتر از واحد موجود فروشنده/خریدار  می باشد');
                    }

                    /*   بررسی تعداد واحد مجاز فروش  برای طرف دوم    */
                    if ($request->order_weight_seller > $second_portfo->weight_sell)
                    {
                        return redirect()->back()->with('warning', 'معامله قابل انجام نمی باشد.');
                    }


                    /*   ویرایش لیست موقت : کم کردن تعداد واحدی که الان معامله شده    */
                    $tradeList->weight -= $request->order_weight_seller;
                    $tradeList->save();


                    /*   بستن معاملات باز طرف دوم که فروشنده می باشد   */
                    $this->CloseOpenTrade($second_portfo, $tradeList, $request->order_weight_seller, 'seller');

                    /*   بستن معاملات باز طرف اول که خریدار می باشد   */
                    $this->CloseOpenTrade($first_portfo, $tradeList, $request->order_weight_seller, 'buyer');
                }



                /*    پاک کردن حراج های مشابه   */
                if ($tradeList->parent_trade !== null && $tradeList->weight == 0)
                {
                    TradeList::where('parent_trade',$tradeList->parent_trade)->delete();
                }

                return redirect()->route('user.portfo.list')->with('success', 'معامله با موفقیت انجام شد');
            }
            else
            {
                    return redirect()->route('user.dashboard')->with('warning', 'ساعت کار بازار به پایان رسیده است.');
            }
        }
        else
        {
            return redirect()->back()->with('warning', 'در حال حاضر بازار تعطیل می باشد و معامله ای انجام نمیشود.');
        }

    }



    /**
     *
     *  PORTFO      :  مربوط به والد معامله می باشد
     *  TRADELIST   :  برای به دست آوردن تعداد واحد و قیمت استفاده شده
     *  WEIGHT      :  تعداد واحد معامله شده
     *  TYPE        :  خریدار یا فروشنده بودن
     *
     * */
    public function CloseOpenTrade(Portfo $portfo, TradeList $tradeList, $weight, $type)
    {

        $trade_unit = Setting::firstWhere('key','trade_unit')->value;

        $trade_success = false;

        /*   به دست آوردن معاملات باز و بستن آنها    */
        $open_trades = Trade::where([['portfo', $portfo->id],
                       ['type', '<>', $type], ['status', 'open']])
                       ->orderBy('id', 'ASC')->get();

        /* ذخیره تعداد واحد معامله شده در متغیر موقت */
        $tmp_weight = $weight;

        if ( count($open_trades) > 0 )
            foreach ($open_trades as $open_trade)
            {
                /*   حالتی که تعداد واحد بیشتر یا برابر از تعداد واحد معامله قبلی باشد     */
                if ($tmp_weight >= $open_trade->weight)
                {
                    /*  به دست آوردن سود و زیان برای معامله قبلی بر اساس خریدار یا فروشنده بودن در حالت معامله قبلی     */
                    $profit = abs((($open_trade->amount - $tradeList->amount) / 1000) * ($trade_unit * 230) * $open_trade->weight);

                    if ($open_trade->type === 'buyer')
                    {
                        if ($open_trade->amount > $tradeList->amount)
                            $profit = $profit * -1;
                    }
                    else
                    {
                        if ($open_trade->amount < $tradeList->amount)
                            $profit = $profit * -1;
                    }

                    $open_trade->status     = 'close';
                    $open_trade->end_amount = $tradeList->amount;  /*   قیمت نهایی    */
                    $open_trade->profit     = $profit;/*   سود و زیان    */


                    /*   حراج هست یا نه    */
                    if ($open_trade->portfo === $tradeList->portfo && $tradeList->parent_trade !== null)
                    {
                        $open_trade->end_type= 'onsale';
                    }


                    $open_trade->save();

                    /*  محاسبه کارمزد   */
                    $this->wage($portfo, $open_trade->weight, $open_trade->id);

                    /*  کم کردن تعداد واحد در متغیر موقت     */
                    $tmp_weight -= $open_trade->weight;

                    /*  تایید موفقیت معامله  */
                    $trade_success = true;
                }
                else
                {
                    if ($tmp_weight > 0)
                    {
                        /*  به دست آوردن سود و زیان برای معامله قبلی بر اساس خریدار یا فروشنده بودن در حالت قبلی     */
                        $profit = abs((($open_trade->amount - $tradeList->amount) / 1000) * ($trade_unit * 230) * $tmp_weight);
                        if ($open_trade->type === 'buyer')
                        {
                            if ($open_trade->amount > $tradeList->amount)
                                $profit = $profit * -1;
                        }
                        else
                        {
                            if ($open_trade->amount < $tradeList->amount)
                                $profit = $profit * -1;
                        }


                        /*   ایجاد معامله جدید برای بخشی از معامله قبلی    */
                        $trade = new Trade();
                        $trade->portfo      = $open_trade->portfo;
                        $trade->weight      = $tmp_weight; /*   تعداد واحد باقی مانده    */
                        $trade->amount      = $open_trade->amount;
                        $trade->type        = $open_trade->type;
                        $trade->status      = 'close';
                        $trade->end_amount  = $tradeList->amount;  /*   قیمت نهایی    */
                        $trade->profit      = $profit; /*   سود و زیان    */

                        /*   حراج هست یا نه    */
                        if ($open_trade->portfo === $tradeList->portfo && $tradeList->parent_trade !== null)
                        {
                            $trade->end_type= 'onsale';
                        }

                        $trade->save();

                        /*  محاسبه کارمزد   */
                        $this->wage($portfo, $trade->weight, $trade->id);

                        /*   ویرایش وزن معامله قبلی    */
                        $open_trade->weight -= $tmp_weight;
                        $open_trade->save();

                        $tmp_weight = 0;

                        /*  تایید موفقیت معامله  */
                        $trade_success = true;
                    }
                }
            }


        /*   در صورتی که وزن متغیر موقت بیشتر از 0 باشد، معامله اصلی باز ذخیره شده    */
        if ($tmp_weight > 0)
        {
            $trade = new Trade();
            $trade->portfo  = $portfo->id;
            $trade->weight  = $tmp_weight;
            $trade->amount  = $tradeList->amount;
            $trade->type    = $type;
            $trade->status  = 'open';
            $trade->profit  = 0;
            $trade->save();

            /*  تایید موفقیت معامله  */
            $trade_success = true;
        }

        if ($trade_success === true && $tradeList->parent_trade == '')
        {
            $trade_last_amount          = Setting::firstWhere('key','trade_last_amount');
            $trade_last_amount->value   = $tradeList->amount;
            $trade_last_amount->save();
        }

        /*  محاسبه سود و ضرر پرتفو   */
        $this->calculateProfit($portfo);

    }



    /*  محاسبه کارمزد  */
    public function wage(Portfo $portfo, $weight, $trade)
    {
        $unit = Setting::firstWhere('key','trade_unit')->value;
        $wage = $weight * ($unit * 120);

        $portfo->amount -= $wage;
        $portfo->save();

        // Log history
        Financial::history($portfo->user, $wage, 'Trade', $trade, 'trade', 'no', 'yes');
    }



    /*  محاسبه سود و زیان کلی و ویرایش پرتفو  */
    public function calculateProfit(Portfo $portfo)
    {

        $trade_unit     = Setting::firstWhere('key','trade_unit')->value;

        $close_trades   = Trade::where([['portfo', $portfo->id],['status', 'close']])
                          ->orderBy('id', 'ASC')->get();

        if (count($close_trades) > 0)
        {
            $profit = $close_trades->sum('profit');

            /*  سود و زیان این دوره را به دست آورده و دوره قبل از آن کم می وشد و با مبلغ جمع شده  */
            $portfo->amount += $profit - $portfo->profit;

            /*  برابر سود و زیان این دوره قرار داده میشود  */
            $portfo->profit = $profit;

            if ($portfo->amount > 0)
            {
                $portfo->weight = floor($portfo->amount / ($portfo->lever * ($trade_unit * 230)));
            }
            else
            {
                $portfo->weight = 0 ;
            }

            $portfo->save();
        }

        /*  محاسبه تعداد واحد مجاز خرید و فروش  */
        $this->calculateWeight($portfo);

    }



    /*  محاسبه تعداد واحد مجاز خرید و فروش  */
    public function calculateWeight(Portfo $portfo)
    {

        $open_trades = Trade::where([['portfo', $portfo->id], ['status', 'open']])
            ->orderBy('id', 'ASC')->get();

        if ($portfo->weight === 0)
        {
            $portfo->weight_buy  = 0;
            $portfo->weight_sell = 0;
            $portfo->save();
        }
        else
        {
            if (count($open_trades) > 0)
            {
                $type   = $open_trades[0]['type'];
                $weight = $open_trades->sum('weight');

                if ($type === 'buyer')
                {
                    $portfo->weight_buy  = $portfo->weight - $weight;
                    $portfo->weight_sell = $portfo->weight + $weight;
                }
                else
                {
                    $portfo->weight_sell = $portfo->weight - $weight;
                    $portfo->weight_buy  = $portfo->weight + $weight;
                }
                $portfo->save();
            }
            else
            {
                $portfo->weight_buy  = $portfo->weight;
                $portfo->weight_sell = $portfo->weight;
                $portfo->save();
            }
        }

    }




}
