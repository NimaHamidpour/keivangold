<?php

namespace App\Http\Controllers\User;

use App\CustomClass\Financial;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Payment\Facade\Payment;

class PayController extends Controller
{
    
    public function callBackBank(Request $request){


        // CHECK PAID ON NOT
        if (isset($request->RefId))
        {

            try
            {
                // UPDATE PAYMENT INFO IN MYSQL
                $pay = Pay::firstWhere('authority',$request->RefId);
                $receipt = Payment::amount($pay->amount)->transactionId($pay->authority)->verify();
                $pay->refid=$receipt->getReferenceId();

                if ($pay->save())
                {
                    // Log history
                    Financial::history(Auth::user()->id,$pay->amount,'Pay',$pay->id,'wallet','yes','yes');

                    $user = User::firstWhere('id',Auth::user()->id);
                    $user->wallet += $pay->amount;
                    $user->save();

                    return redirect()->route('user.dashboard')->with('success','پرداخت شما با موفقیت انجام شد.');

                }

                return redirect()->route('user.dashboard')->with('success','پرداخت شما با موفقیت انجام همراه نبود.لطفا با کارشناسان وب سایت تماس بگیرید');

            }
            catch (InvalidPaymentException $exception)
            {
                echo $exception->getMessage();
            }

            return redirect()->route('user.dashboard')->with('success','پرداخت شما با موفقیت انجام همراه نبود.لطفا با کارشناسان وب سایت تماس بگیرید');

        }
    }




}



