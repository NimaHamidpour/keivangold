<?php

namespace App\Http\Controllers\User;

use App\CustomClass\Financial;
use App\Http\Controllers\Controller;
use App\Models\Pledge;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\TradeList;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PortfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();


        $response['page_title'] = 'پرتفوی شما';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $response['unit_trade'] = Setting::firstWhere('key','trade_unit')->value;
        $response['portfo_open']=Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);
        $response['portfo'] = Portfo::where('user', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10);

        return view('user.page.portfo.list', compact('response'));
    }

    public function store(Request $request)
    {
        /*  ساعت باز و بسته شدن معاملات  */
        $trade_start_time   = (int)Setting::firstWhere('key','trade_start_time')->value;
        $trade_end_time     = (int)Setting::firstWhere('key','trade_end_time')->value;

        $request->validate([
             'amount' => 'required|numeric|max:'.Auth::user()->wallet,
             'lever'  => 'required|numeric|min:50|max:100'
        ]);

        /*  بررسی زمان معامله و باز و بسته بودن بازار  */
        $hour = jdate()->format('H');
        $trade_close = optional(Setting::firstWhere('key','trade_close'))->value;

        if ($trade_close === 'no')
        {
            if ($hour >= $trade_start_time && $hour < $trade_end_time)
            {
                $unit = Setting::firstWhere('key','trade_unit')->value;
                $weight = floor($request->amount / ($request->lever * $unit * 230 )) ;

                if ($weight < 1)
                {
                    return redirect()->back()->with('warning', 'میزان وجه تضمین شما جهت ساخت پرتفو کمتر از ضمانت حداقل یک واحد با اهرم 2x، (1/150/000 تومان) می باشد.لطفا دوباره تلاش کنید. ');

                }
                $check_portfo = Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);

                if ($check_portfo !== null)
                {
                    return redirect()->back()->with('warning', 'هر کاربر فقط یک معامله باز می تواند داشته باشد');
                }
                else
                {
                    // AMOUNT CAN USE
                    $wallet = Auth::user()->wallet;

                    if ($request->amount > $wallet)
                    {
                        return redirect()->back()
                            ->with('warning', 'موجودی شما کمتر از حد مورد نیاز می باشد.لطفا کیف پول خود را شارژ کنید.مبلغ مورد نیاز برابر : '
                                . number_format($request->amount - $wallet) . ' تومان ');
                    }

                    $portfo = new Portfo();

                    $portfo->user        = Auth::user()->id;
                    $portfo->weight      = $weight;
                    $portfo->openAmount  = $request->amount;
                    $portfo->amount      = $request->amount;
                    $portfo->lever       = $request->lever;
                    $portfo->weight_buy  = $weight;
                    $portfo->weight_sell = $weight;

                    try
                    {
                        $portfo->save();
                    }
                    catch (Exception $exception) {
                        return redirect()->back()->with('warning', 'ثبت درخواست  موفقیت آمیز نبود. لطفا دوباره تلاش فرمایید');
                    }

                    // MINES FROM WALLET
                    $user = User::firstWhere('id', Auth::user()->id);

                    $user->wallet = $user->wallet - $portfo->amount;
                    $user->save();


                    // Log history
                    Financial::history($portfo->user,$portfo->amount,'Portfo',$portfo->id,'wallet','no','yes');

                    return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
                }
            }
            else
            {
                return redirect()->route('user.dashboard')->with('warning', 'ساعت کار بازار به پایان رسیده است.');
            }
        }
        else
        {
            return  redirect()->back()->with('warning','در حال حاضر بازار تعطیل می باشد و معامله ای انجام نمیشود.');

        }

    }

    public  function close(Portfo $portfo)
    {
        // delete old trade list
        TradeList::where('portfo',$portfo->id)->delete();

        time_sleep_until(time() + 2);

        if ($portfo->weight_buy === $portfo->weight_sell)
        {
            if ($portfo->end_date === null)
            {
                $portfo->end_date = Carbon::now()->format('Y-m-d H:i:s');
                $portfo->save();

                $user = User::firstWhere('id',$portfo->user);
                $user->wallet +=$portfo->amount;
                $user->save();

                // Log history
                Financial::history($portfo->user,$portfo->amount,'Portfo',$portfo->id,'wallet','yes','yes');


                return redirect()->back()->with('success','مبلغ موجود در پرتفو با موفقیت به کیف پول شما منتقل گشت');
            }
            else
            {
                return redirect()->route('user.dashboard');
            }
        }
        else
        {
            return redirect()->route('user.dashboard')->with('warning','کاربر گرامی به علت باز بودن معاملات امکان بستن پرتفو وجود ندارد.');
        }

    }
}
