<?php
namespace App\Http\Controllers\User;

use App\CustomClass\Financial;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class WithdrawalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=User::firstWhere('id',Auth::user()->id);

        $response['page_title']='لیست برداشت از حساب';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $response['withdrawal']=Withdrawal::where('user',Auth::user()->id)->orderBy('id','DESC')->paginate(5);

        return view('user.page.withdrawal.create',compact('response'));
    }

    public function store(Request $request)
    {


        $user = User::firstWhere('id',Auth::user()->id);

        $request->validate([
            'amount'=>'required|numeric|min:15000|max : '.$user->wallet
        ]);



        $withdrawal = new Withdrawal();
        $withdrawal->user =Auth::user()->id;
        $withdrawal->amount =$request->amount;
        $withdrawal->status = 'waiting';
        $withdrawal->save();

        // minus money
        $user->wallet -= $request->amount;
        $user->save();

        // Log history
        Financial::history(Auth::user()->id,$request->amount,'Withdrawal',$withdrawal->id,'wallet','no','yes');


        return redirect()->back()->with('success','درخواست شما با موفقیت ثبت شد');
    }



}
