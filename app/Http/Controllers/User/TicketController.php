<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Meta;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $response['page_title'] = 'لیست پیام ها';
        $response['tickets'] = Ticket::where('user', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(20);


        return view('user.page.ticket.list', compact('response'));
    }

    public function create()
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $response['page_title'] = 'ایجاد پیام جدید';

        return view('user.page.ticket.add', compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate
        ([
            'title' => 'required',
            'message' => 'required|min:5',
            'Capcha' => 'required|captcha',
        ]);


        // CRAETE TICKET AND SAVE TICKET DB
        $ticket             = new Ticket();
        $ticket->user       = Auth::user()->id;
        $ticket->title      = $request->title;
        $ticket->priority   = $request->priority;
        $ticket->status     = 'admin-answer';
        $ticket->save();


        //  SAVE CONVERSATION DB
        $conversation            = new Conversation();
        $conversation->writer    = 'User';
        $conversation->writer_id = Auth::user()->id;
        $conversation->message   = $request->message;
        $conversation->model     = 'Ticket';
        $conversation->model_id  = $ticket->id;
        $conversation->save();


        foreach ($request->files as $row => $file)
        {
            foreach ($file as $item)
            {
                if (++$row < 2)
                {

                    $path = strtolower($item->getClientOriginalExtension());

                    if ($path === 'png' || $path === 'jpg' || $path === 'jpeg')
                    {
                        $destinationPath = 'storage/upload/';
                        $filename = time() . rand(0, 999999) . '.' . $item->getClientOriginalExtension();

                        $img = Image::make($item->getRealPath())->resize(300, 300);
                        $img->save($destinationPath . $filename);

                        $meta           = new Meta();
                        $meta->key      = 'file';
                        $meta->unique   = 'img';
                        $meta->value    = 'storage/upload/' . $filename;
                        $meta->model    = 'Conversation';
                        $meta->model_id = $conversation->id;
                        $meta->save();

                    }

                }
            }
        }

        return redirect()->route('user.ticket.list')->with('success', 'پیام با موفقیت ارسال شد');
    }

    public function show(Ticket $ticket)
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $response['page_title'] = ' تیکت : ' . $ticket->title;
        $response['ticket']     = $ticket;
        $response['reply']      = Conversation::where([['model', 'Ticket'], ['model_id', $ticket->id]])->orderBy('id', 'ASC')->get();

        return view('user.page.ticket.reply', compact('response'));
    }


    public function finished(Ticket $ticket)
    {
        $ticket->status = 'close';
        $ticket->save();

        return redirect()->route('user.ticket.list')->with('success', 'درخواست پشتیبانی با موفقیت بسته شد');
    }

}
