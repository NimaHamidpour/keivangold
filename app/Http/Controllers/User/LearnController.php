<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Meta;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Visit;
use Illuminate\Support\Facades\Auth;

class LearnController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='آموزش معاملات نقدی';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        $video = Setting::firstWhere('key','LearnVideo');
        $response['video'] = Meta::firstWhere([['model','Setting'],['model_id',$video->id],['key','LearnVideo']]);

        return view('user.page.video.show',compact('response'));
    }
}
