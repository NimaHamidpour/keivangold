<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\TradeList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TradeListController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $type)
    {
        /*  ساعت باز و بسته شدن معاملات  */
        $trade_start_time = (int)Setting::firstWhere('key', 'trade_start_time')->value;
        $trade_end_time = (int)Setting::firstWhere('key', 'trade_end_time')->value;

        /*  بررسی زمان معامله و باز و بسته بودن بازار  */
        $hour = jdate()->format('H');
        $trade_close = optional(Setting::firstWhere('key', 'trade_close'))->value;

        if ($trade_close === 'no') {
            if ($hour >= $trade_start_time && $hour < $trade_end_time) {

                $portfo = Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);

                $trade_last_amount = (int)optional(Setting::firstWhere('key', 'trade_last_amount'))->value;

                if ($trade_last_amount !== null)
                    $last_amount = $trade_last_amount;
                else
                    $last_amount = 0;

                if ($portfo !== null)
                {
                    if ($type === 'buyer')
                    {
                        $request->validate([
                            'buy_weight' => 'required|integer|min:1|max:10',
                            'buy_amount' => 'required|integer|min:1',
                        ]);

                        /*  اختلاف قیمت پیشنهادی با مظنه بازار باید نهایتا 20 خط باشد  */
                        $trade_max_dispute = (int)optional(Setting::firstWhere('key', 'trade_max_dispute'))->value; // get dynamic from db

                        if ($last_amount === 0)
                            $last_amount = $request->buy_amount * 1000;

                        if (abs(($request->buy_amount * 1000) - $last_amount) > $trade_max_dispute)
                        {
                            return redirect()->back()->with('warning', 'اختلاف مبلغ وارد شده با مظنه بازار بیشتر از حد مجاز است.');
                        }

                        /* تعداد واحد مجاز قابل معامله  */
                        if ($request->buy_weight > $portfo->weight_buy)
                        {
                            return redirect()->back()->with('warning', 'تعداد واحد وارد شده بیشتر از حد مجاز می باشد.');
                        }
                        else
                        {
                            $amount = $request->buy_amount * 1000;
                            $weight = $request->buy_weight;
                            $type = 'buyer';
                        }
                    }
                    else
                    {
                        $request->validate([
                            'sell_weight' => 'required|integer|min:1|max:10',
                            'sell_amount' => 'required|integer|min:1',
                        ]);


                        /*  اختلاف قیمت پیشنهادی با مظنه بازار باید نهایتا 20 خط باشد  */
                        $trade_max_dispute = (int)optional(Setting::firstWhere('key', 'trade_max_dispute'))->value; // get dynamic from db

                        if ($last_amount === 0)
                            $last_amount = $request->sell_amount * 1000;

                        if (abs(($request->sell_amount * 1000) - $last_amount) > $trade_max_dispute)
                        {
                            return redirect()->back()->with('warning', 'اختلاف مبلغ وارد شده با آخرین معامله انجام شده، بیشتر از حد مجاز است.');
                        }

                        /* تعداد واحد مجاز قابل معامله  */
                        if ($request->sell_weight > $portfo->weight_sell)
                        {
                            return redirect()->back()->with('warning', 'تعداد واحد وارد شده بیشتر از حد مجاز می باشد.');
                        }
                        else
                        {
                            $amount = $request->sell_amount * 1000;
                            $weight = $request->sell_weight;
                            $type = 'seller';
                        }
                    }

                    /*  ذخیره اطلاعات در داخل دیتابیس  */
                    $tradelist = new TradeList();

                    $tradelist->portfo = $portfo->id;
                    $tradelist->weight = $weight;
                    $tradelist->amount = $amount;
                    $tradelist->type   = $type;
                    $tradelist->active = 'yes';

                    try
                    {
                        $tradelist->save();
                    }
                    catch (Exception $exception)
                    {
                        return redirect()->back()->with('warning', 'ثبت درخواست  موفقیت آمیز نبود. لطفا مجددا تلاش فرمایید');
                    }

                    return redirect()->back()->with('success', 'سفارش شما با موفقیت ثبت شد');

                }
                return redirect()->back()->with('warning', 'ابتدا باید یک پرتفو فعال جهت انجام معاملات باز کنید');

            } else {
                return redirect()->route('user.dashboard')->with('warning', 'ساعت کار بازار به پایان رسیده است.');
            }
        } else {
            return redirect()->back()->with('warning', 'در حال حاضر بازار تعطیل می باشد و معامله ای انجام نمیشود.');

        }
    }

    public function destroy($id)
    {

        $tradeList = TradeList::firstWhere('id',$id);
        
        if ($tradeList !== null)
        {
            $tradeList->delete();
            return redirect()->back()->with('success','پیشنهاد شما با موفقیت حذف گردید.');
        }
        return redirect()->back()->with('success','پیشنهاد شما در لیست پیشنهادات وجود ندارد.');
    }

}
