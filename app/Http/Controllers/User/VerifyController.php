<?php

namespace App\Http\Controllers\User;

use App\CustomClass\PersianNum;
use App\CustomClass\SendSms;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class VerifyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // EDIT NAME AND MOBILE AND AVATAR
    public function edit()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=User::firstWhere('id',Auth::user()->id);

        $response['page_title']='تایید تلفن همراه';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;

        $user = Auth::user();

        $code = rand(10000,99999);

        $user->deleteMeta('verify_mobile');

        $user->setMeta(['verify_mobile' => $code ]);


        SendSms::verify(Auth::user()->mobile,Auth::user()->name,$code);

        return view('user.page.verify.edit',compact('response'));
    }

    public function update(Request $request)
    {
        $verifyCode   = PersianNum::convert($request->verifyCode);

        $request->merge(['verifyCode' => $verifyCode]);

        $request->validate(['verifyCode'=>'required|numeric']);

        $user = Auth::user();

        $verifyCode = $user->getMeta('verify_mobile');

        if ($verifyCode === $request->verifyCode)
        {
            $user->mobile_verified_at = Carbon::now()->toDateString();
            $user->timestamps         =   false;

            $user->save();

            $user->deleteMeta('verify_mobile');
            return redirect()->route('user.dashboard')->with('success','شماره تلفن شما با موفقیت تایید شد.');
        }
        else
        {
            return redirect()->back()->with('warning','کد تایید وارد شده معتبر نمی باشد.لطفا دوباره تلاش کنید.');
        }
    }

}
