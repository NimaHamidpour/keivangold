<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\OnlinePay;
use App\Models\Pay;
use App\Models\Pledge;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class WalletController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=User::firstWhere('id',Auth::user()->id);

        $response['page_title']='شارژ کیف پول';

        // AMOUNT CAN USE
        $response['wallet'] = Auth::user()->wallet;


        return view('user.page.wallet.create',compact('response'));
    }

    public function sharj_wallet(Request $request)
    {
        $request->validate([
            'amount'=>'required|numeric'
        ]);

        $invoice = new Invoice;
        $invoice->amount((int)$request->amount);
        $invoice->detail('price',$request->amount);
        Payment::purchase($invoice,function($driver, $transactionId) {});

        // ONLINE PAY DATABASE
        $pay=new Pay();
        $pay->user           =   Auth::user()->id;
        $pay->amount         =   $request->amount;
        $pay->authority      =   $invoice->getTransactionId();

        $pay->save();

        return Payment::pay()->render();
    }



}
