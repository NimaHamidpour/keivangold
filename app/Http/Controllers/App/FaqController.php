<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Setting;


class  FaqController extends Controller
{
    public function index()
    {

        $response=[];

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['socials']=Setting::where('type','social')->get();


       $response['question'] = Question::orderBy('id','ASC')->get()->groupBy('category');


       return view('app.page.faq',compact('response'));
    }
}
