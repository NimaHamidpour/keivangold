<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Setting;

class CartController extends Controller
{

    public function index()
    {
        $response=[];

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['socials']=Setting::where('type','social')->get();

        $response['sum']=0;
        foreach (session()->get('cart') as $cart)
        {
            $response['sum']+=$cart['price'];
        }
        return view('app.page.cart',compact('response'));
    }

}
