<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Visit;

class AboutController extends Controller
{
    public function index()
    {
        $response=[];

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['socials']=Setting::where('type','social')->get();


        // META
        $response['aboutPage']=Setting::firstWhere('key','aboutPage');
        $response['meta']=$response['aboutPage']->getMeta('meta',true);


        // VISITS
        $visit = new Visit();
        $visit->ip = \Request::getClientIp();
        $visit->useragent = \Request::header('User-Agent');
        $visit->session_id =\Request::getSession()->getId();
        $visit->url = \Request::url();
        $visit->model = 'Setting';
        $visit->model_id = $response['aboutPage']->id;
        $visit->save();


        return view('app.page.about',compact('response'));
    }
}
