<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Category;
use App\Models\Image;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\TradeList;
use App\Models\Visit;

class HomeController extends Controller
{
    public function index()
    {

        $response=[];

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['socials']=Setting::where('type','social')->get();
        $response['product_category']=Category::where('type','product')->orderBy('id','ASC')->get();

        $response['product']=Product::orderBy('id','DESC')->get();

        $response['whyus']=Image::where('key','whyus')->get();





        // BLOG
        $response['blog_groups']=Category::where('type','blog')->orderBy('id','ASC')->get();
        if (count($response['blog_groups']) > 0)
            $response['blogs']=Page::where([['type','blog'],['parent',$response['blog_groups']->first()->id]])
                ->orderBy('id','DESC')->limit(6)->get();

        // META
        $response['main']=Setting::firstWhere('key','mainPage');
        $response['meta']=$response['main']->getMeta('meta',true);

        // VISITS
        $visit = new Visit();
        $visit->ip = \Request::getClientIp();
        $visit->useragent = \Request::header('User-Agent');
        $visit->session_id =\Request::getSession()->getId();
        $visit->url = \Request::url();
        $visit->model ='Setting';
        $visit->model_id = $response['main']->id;
        $visit->save();

        $response['help']   = Page::firstWhere('id','1');
        $response['trade']  = Page::firstWhere('id','2');

        return view('app.page.home',compact('response'));
    }

}
