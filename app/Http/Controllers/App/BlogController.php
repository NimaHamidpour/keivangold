<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Visit;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index($slug=null)
    {
        $response=[];

        // **** SETTINGS OF SITE ****
        $response['websazandeh_name']='آکادمی نیما حمیدپور';
        $response['websazandeh_logo']='https://websazandeh.ir/img/logo200.png';

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['blog_footer']=optional(Setting::firstWhere('key','blog_footer'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_main_tell']=optional(Setting::firstWhere('key','app_main_tell'))->value;
        $response['app_main_email']=optional(Setting::firstWhere('key','app_main_email'))->value;
        $response['socials']=Setting::where('type','social')->get();


        // GET CATEGORY EXIST
        $parent=Page::where('type','blog')->get()->pluck('parent');
        $response['categories']=Category::whereIn('id',$parent)->get();


        // GET MOST VIEW BLOG
        $response['most_visited']=Page::where('type','blog')->orderBy('views','DESC')->limit(3)->get();




        // GET BLOG --> IF SEARCH CATEGORY GET BLOG WITH SELECTED CATEGORY IF NOT GET ALL
        if ($slug !== null)
        {
            $category=Category::firstWhere('slug',$slug);
            $response['latest_content']=Page::where('type','blog')
                ->whereNotIN('id',$response['most_visited']->pluck('id'))
                ->where('parent',$category->id)
                ->orderBy('id','DESC')->paginate(10);
        }
        else
        {
            $response['latest_content']=Page::where('type','blog')
                ->whereNotIN('id',$response['most_visited']->pluck('id'))
                ->orderBy('id','DESC')->paginate(10);
        }


        // META
        $response['main']=Setting::firstWhere('key','blogPage');
        $response['meta']=$response['main']->getMeta('meta',true);

        // VISITS
        $visit = new Visit();
        $visit->ip = \Request::getClientIp();
        $visit->useragent = \Request::header('User-Agent');
        $visit->session_id =\Request::getSession()->getId();
        $visit->url = \Request::url();
        $visit->model ='Setting';
        $visit->model_id = $response['main']->id;
        $visit->save();

        return view('blog.page.main',compact('response'));
    }

    public function show($slug)
    {

        $response=[];

        // **** SETTINGS OF SITE ****
        $response['websazandeh_name']='آکادمی نیما حمیدپور';
        $response['websazandeh_logo']='https://websazandeh.ir/img/logo200.png';

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['blog_footer']=optional(Setting::firstWhere('key','blog_footer'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_main_tell']=optional(Setting::firstWhere('key','app_main_tell'))->value;
        $response['app_main_email']=optional(Setting::firstWhere('key','app_main_email'))->value;
        $response['socials']=Setting::where('type','social')->get();


        // GET CATEGORY EXIST
        $parent=Page::where('type','blog')->get()->pluck('parent');
        $response['categories']=Category::whereIn('id',$parent)->get();


        // GET BLOG
        $response['blog']=Page::firstWhere([['type','blog'],['slug',$slug]]);
        $response['blog']->views++;
        $response['blog']->save();

        // GET META TAG FOR THIS BLOG
        $response['meta']=$response['blog']->getMeta('meta',true);

        // GET RELATED AND MOST VIEW BLOG
        $response['related_blog']=Page::where([['type','blog'],['parent',$response['blog']->parent]]);
        $response['latest_content']=Page::where('type','blog')
            ->where('id','<>',$response['blog']->id)
            ->orderBy('id','DESC')
            ->limit(3)
            ->get();


        // VISITS
        $visit = new Visit();
        $visit->ip = \Request::getClientIp();
        $visit->useragent = \Request::header('User-Agent');
        $visit->session_id =\Request::getSession()->getId();
        $visit->url = \Request::url();
        $visit->model ='Page';
        $visit->model_id = $response['blog']->id;
        $visit->save();


        return view('blog.page.article',compact('response'));
    }

    public function search(Request $request)
    {
        $response=[];

        // **** SETTINGS OF SITE ****
        $response['websazandeh_name']='آکادمی نیما حمیدپور';
        $response['websazandeh_logo']='https://websazandeh.ir/img/logo200.png';

        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_main_tell']=optional(Setting::firstWhere('key','app_main_tell'))->value;
        $response['app_main_email']=optional(Setting::firstWhere('key','app_main_email'))->value;
        $response['socials']=Setting::where('type','social')->get();


        // GET CATEGORY EXIST
        $parent=Page::where('type','blog')->get()->pluck('parent');
        $response['categories']=Category::whereIn('id',$parent)->get();


        // GET SEARCH TEXT AND BLOG
        $response['search']=$request->search_text_name;

        $response['blogs']=Page::where([['type','blog'],['title','LIKE',$request->search_text_name.'%']])
                            ->orderBy('id','DESC')->get();


        // GET LATEST BLOG
        $response['latest_blog']=Page::where('type','blog')->orderBy('id','DESC')->limit(3)->get();

        // GET MOST VIEW BLOG
        $response['most_visited']=Page::where('type','blog')->orderBy('views','DESC')->limit(3)->get();

        return view('blog.page.search',compact('response'));
    }


}
