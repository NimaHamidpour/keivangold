<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {

        if($request->category === 'all')
        {
            $products=Product::orderBy('id','DESC')->get();
        }
        else
        {
            $products=Product::where('category',$request->category)->orderBy('id','DESC')->get();
        }


        $content='<div class="carousel owl-carousel" id="product-carousel">';

        foreach ($products as $product)
        {
            if (isset(session()->get('cart')[$product->id]))
            {
                $class='btn-secondary';
                $txt='به سبد خرید شما اضافه شد';
                $disable='disabled';
            }
            else
            {
                $class='btn-warning';
                $txt='افزودن به سبد خرید';
                $disable='';
            }
            $content.='<div class="item border border-warning rounded mx-1">
                            <img src="'.url($product->poster).'" alt="'.$product->title.'"
                                         class="img-fluid" style="height: 260px;width: 320px">
                            <h2 class="title pt-3 border-top border-warning bg-light">'.$product->title.'</h2>

                        </div>';
        }

        $content.='</div>';

        echo $content;
    }
}
