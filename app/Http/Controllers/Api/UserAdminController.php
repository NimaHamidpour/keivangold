<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function search(Request $request)
    {

        $users = User::where('name','LIKE','%'.$request->search.'%')
                    ->orderBy('id','DESC')->limit(15)->get();
        $output='';

        foreach ($users as $user)
        {
            $param =$user->id .",'".$user->name."'";
            $output.='<div class="bg-white border">
                          <a class="btn font-size-13 text-dark font-weight-bold"
                           onclick="setUser('.$param.')">'.$user->name.'</a>
                     </div>';
        }

        echo $output;
    }
}
