<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Trade;
use App\Models\TradeList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TradeController extends Controller
{

    public function index(Request $request)
    {

        $trade_close  =  Setting::firstWhere('key','trade_close');
        $response=[];
        $response['output']='';

        $portfo = Portfo::firstWhere([['user', Auth::user()->id], ['end_date', null]]);

        if ($trade_close->value === 'yes')
        {
            $response['msg'] = 'close';
        }
        else
        {
            $response['msg'] = 'open';

            /*   دریافت معاملات جدید بر اساس خریدار یا فروشنده بودن  */
            if ($request->type === 'buyer')
                $trades = TradeList::where([['active','yes'],['type',$request->type],['weight','>',0]])
                    ->orderBy('amount','DESC')->limit(5)->get();
            else
                $trades = TradeList::where([['active','yes'],['type',$request->type],['weight','>',0]])
                    ->orderBy('amount','ASC')->limit(5)->get();

            foreach ($trades as $trade)
            {


                $response['output'] .='<tr class="text-center font-size-13">
                               <td class="user-danger border-left d-none d-xl-block d-lg-block">'.$trade->id.'</td>';


                if ($trade->parent_trade === null)
                {
                    $response['output'] .=' <td class="user-info">'. number_format($trade->weight).' </td>';
                }
                else
                {
                    $response['output'] .= '<td class="user-info">'; number_format($trade->weight).' </td>';
                    $response['output'] .=  number_format($trade->weight);
                    $response['output'] .= '<span class="text-danger font-size-11 font-weight-bold"> (حـراج) </span>';
                    $response['output'] .= '</td>';
                }

                $response['output'] .='<td class="user-info">'.number_format($trade->amount / 1000).'</td>';

                if ($request->type === 'buyer')
                {
                    $tmp = "'#order_weight_buyer_$trade->id'";

                    if ($trade->portfo == $portfo->id)
                    {
                        $response['output'] .='<td>  <img src="'.url('app/img/star.svg').'" class="m-0 p-0"
                                                            alt="star" height="13px" width="13"></td>';
                        $response['output'] .='<td>  <img src="'.url('app/img/star.svg').'" class="m-0 p-0"
                                                            alt="star" height="13px" width="13"></td>';
                    }
                    else
                    {
                        $response['output'] .='<td>
                            <input type="number" value="'.$trade->weight.'" placeholder="تعداد واحد"
                                   class="form-control font-size-13 fa-num mx-auto text-center direction-ltr"
                                   style="width: 83px;height: 30px"
                                   onkeyup="$('."$tmp".').val(this.value)">
                            </td>';
                        $response['output'] .='<td>
                              <form action="'.route('user.trade.store',$trade->id).'" method="post">
                                            '.csrf_field().'
                                            <input type="hidden" name="order_weight_buyer" id="order_weight_buyer_'.$trade->id.'" value="'.$trade->weight.'">
                                            <input type="submit" value="معامله"
                                            class="btn btn-outline-danger btn-sm font-size-13"/>
                                  </form>
                            </td>
                    </tr>';
                    }
                }
                else
                {
                    $tmp = "'#order_weight_seller_$trade->id'";

                    if ($trade->Portfo->user == Auth::user()->id)
                    {
                        $response['output'] .='<td>  <img src="'.url('app/img/star.svg').'" class="m-0 p-0"
                                                            alt="star" height="13px" width="13"></td>';
                        $response['output'] .='<td>  <img src="'.url('app/img/star.svg').'" class="m-0 p-0"
                                                            alt="star" height="13px" width="13"></td>';
                    }
                    else
                    {
                        $response['output'] .='<td>
                            <input type="number" value="'.$trade->weight.'" placeholder="تعداد واحد"
                                   class="form-control fa-num font-size-13 mx-auto text-center"
                                   style="width: 83px;height: 30px"
                                   onkeyup="$('."$tmp".').val(this.value)">
                            </td>';
                        $response['output'] .='<td>
                              <form action="'.route('user.trade.store',$trade->id).'" method="post">
                                            '.csrf_field().'
                                            <input type="hidden" name="order_weight_seller" id="order_weight_seller_'.$trade->id.'" value="'.$trade->weight.'">
                                            <input type="submit" value="معامله"
                                            class="btn btn-outline-primary btn-sm font-size-13"/>
                                  </form>
                            </td>';
                    }

                    $response['output'] .='</tr>';
                }
            }
        }


        /* دریافت آنلاین قیمت */
        $response['trade_online_amount'] = 0;
        $trade_online_amount =  (int) optional(Setting::firstWhere('key','trade_online_amount'))->value;
        if ($trade_online_amount !== null)
            $response['trade_online_amount'] =  number_format($trade_online_amount);

        /* دریافت آخرین قیمت */
        $response['trade_last_amount'] = 0;
        $trade_last_amount =  (int) optional(Setting::firstWhere('key','trade_last_amount'))->value;
        if ($trade_last_amount !== null)
            $response['trade_last_amount'] =  number_format($trade_last_amount);


        /* گزارش معاملات هر شخص به خودش */
        $portfo = Portfo::firstWhere([['end_date',null],['user',Auth::user()->id]]);

        if ($portfo !== null)
        {
            $response['weight_buy']  = $portfo->weight_buy;
            $response['weight_sell'] = $portfo->weight_sell;
            $response['portfo_profit'] = number_format($portfo->profit);
            $response['portfo_profit_status'] = $portfo->profit >=0 ? 'posetive' : 'negative';

            if($portfo->end_date === null && count(Trade::where([['portfo',$portfo->id],['status','open']])->get()) > 0)
            {
                $response['portfo_status']  = ' <span class="user-info font-size-11">';
                $response['portfo_status'] .= 'باز';
                $response['portfo_status'] .= '(';
                $response['portfo_status'] .=  Trade::where([['portfo',$portfo->id],['status','open']])->sum('weight') . ' واحد ';
                $response['portfo_status'] .=  Trade::firstWhere([['portfo',$portfo->id],['status','open']])->type === 'seller' ? 'فروش':'خرید';
                $response['portfo_status'] .= ')';
                $response['portfo_status'] .= ' </span>';
            }
            elseif ($portfo->end_date === null && count(Trade::where([['portfo',$portfo->id],['status','open']])->get()) === 0)
            {
                $response['portfo_status']  = '<span class="user-danger font-size-11">';
                $response['portfo_status'] .= 'باز بدون معامله انجام شده';
                $response['portfo_status'] .= ' </span>';
            }
            else
            {
                $response['portfo_status']  = '<span class="user-black font-size-11">';
                $response['portfo_status'] .= 'بسته';
                $response['portfo_status'] .= ' </span>';
            }

            $response['portfo_msg'] = true;
        }
        else
        {
            $response['portfo_msg'] = false;
        }

        echo json_encode($response);

    }
}
