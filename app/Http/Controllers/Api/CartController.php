<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{

    /* ADD TO CART */
    public function addToCart(Request $request){

        $response=[];
        $product=Product::find($request->id);

        if(!$product)
        {
            echo 'wrong product';
        }

        $cart = session()->get('cart');


        // if cart is empty then this the first product
        if(!$cart) {
            $cart = [
                $request->id => [
                    "id"=>$product->id,
                    "title" => $product->title,
                    "weight" => $product->weight,
                    "price" => $product->price,
                    "poster" => $product->poster,
                ]
            ];
            session()->put('cart', $cart);
            $response['msg']='add';
        }
        else {
                if (!isset($cart[$request->id]))
                {
                    // if item not exist in cart then add to cart with quantity = 1
                    $cart[$request->id] = [
                        "id"=>$product->id,
                        "title" => $product->title,
                        "weight" => $product->weight,
                        "price" => $product->price,
                        "poster" => $product->poster,
                    ];
                    session()->put('cart', $cart);
                    $response['msg']='add';

                }
            }

        $response['count'] =count($cart);

        echo json_encode($response);
    }


    /* REMOVE FROM CART */
    public function removecart($id)
    {
        $cart = session()->get('cart');
        if(isset($cart[$id])) {
            unset($cart[$id]);
            session()->put('cart', $cart);
            return redirect()->back();
        }

        return redirect()->back();
    }

}
