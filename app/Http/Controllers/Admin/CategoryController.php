<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\Relation;
use App\Models\Setting;
use http\Exception;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /* SHOW LIST */
    public function index($type)
   {
       $response=[];
       $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
       $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

       $response['categories']=Category::where('type',$type)->paginate(10);
       $response['type']=$type;

        if ($type === 'blog')
            $response['page_title']='لیست دسته های بلاگ';
        elseif ($type === 'product')
            $response['page_title']='لیست دسته های محصولات';
        else
            $response['page_title']='لیست سوالات متداول';

       return view('admin.page.category.list',compact('response'));
   }

    /* STORE  GROUPS OR BLOG OR GENERATOR OR SERVICE */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|unique:categories',
            'icon' => 'required|image|max:2024',
        ]);

        $category=new Category([
            'title'=>$request->title,
            'type'=>$request->type,
            'slug'=>$request->slug,
        ]);

        try{
            $category->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->back()->with('success','دسته با موفقیت اضافه گردید.');

    }

    /* EDIT OF SERVICE OR BRAND */
    public function edit($id)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['category']=Category::firstWhere('id',$id);
        $response['page_title']='ویرایش دسته : '. $response['category']->title;
        return view('admin.page.category.edit',compact('response'));
    }

    /* UPDATE  GROUPS OR BLOG OR GENERATOR OR SERVICE*/
    public function update(Request $request,$id)
    {
        $category=Category::firstWhere('id',$id);

        $request->validate([
            'title'=>'required|unique:categories,title,'.$category->id,
        ]);


        $category->title = $request->title;
        $category->type = $request->type;

        try
        {
            $category->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','دسته با موفقیت ویرایش شد.');
    }

    /* DELETE  GROUPS OR BLOG OR GENERATOR OR SERVICE*/
    public function destroy($id)
    {
        $category=Category::firstWhere('id',$id);

        //delete meta for this category
        $category->Meta()->delete();

        try
        {
            if ($category->type=='blog')
            {
                Page::where([['parent',$category->id],['type','blog']])->delete();
            }
            $category->pivotTruncate();
            $category->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }
        return redirect()->back()->with('success','دسته با موفقیت حذف شد.');
    }


}
