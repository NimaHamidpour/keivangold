<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Question;
use App\Models\Setting;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='لیست سوالات متداول';
        $response['question']=Question::orderBy('id','DESC')->paginate(10);

        return view('admin.page.question.list',compact('response'));
    }

    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='اضافه کردن سوال جدید';
        $response['parent']=Category::where('type','question')->get();

        return view('admin.page.question.add',compact('response'));
    }

    public function store(Request $request)
    {


        $request->validate([
            'question'=>'required',
            'answer' => 'required',
        ]);

        $question = new Question();
        $question->question   = $request->question;
        $question->answer     = $request->answer;
        $question->category   = $request->category;


        try
        {
            $question->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }



        return redirect()->route('admin.question.list')->with('success','سوال جدید با موفقیت اضافه گردید.');
    }

    public function edit(Question $question)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['page_title']='ویرایش سوال';

        $response['question']=$question;

        $response['parent']=Category::where('type','question')->get();



        return view('admin.page.question.edit',compact('response'));
    }

    public function update(Request $request,Question $question)
    {

        $request->validate([
            'question'=>'required',
            'answer' => 'required',
        ]);

        $question->question   = $request->question;
        $question->answer     = $request->answer;
        $question->category   = $request->category;

        try
        {
            $question->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','مطلب با موفقیت ویرایش گردید.');
    }

    public function destroy(Question $question)
    {

        try
        {
            $question->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }


        return redirect()->back()->with('success','سوال با موفقیت حذف شد.');

    }
}
