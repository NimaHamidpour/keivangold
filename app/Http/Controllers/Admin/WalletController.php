<?php

namespace App\Http\Controllers\Admin;

use App\CustomClass\Financial;
use App\Exports\Export;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Trade;
use App\Models\TradeList;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create(User $user)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='شارژ کیف پول کاربر : '.$user->name;

        $response['user'] = $user;
        return view('admin.page.user.wallet',compact('response'));
    }



    public function store(Request $request)
    {

        $request->validate([
            'amount'=>'required|numeric',
        ]);

        $pay = new Pay();
        $pay->user       = $request->user;
        $pay->amount     = $request->amount;
        $pay->authority  = rand(1000,9999999999);
        $pay->refid      = rand(1000,9999999999);

        $pay->save();

        $user = User::firstWhere('id',$request->user);
        $user->wallet += $request->amount;
        $user->save();

        // Log history
        Financial::history($user->id,$pay->amount,'Pay',$pay->id,'wallet','yes','yes');

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');
    }


}
