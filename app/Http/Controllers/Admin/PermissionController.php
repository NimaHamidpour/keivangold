<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Meta;
use App\Models\Setting;
use Illuminate\Http\Request;


class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Admin $admin)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['permission']=Meta::where([['Model','Admin'],['model_id',$admin->id],['key','permission']])->orderBy('id','DESC')->get();

        $response['page_title']='سطح دسترسی ادمین : '.$admin->name;

        $response['permission_list']=Setting::where('type','permission')
                                     ->whereNotIn('key',$response['permission']->pluck('value')->toArray())
                                     ->orderBy('title','ASC')->get();

        $response['admin']=$admin;
        return view('admin.page.manager.permission',compact('response'));
    }

    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='ثبت ادمین جدید';

        return view('admin.page.manager.add',compact('response'));
    }

    public function store(Request $request,Admin $admin)
    {

        $permission = Setting::firstWhere([['type','permission'],['id',$request->permission]]);

        if ($permission != null)
        {
            $admin->setMeta(['permission'=>$permission->key],true,$permission->title);

            return redirect()->route('admin.permission.list',$admin->id)->with('success','ادمین با موفقیت اضافه گردید');
        }
        return redirect()->back()->with('warning','عملیات با خطا مواجه گردید');

    }

    public function destroy($permission)
    {
        Meta::firstWhere('id',$permission)->delete();
        return redirect()->back()->with('success','عملیات با موفقیت انجام گردید');
    }


}
