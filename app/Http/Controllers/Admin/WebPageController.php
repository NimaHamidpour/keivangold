<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebPageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='لیست مطالب وب سایت';
        $response['pages']=Page::where('type','page')->orderBy('id','DESC')->paginate(10);

        return view('admin.page.web-page.list',compact('response'));
    }

    public function edit($id)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page']=Page::firstWhere('id',$id);

        $response['page_title']='ویرایش مطلب : '.$response['page']->title;

        return view('admin.page.web-page.edit',compact('response'));
    }

    public function update(Request $request,$id)
    {
        $page = Page::firstWhere('id',$id);

        $request->validate([
            'description' => 'required',
        ]);

        $page->content=$request->description;
        $page->user_id=Auth::user()->id;

        try
        {
            $page->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','مطلب با موفقیت ویرایش گردید.');
    }

}
