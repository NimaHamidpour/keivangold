<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Pay;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Visit;
use Carbon\Carbon;


class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['page_title']='داشبـــورد';


        $response['user_count']=User::all()->count();
        $response['paid_count']=Pay::where('refid','<>',0)->get()->count();
        $response['unpaid_count']=Pay::where('refid',0)->get()->count();
        $response['blog_count']=Page::where('type','blog')->get()->count();
        $response['unread-message']=Ticket::where('status','admin-answer')->get()->count();
        $response['portfo_count']=Portfo::where('end_date',null)->get()->count();


        //  VIEWS
        for ($i=0;$i < 11 ;$i++)
        {
            $response['view_count'][$i]=Visit::whereDate('created_at',Carbon::now()->subDays($i)
                ->toDateString())->get()->count();
            $response['view_date'][$i]=jdate(Carbon::now()->subDays($i)->toDateString())->format('Y/m/d');
        }
        $response['view_all']=Visit::all()->count();

        $response['max_view_count']=max($response['view_count']);

        return view('admin.page.dashboard.dashboard',compact('response'));
    }

}
