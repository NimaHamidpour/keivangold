<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Meta;
use App\Models\Notification;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Visit;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['type']='blog';
        $response['page_title']='لیست اطلاعیه ها';
        $response['notification']=Notification::orderBy('id','DESC')->paginate(10);

        return view('admin.page.notification.list',compact('response'));
    }


    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='اضافه کردن اطلاعیه جدید';


        return view('admin.page.notification.add',compact('response'));
    }


    public function store(Request $request)
    {

        $request->validate
                ([
                    'title'=>'required|unique:pages',
                    'description' => 'required',
                ]);


        $notification = new Notification();
        $notification->title    = $request->title;
        $notification->content  = $request->description;
        $notification->writer  = Auth::user()->id;

        try
        {
            $notification->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->route('admin.notification.list')->with('success','اطلاعیه جدید با موفقیت اضافه گردید.');
    }


    public function edit(Notification $notification)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='ویرایش اطلاعیه : '.$notification->title;

        $response['notification'] = $notification;

        return view('admin.page.notification.edit',compact('response'));
    }


    public function update(Request $request,Notification $notification)
    {

        $request->validate([
            'title'=>'required',
            'description' => 'required',

        ]);

        $notification->title=$request->title;
        $notification->content=$request->description;
        $notification->writer  = Auth::user()->id;

        try
        {
            $notification->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','اطلاعیه با موفقیت ویرایش گردید.');
    }


    public function destroy(Notification $notification)
    {
        Visit::where([['model','Notification'],['model_id',$notification->id]])->delete();


        try
        {
            $notification->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','مطلب با موفقیت حذف شد.');

    }
}
