<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\TradeList;
use Illuminate\Http\Request;



class OnlineTradeSettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function tradeEdit()
    {

        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;

        $response['page_title'] = 'تنظیمات معاملات نقدی';


        $response['trade_start_time'] = Setting::firstWhere('key','trade_start_time');
        $response['trade_end_time'] = Setting::firstWhere('key','trade_end_time');
        $response['trade_close'] = Setting::firstWhere('key','trade_close');

        return view('admin.page.trade.trade-setting', compact('response'));
    }

    public function amountEdit()
    {

        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;

        $response['page_title'] = 'تنظیمات معاملات نقدی';


        $response['trade_last_amount'] = Setting::firstWhere('key','trade_last_amount');
        $response['trade_max_dispute'] = Setting::firstWhere('key','trade_max_dispute');
        $response['trade_online_amount'] = Setting::firstWhere('key','trade_online_amount');
        $response['trade_active_online_api'] = Setting::firstWhere('key','trade_active_online_api');
        $response['trade_active_online_api_show'] = Setting::firstWhere('key','trade_active_online_api_show');

        return view('admin.page.trade.amount-setting', compact('response'));
    }


    public function update(Request $request)
    {

        // API online amount
        if ($request->trade_online_amount !== null && is_numeric($request->trade_online_amount))
        {
            $trade_online_amount        = Setting::firstWhere('key','trade_online_amount');
            $trade_online_amount->value =  str_replace('.','',$request->trade_online_amount);
            $trade_online_amount->save();
        }


        // API active
        if ($request->trade_active_online_api !== null)
        {
            $trade_active_online_api        = Setting::firstWhere('key','trade_active_online_api');
            $trade_active_online_api->value =  $request->trade_active_online_api;
            $trade_active_online_api->save();
        }

        // SHOW API
        if ($request->trade_active_online_api_show !== null)
        {
            $trade_active_online_api_show        = Setting::firstWhere('key','trade_active_online_api_show');
            $trade_active_online_api_show->value =  $request->trade_active_online_api_show;
            $trade_active_online_api_show->save();
        }


        // update last amount for trade
        if ($request->trade_last_amount !== null && is_numeric($request->trade_last_amount) &&  $request->trade_last_amount > 3500000 && $request->trade_last_amount < 8000000)
        {
            $trade_last_amount          = Setting::firstWhere('key','trade_last_amount');
            $trade_last_amount->value   =  str_replace('.','',$request->trade_last_amount);
            $trade_last_amount->save();
        }



        // update start time for trade
        if ($request->trade_start_time !== null)
        {
            $trade_start_time        = Setting::firstWhere('key','trade_start_time');
            $trade_start_time->value =  $request->trade_start_time;
            $trade_start_time->save();
        }



        // update end time for trade
        if ($request->trade_end_time !== null)
        {
            $trade_end_time         = Setting::firstWhere('key','trade_end_time');
            $trade_end_time->value  =  $request->trade_end_time;
            $trade_end_time->save();
        }



        // update max dispute for trade
        if ($request->trade_max_dispute !== null)
        {
            $trade_max_dispute          =  Setting::firstWhere('key','trade_max_dispute');
            $trade_max_dispute->value   =  $request->trade_max_dispute;
            $trade_max_dispute->save();
        }



        // update trade close
        if ($request->trade_close !== null)
        {

            if ($request->trade_close !== 'no')
            {
                // delete old trade list
                TradeList::truncate();
            }

            if ($request->trade_close === 'close')
            {

                $trade_close        =  Setting::firstWhere('key','trade_close');
                $trade_close->value =  'yes';
                $trade_close->save();

                time_sleep_until(time() + 5);
                return  redirect()->route('admin.closeTrade');
            }
            else
            {
                $trade_close        = Setting::firstWhere('key','trade_close');
                $trade_close->value =  $request->trade_close;
                $trade_close->save();
            }

        }


        return  redirect()->back()->with('success','ویرایش تنظیمات معاملات نقدی با موفقیت انجام شد');

    }


}
