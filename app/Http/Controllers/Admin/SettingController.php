<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function edit()
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_email']=optional(Setting::firstWhere('key','app_email'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;

        $response['page_title']='تنـظیمات وب سایت';


        return view('admin.page.setting.setting',compact('response'));
    }

    public function update(Request $request)
    {
        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('storage\setting');
//            $destinationPath = 'storage/setting';
            $img =\Intervention\Image\Facades\Image::make($image->getRealPath());
            $img->fit(200)->save($destinationPath. $input['imagename']);

            /* APP LOGO */
            $app_logo=Setting::firstWhere('key','app_logo');
            if ($app_logo!='')
            {
                $app_logo->value='storage/setting'.$input['imagename'];
            }
            else
            {
                $app_logo=new Setting([
                    'key'=>'app_logo',
                    'value'=>'storage/setting/'.$input['imagename'],
                    'title'=>'لوگو سایت',
                ]);
            }
            $app_logo->save();

            // UPDATE LOGO TO STORAGE/SETTING
        }


        /* APP NAME */
        $app_name=Setting::firstWhere('key','app_name');
        if ($app_name!='')
        {
            $app_name->value=$request->title;
        }
        else
        {
            $app_name=new Setting([
                'key'=>'app_name',
                'value'=>$request->title,
                'title'=>'عنوان سایت',
            ]);
        }
        $app_name->save();


        /* APP ISSUE */
        $app_issue=Setting::firstWhere('key','app_issue');
        if ($app_issue!='')
        {
            $app_issue->value=$request->issue;
        }
        else
        {
            $app_issue=new Setting([
                'key'=>'app_time',
                'value'=>$request->issue,
                'title'=>'موضوع سایت',
            ]);
        }
        $app_issue->save();


        /* APP ADDRESS */
        $app_address=Setting::firstWhere('key','app_address');
        if ($app_address!='')
        {
            $app_address->value=$request->address;
        }
        else
        {
            $app_address=new Setting([
                'key'=>'app_address',
                'value'=>$request->address,
                'title'=>'آدرس',
            ]);
        }
        $app_address->save();



        /* APP TELLS */
        $app_tell=Setting::firstWhere('key','app_tell');
        if ($app_tell!='')
        {
            $app_tell->value=$request->app_tell;
        }
        else
        {
            $app_tell=new Setting([
                'key'=>'app_tell',
                'value'=>$request->app_tell,
                'title'=>'تلفن تماس',
            ]);
        }
        $app_tell->save();



        /* APP EMAIL */
        $app_email=Setting::firstWhere('key','app_email');
        if ($app_email!='')
        {
            $app_email->value=$request->app_email;
        }
        else
        {
            $app_email=new Setting([
                'key'=>'app_email',
                'value'=>$request->app_email,
                'title'=>'ایمیل',
            ]);
        }
        $app_email->save();



        /* APP DESCRIPTION */
        $app_description=Setting::firstWhere('key','app_description');
        if ($app_description!='')
        {
            $app_description->value=$request->description;
        }
        else
        {
            $app_description=new Setting([
                'key'=>'app_description',
                'value'=>$request->description,
                'title'=>'درباره ما',
            ]);
        }
        $app_description->save();




        /* APP SHORT TEXT */
        $app_short_text=Setting::firstWhere('key','app_short_text');
        if ($app_short_text!='')
        {
            $app_short_text->value=$request->short_text;
        }
        else
        {
            $app_short_text=new Setting([
                'key'=>'app_short_text',
                'value'=>$request->short_text,
                'title'=>'توضیحات کوتاه',
            ]);
        }
        $app_short_text->save();


        return redirect()->back()->with('ویرایش با موفقیت انجام شد');
    }
}
