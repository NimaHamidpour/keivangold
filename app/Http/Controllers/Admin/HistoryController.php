<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FinancialHistory;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(User $user)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['user']=$user;

        $response['page_title']='تاریخچه مالی';


        $response['history']=FinancialHistory::where([['user',$user->id],['key','wallet']])->orderBy('id','DESC')->paginate(15);

        return view('admin.page.user.history',compact('response'));
    }




}
