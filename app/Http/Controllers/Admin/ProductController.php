<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['category']=Category::where('type','product')->orderBy('title','ASC')->get();
        $response['products']=Product::orderBy('id','DESC')->paginate(15);
        $response['page_title']='لیست نمونه کارها';

        return view('admin.page.product.list',compact('response'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'poster'=>'required|image|max:2024',
            'title'=>'required',
            'weight'=>'required',
            'description'=>'required',
        ]);


        $product    =  new Product();
        $product->title       = $request->title;
        $product->weight      = $request->weight;
        $product->description = $request->description;
        $product->category = $request->category;


        try
        {
            $product->save();
        }
        catch (Exception $exception)
        {
            return  redirect()->back()->with('warning','عملیات با موفقیت انجام نشد.لطفا مجددا تلاش کنید');
        }

        return redirect()->back()->with('success','ایتم با موفقیت اضافه شد.');
    }


    public function destroy($id)
    {
        $image=Product::firstWhere('id',$id);
        $image->delete();
        return redirect()->back()->with('تصویر با موفقیت حذف گردید');

    }

}
