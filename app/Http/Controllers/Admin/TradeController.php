<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Trade;
use App\Models\TradeList;
use Illuminate\Http\Request;


class TradeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='لیست معاملات باز ';

        $response['unit'] = Setting::firstWhere('key','trade_unit')->value;
        $response['trade_last_amount'] = Setting::firstWhere('key','trade_last_amount')->value;


        $response['trade'] = Trade::where('status','open');


        if (isset($request->trade) && $request->trade !== null)
        {
            $response['trade'] = $response['trade']->where('id',$request->trade);
            $response['trade_id'] = $request->trade;
        }


        if (isset($request->type) && $request->type === 'seller')
        {
            $response['trade'] = $response['trade']->where('type',$request->type);
            $response['trade_type'] = 'seller';
        }
        else
        {
            $response['trade'] = $response['trade']->where('type','buyer');
            $response['trade_type'] = 'buyer';
        }



        if (isset($request->sort) && $request->sort === 'profit')
        {
            if ($response['trade_type'] === 'buyer')
            {
                $response['trade'] = $response['trade']->orderBy('amount','ASC');
            }
            else
            {
                $response['trade'] = $response['trade']->orderBy('amount','DESC');
            }

            $response['trade_sort'] = 'profit';
        }
        else
        {

            if ($response['trade_type'] === 'buyer')
            {
                $response['trade'] = $response['trade']->orderBy('amount','DESC');
            }
            else
            {
                $response['trade'] = $response['trade']->orderBy('amount','ASC');
            }

            $response['trade_sort'] = 'loss';
        }


        $response['trade'] = $response['trade']->paginate(50);

        return view('admin.page.trade.list',compact('response'));
    }

    public function onSale()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='لیست معاملات حراجی ';
        $response['unit'] = Setting::firstWhere('key','trade_unit')->value;


        $response['onsale_trade'] = TradeList::where('parent_trade','<>',null)->orderBy('id','DESC')->paginate(25);


        return view('admin.page.trade.onSale',compact('response'));
    }
}
