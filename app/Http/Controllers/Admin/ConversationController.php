<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Meta;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ConversationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $request->validate([
            'message' => 'required|min:5',
            'Capcha' => 'required|captcha',
        ]);


        $conversation = new Conversation();

        $conversation->writer = 'Admin';
        $conversation->writer_id = Auth::user()->id;
        $conversation->message = $request->message;
        $conversation->model = $request->parentType;
        $conversation->model_id = $request->parent;

        $conversation->save();

        // SAVE ATTACHMENT
        foreach ($request->files as $file)
        {
            foreach ($file as $row => $item) {
                if (++$row < 2) {

                    $path = strtolower($item->getClientOriginalExtension());

                    if ($path === 'png' || $path === 'jpg' || $path === 'jpeg') {

                        $destinationPath = 'storage/upload/';
                        $filename = time() . rand(0, 999999) . '.' . $item->getClientOriginalExtension();

                        $img = Image::make($item->getRealPath())->resize(300, 300);
                        $img->save($destinationPath . $filename);


                        $meta = new Meta();
                        $meta->key = 'file';
                        $meta->unique = 'img';
                        $meta->value = 'storage/upload/' . $filename;
                        $meta->model = 'Conversation';
                        $meta->model_id = $conversation->id;
                        $meta->save();
                    }
                }
            }


        }

        // CHECK IF TICKET CHANGE STATUS TO ADMIN ASNWER
        if ($conversation->model === 'Ticket')
        {
            $ticket = Ticket::firstWhere('id', $conversation->model_id);
            $ticket->status = 'user-answer';
            $ticket->save();
        }


        return redirect()->back()->with('success', 'پیام با موفقیت ارسال شد');
    }

}
