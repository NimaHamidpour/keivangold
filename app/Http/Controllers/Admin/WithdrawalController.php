<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Export;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Morilog\Jalali\Jalalian;


class WithdrawalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='لیست برداشت از حساب';

        $response['withdrawal']=Withdrawal::orderBy('id','DESC');

        if (isset($request->status) && $request->status !== null && $request->status !== 'all')
        {
            $response['withdrawal'] =  $response['withdrawal']->where('status',$request->status);

            $response['status'] = $request->status;
            session()->put('excell_status', $request->status);
        }
        else
        {
            session()->put('excell_status',null);
        }

        if (isset($request->mobile) && $request->mobile !== null)
        {
            $user = User::where('mobile','Like','%'.$request->mobile.'%')->get()->pluck('id')->toArray();

            $response['withdrawal'] =  $response['withdrawal']->whereIn('user',$user);

            $response['mobile'] = $request->mobile;
            session()->put('excell_mobile', $request->mobile);
        }
        else
        {
            session()->put('excell_mobile',null);
        }


        // WHERE START_DATE
        if (isset($request->start_date) && $request->start_date !== null)
        {
            $response['withdrawal'] =  $response['withdrawal']->whereDate('created_at','>=',Jalalian::forge($request->start_date)->toCarbon()->toDateString());

            $response['start_date'] = $request->start_date;
            session()->put('excell_start_date', $request->start_date);
        }
        else
        {
            session()->put('excell_start_date',null);
        }


        // WHERE END_DATE
        if (isset($request->end_date) && $request->end_date !== null)
        {
            $response['withdrawal'] =  $response['withdrawal']->whereDate('created_at','<=',Jalalian::forge($request->end_date)->toCarbon()->toDateString());

            $response['end_date'] = $request->end_date;
            session()->put('excell_end_date', $request->end_date);
        }
        else
        {
            session()->put('excell_end_date',null);
        }

        $response['withdrawal'] = $response['withdrawal']->paginate(50);

        return view('admin.page.pay.withdrawal',compact('response'));
    }

    public function update(Withdrawal $withdrawal)
    {
        if ($withdrawal->status === 'waiting')
            $withdrawal->status = 'success';
        else
            $withdrawal->status = 'waiting';

        $withdrawal->save();
        return redirect()->back()->with('success','درخواست  با موفقیت ویرایش شد');
    }


    public function excell()
    {

        $excell_status = session()->get('excell_status');
        $excell_mobile = session()->get('excell_mobile');
        $excell_start_date = session()->get('excell_start_date');
        $excell_end_date = session()->get('excell_end_date');

        $withdrawals = Withdrawal::orderBy('id','DESC');


        if (isset($excell_status) && $excell_status !== null && $excell_status !== 'all')
        {
            $withdrawals = $withdrawals->where('status',$excell_status);
        }

        if (isset($excell_mobile) && $excell_mobile !== null)
        {
            $user = User::where('mobile','Like','%'.$excell_mobile.'%')->get()->pluck('id')->toArray();
            $withdrawals = $withdrawals->whereIn('user',$user);
        }

        if (isset($excell_start_date) && $excell_start_date !== null)
        {
            $withdrawals = $withdrawals->whereDate('created_at','>=',Jalalian::forge($excell_start_date)->toCarbon()->toDateString());
        }

        if (isset($excell_end_date) && $excell_end_date !== null)
        {
            $withdrawals = $withdrawals->whereDate('created_at','<=',Jalalian::forge($excell_end_date)->toCarbon()->toDateString());
        }

        $withdrawals = $withdrawals->get();

        $customer_array[] = array('نام','کد ملی','موبایل',
            'شماره شبا','نام بانک','کیف پول',
            'مبلغ درخواست','تاریخ  درخواست');
        foreach($withdrawals as $withdrawal)
        {

            $customer_array[] = array(
                'نام'               => $withdrawal->User->name,
                'کد ملی'            => $withdrawal->User->codemeli,
                'موبایل'            => $withdrawal->User->mobile,
                'شماره شبا'         => $withdrawal->User->sheba,
                'نام بانک'          => $withdrawal->User->bank,
                'کیف پول'           => $withdrawal->User->wallet,
                'مبلغ درخواست'      => $withdrawal->amount,
                'تاریخ  درخواست'    =>jdate($withdrawal->created_at)->format('Y/m/d'),

            );
        }
        return Excel::download(new Export($customer_array), 'withdrawal.xlsx');
    }
}
