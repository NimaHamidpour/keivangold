<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FinancialHistory;
use App\Models\Pay;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Trade;
use App\Models\TradeList;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;

class PayController extends Controller
{

    public function index($type)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        if ($type === 'paid')
        {
            $response['page_title']='لیست موارد پرداخت شده';

            $response['pays']=Pay::where('refid','<>',0)->orderBy('id','DESC')->paginate(15);

        }

        if ($type === 'unpaid')
        {
            $response['page_title']='لیست موارد پرداخت نشده';

            $response['pays']=Pay::where('refid',0)->orderBy('id','DESC')->paginate(15);
        }

        return view('admin.page.pay.list',compact('response'));

    }

    public function information(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title'] = 'اطلاعات آماری';

        $response['success_pay'] = Pay::where('refid','<>',0)->get();

        $response['success_pay_to_user'] = Withdrawal::where('status','success')->get();
        $response['waiting_pay_to_user'] = Withdrawal::where('status','waiting')->get();

        $response['wallet'] = User::where('wallet','>',0)->get();

        $response['portfo'] = Portfo::where('end_date',null)->get();

        $response['wage']  = FinancialHistory::where('model','Trade')->get();

        $response['open_trade']  = Trade::where('status','open')->get();
        $response['onsale_trade'] = TradeList::where('parent_trade','<>',null)->get();
        return view('admin.page.pay.info',compact('response'));

    }
}
