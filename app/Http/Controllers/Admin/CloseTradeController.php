<?php

namespace App\Http\Controllers\Admin;

use App\CustomClass\Financial;
use App\Http\Controllers\Controller;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Trade;
use App\Models\TradeList;
use App\Models\User;
use Carbon\Carbon;


class CloseTradeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');

    }


    /* بستن معاملات بر اساس آخرین قیمت  */
    public function closeTrade()
    {

        $unit = Setting::firstWhere('key','trade_unit')->value;

        /*  قیمت  آحرین معامله  */
        $trade_last_amount = (int) Setting::firstWhere('key','trade_last_amount')->value;


        /*  لیست پرتفوهای باز  */
        $portfos = Portfo::where('end_date', null)->orderBy('id','ASC')->get();

        foreach ($portfos as $portfo)
        {
            /*  لیست معاملات باز  */
            $trades = Trade::where([['status','open'],['portfo',$portfo->id]])->orderBy('id','ASC')->get();

            /*  محاسبه سود و زیان هر معامله  */
            foreach ($trades as $trade)
            {

                $profit = abs((($trade_last_amount - $trade->amount) / 1000) * ($unit * 230) * $trade->weight);

                if ($trade->type === 'buyer')
                {
                    if ($trade->amount > $trade_last_amount)
                        $profit = $profit * -1;
                }
                else
                {
                    if ($trade->amount < $trade_last_amount)
                        $profit = $profit * -1;
                }

                $trade->profit      = $profit;
                $trade->status      = 'close';
                $trade->end_type    = 'system';
                $trade->end_amount  = $trade_last_amount;

                $trade->save();

                /*  محاسبه کارمزد   */
                $this->wage($portfo,$trade->weight,$trade->id);
            }


            $this->calculateProfit($portfo);
        }


        // delete old trade list
        TradeList::truncate();

        return redirect()->route('admin.pay.info')->with('success','کلیه معاملات تسویه حساب گردید');

    }


    /*  محاسبه سود و واحدهای قابل معامله  */
    public function calculateProfit(Portfo $portfo)
    {
        $unit = Setting::firstWhere('key','trade_unit')->value;

        $close_trades  = Trade::where([['portfo',$portfo->id],['status','close']])
                         ->orderBy('id','ASC')->get();

        if (count($close_trades) > 0)
        {
            $profit = $close_trades->sum('profit');

            /*  سود و زیان این دوره را به دست آورده و دوره قبل از آن کم می وشد و با مبلغ جمع شده  */
            $portfo->amount += $profit - $portfo->profit;

            /*  برابر سود و زیان این دوره قرار داده میشود  */
            $portfo->profit  = $profit;

            if ($portfo->amount > 0)
            {
                $weight = floor($portfo->amount / ($portfo->lever * ($unit * 230)));
                $portfo->weight      = $weight;
                $portfo->weight_buy  = $weight;
                $portfo->weight_sell = $weight;
                $portfo->save();
            }
            else
            {
                $portfo->weight      = 0;
                $portfo->weight_buy  = 0;
                $portfo->weight_sell = 0;
            }

            $portfo->save();
        }

        /*  بستن معامله */
        $portfo->end_date = Carbon::now()->format('Y-m-d H:i:s');
        $portfo->save();


        /*  واریز پول به کیف پول */
        $user = User::firstWhere('id',$portfo->user);
        $user->wallet +=$portfo->amount;
        $user->save();

        // Log history
        Financial::history($portfo->user,$portfo->amount,'Portfo',$portfo->id,'wallet','yes','yes');

    }


    /*  محاسبه کارمزد  */
    public  function wage(Portfo $portfo,$weight,$trade)
    {

        $unit = Setting::firstWhere('key','trade_unit')->value;

        $wage = $weight * ($unit * 120);

        $portfo->amount -= $wage;
        $portfo->save();

        // Log history
        Financial::history($portfo->user,$wage,'Trade',$trade,'trade','no','yes');

    }
}


