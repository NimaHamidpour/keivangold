<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;

class VerifyUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='لیست کاربران تایید نشده';

        $response['users'] = User::where([['mobile_verified_at','<>',null],['active','waiting']])->paginate(50);


        return view('admin.page.user.activeList',compact('response'));
    }

    public function edit(User $user)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='ویرایش وضعیت کاربر : '.$user->name;

        $response['user'] = $user;


        return view('admin.page.user.activeDetail',compact('response'));
    }

    public function update(Request $request,User $user)
    {
        $user->active = $request->active;
        $user->save();

        return redirect()->back();
    }

}
