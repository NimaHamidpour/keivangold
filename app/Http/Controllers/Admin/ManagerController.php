<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class ManagerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['admins']=Admin::where('active','yes')->orderBy('id','DESC')->paginate(15);

        $response['page_title']='لیست ادمین';

        return view('admin.page.manager.list',compact('response'));
    }

    public function create()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['page_title']='ثبت ادمین جدید';

        $response['role'] = Setting::where('type','role')->orderBy('value','asc')->get();
        $response['chart'] = Setting::where('type','chart')->orderBy('value','asc')->get();
        return view('admin.page.manager.add',compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'mobile'=>'required|unique:admins',
            'password'=>'required|confirmed',
        ]);

        $admin=new Admin();
        $admin->name        =   $request->name;
        $admin->mobile      =   $request->mobile;
        $admin->role        =   'admin';
        $admin->password    =   Hash::make($request->password);

        try
        {
            $admin->save();
        }
        catch (\Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getMessage());
        }

        return redirect()->route('admin.manager.list')->with('success','ادمین با موفقیت اضافه گردید');
    }

    public function destroy(Admin $admin)
    {
        $admin->active = 'no';
        $admin->save();
        return redirect()->back();
    }


}
