<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Meta;
use App\Models\Page;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Visit;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['type']='blog';
        $response['page_title']='لیست مطالب منتشر شده';
        $response['pages']=Page::where([['parent_model','Category'],['type','blog']])->orderBy('id','DESC')->paginate(10);

        return view('admin.page.page.list',compact('response'));
    }


    public function create($type)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;


        $response['page_title']='اضافه کردن بلاگ جدید';
        $response['parent']=Category::where('type','blog')->get();

        $response['type']=$type;

        return view('admin.page.page.add',compact('response'));
    }


    public function store(Request $request)
    {

        $request->validate([
            'title'=>'required|unique:pages',
            'description' => 'required',
            'thumbnail' => 'required|image|max:2024',
            'category' => 'required',
        ]);


        $page=new Page();

        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('thumbnail')) {

            $image = $request->file('thumbnail');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path('storage\page\img\\');
            $destinationPath = 'storage/page/img/';
            $img = Image::make($image->getRealPath());
            $img->resize(800,400)->save($destinationPath. $input['imagename']);

            $page->img = 'storage/page/img/'.$input['imagename'];
        }


        $page->title = $request->title;
        $page->parent = $request->category;
        $page->parent_model = 'Category';
        $page->content = $request->description;
        $page->type = $request->type;
        $page->user_id = Auth::user()->id;



        try
        {
            $page->save();

        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        if ($page->type === 'blog')
            $page->setMeta(['page'=>$page->id],false,'blog');

        return redirect()->back()->with('success','مطلب جدید با موفقیت اضافه گردید.');
    }


    public function edit($id)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['page_title']='ویرایش مطلب';

        $response['page']=Page::firstWhere('id',$id);

        if ($response['page']->type === 'blog')
            $response['parent']=Category::where('type','blog')->get();

        if ($response['page']->type === 'analysis')
            $response['parent']=Category::where('type','analysis')->get();

        return view('admin.page.page.edit',compact('response'));
    }


    public function update(Request $request,$id)
    {
        $page=Page::firstWhere('id',$id);

        $request->validate([
            'title'=>'required',
            'description' => 'required',
            'category' => 'required',
        ]);

        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('thumbnail'))
        {

            $image = $request->file('thumbnail');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            // $destinationPath = public_path('storage\page\img\\');
            $destinationPath = 'storage/page/img/';
            $img = Image::make($image->getRealPath());
            $img->resize(800,400)->save($destinationPath. $input['imagename']);

            $page->img = 'storage/page/img/'.$input['imagename'];
        }

        $page->title=$request->title;
        $page->parent=$request->category;
        $page->content=$request->description;
        $page->type=$request->type;
        $page->user_id=Auth::user()->id;

        try
        {
            $page->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','مطلب با موفقیت ویرایش گردید.');
    }


    public function destroy($id)
    {
        try
        {
            Page::firstWhere('id',$id)->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }


        Visit::where([['model','Page'],['model_id',$id]])->delete();
        Meta::where([['model','Page'],['model_id',$id]])->delete();

        return redirect()->back()->with('success','مطلب با موفقیت حذف شد.');

    }
}
