<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Export;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Trade;
use App\Models\TradeList;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;



        $response['page_title']='لیست کاربران';

        $user=User::orderBy('id','DESC');

        $response['paginate_search'] = null;

        if (isset($request->name) && $request->name !== null)
        {
            $user=$user->where('name','LIKE','%'.$request->name.'%');

            session()->put('excell_name', $request->name);
            $response['user_name'] = $request->name;
        }


        if (isset($request->mobile) && $request->mobile !== null)
        {
            $user=$user->where('mobile','LIKE','%'.$request->mobile.'%');

            session()->put('excell_mobile', $request->mobile);
            $response['user_mobile']  =  $request->mobile;
        }

        if (isset($request->friendShip) && $request->friendShip !== null)
        {
            $user=$user->where('friendShip',$request->friendShip);

            session()->put('excell_friendShip', $request->friendShip);
            $response['user_friendShip'] = $request->friendShip;
        }


        $response['users']=$user->paginate(50);

        return view('admin.page.user.list',compact('response'));
    }

    public function show(User $user)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['user']=$user;

        $response['ticket']=Ticket::where('user',$user->id)->orderBy('created_at','DESC')->paginate(20);

        $response['portfo'] = Portfo::where('user',$user->id)->orderBy('id', 'DESC')->paginate(20);

        $response['openTrade'] = Portfo::where([['user',$user->id],['end_date',null]])->get()->pluck('id')->toArray();
        $response['page_title']='مشخصات  : '.$user->name;

        return view('admin.page.user.detail',compact('response'));
    }

    public function status(User $user)
    {
        if ($user->status==='active')
            $user->status='deactive';
         else
            $user->status='active';
        $user->save();
        return redirect()->back();
    }


    public function destroy(User $user)
    {
        /* delete from ticket */
        Ticket::where('user',$user->id)->delete();

        /* delete from portfo,trade,tradeList */
        $portfo = Portfo::where('user',$user->id)->get()->pluck('id')->toArray();
        Trade::whereIn('portfo',$portfo)->delete();
        TradeList::whereIn('portfo',$portfo)->delete();
        Portfo::where('user',$user->id)->delete();

        /* delete from withdrawal and pay */
        Withdrawal::where('user',$user->id)->delete();
        Pay::where('user',$user->id)->delete();

        /* delete selected user */
        $user->delete();

        return redirect()->back()->with('success','کاربر با موفقیت حذف گردید');
    }




    public function excell()
    {

        $excell_name = session()->get('excell_name');
        $excell_mobile = session()->get('excell_mobile');
        $excell_friendShip = session()->get('excell_friendShip');

        $user=User::orderBy('id','DESC');

        if (isset($excell_name) && $excell_name !== null)
        {
            $user = $user->where('name','LIKE','%'.$excell_name.'%');
        }

        if (isset($excell_mobile) && $excell_mobile !== null)
        {
            $user = $user->where('mobile','LIKE','%'.$excell_mobile.'%');
        }

        if (isset($excell_friendShip) && $excell_friendShip !== null)
        {
            $user = $user->where('friendShip',$excell_friendShip);
        }

        $users = $user->get();

        $customer_array[] = array('نام','کد ملی','موبایل','راه آشنایی',
                                  'شماره شبا','نام بانک','کیف پول',
                                  'تاریخ ثبت نام');
        foreach($users as $user)
        {
            if($user->friendShip === 'instagram')
                $frienShip = 'اینستاگرام';
            elseif($user->friendShip === 'telegram')
                $frienShip = 'تلگرام';
            elseif ($user->friendShip === 'aparat')
                $frienShip = 'آپارات';
            elseif($user->friendShip === 'website')
                $frienShip = 'وب سایت';
            elseif($user->friendShip === 'family')
                $frienShip = 'آشنایی قبلی';
            else
                $frienShip = 'راه های دیگر';


            $customer_array[] = array(
                'نام'        => $user->name,
                'کد ملی'     => $user->codemeli,
                'موبایل'     => $user->mobile,
                'راه آشنایی' => $frienShip,
                'شماره شبا'  => $user->sheba,
                'نام بانک'   => $user->bank,
                'کیف پول'    => $user->wallet,
                'تاریخ ثبت نام'=>jdate($user->created_at)->format('Y/m/d'),

            );
        }
        return Excel::download(new Export($customer_array), 'user.xlsx');
    }
}
