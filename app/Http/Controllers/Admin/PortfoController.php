<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Portfo;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\Trade;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;


class PortfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $response = [];
        $response['app_name'] = optional(Setting::firstWhere('key', 'app_name'))->value;
        $response['app_logo'] = optional(Setting::firstWhere('key', 'app_logo'))->value;
        $response['unread_message'] = Ticket::where([['user', Auth::user()->id], ['status', 'user-answer']])->count();

        $response['page_title']='لیست پرتفو کاربران';


        $response['portfo'] = Portfo::orderBy('id', 'DESC');

        if ($request->mobile !== null)
        {
            $user = User::where('mobile','LIKE','%'.$request->mobile.'%')->get()->pluck('id')->toArray();
            $response['portfo']=$response['portfo']->whereIn('user',$user);

            $response['user_mobile'] = $request->mobile;
        }

        if ($request->portfo !== null)
        {
            $response['portfo']=$response['portfo']->where('id',$request->portfo);

            $response['portfo_id'] = $request->portfo;
        }

        if ($request->name !== null)
        {
            $user = User::where('name','LIKE','%'.$request->name.'%')->get()->pluck('id')->toArray();
            $response['portfo']=$response['portfo']->whereIn('user',$user);

            $response['user_name'] = $request->name;
        }
        // WHERE START_DATE
        if (isset($request->start_date) && $request->start_date !== null)
        {
            $response['portfo']=$response['portfo']->whereDate('created_at','>=',Jalalian::forge($request->start_date)->toCarbon()->toDateString());
        }

        // WHERE END_DATE
        if (isset($request->end_date) && $request->end_date !== null)
        {
            $response['portfo']=$response['portfo']->whereDate('created_at','<',Jalalian::forge($request->end_date)->toCarbon()->toDateString());
        }

        if (isset($request->status) && $request->status !== null && $request->status !== 'all')
        {
            /* پرتفوهای بسته */
            if ($request->status === 'close')
            {
                $response['portfo']=$response['portfo']->where('end_date','<>',null);
            }
            else if ($request->status === 'openWithProgress')
            {
                /* پرتفوهای باز و در جریان */
                $portfo = Trade::where('status','open')->get()->pluck('portfo')->toArray();
                $response['portfo']=$response['portfo']->whereIn('id',$portfo);
            }
            else
            {
                /* پرتفوهای باز */
                $response['portfo']=$response['portfo']->where('end_date',null);
            }

            $response['status'] = $request->status;
        }
        $response['portfo']=$response['portfo']->paginate(50);


        $response['openTrade'] = Trade::where('status','open')->get()->pluck('portfo')->toArray();
        return view('admin.page.portfo.list', compact('response'));
    }

    /*  لیست معاملات انجام شده هر شخص بر اسا شماره پرتفو  */
    public function trade(Portfo $portfo)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['page_title']='لیست معاملات پرتفو شماره : '.$portfo->id;

        $response['trade'] = Trade::where('portfo',$portfo->id)->orderBy('id','DESC')->get();
        $response['unit']  = Setting::firstWhere('key','trade_unit')->value;

        return view('admin.page.portfo.trade',compact('response'));
    }


}
