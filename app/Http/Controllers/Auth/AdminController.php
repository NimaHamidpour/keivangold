<?php

namespace App\Http\Controllers\Auth;

use App\CustomClass\SendSms;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Kavenegar\KavenegarApi;

class AdminController extends Controller
{

    //***   login  ***/
    public function login_get_mobile()
    {
        $response=[];
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        return view('auth.admin.login.get-mobile',compact('response'));
    }
    public function login_send_code(Request $request)
    {
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;

        $request->validate([
            'mobile'=>'required|digits:11',
            'Capcha'=>'required|captcha',
        ]);


        if (Auth::guard('admin')->attempt(['mobile'=>$request->mobile,'password'=>$request->password,'active'=>'yes']))
        {
            Auth::guard('admin')->logout();

            $admin = Admin::firstWhere('mobile',$request->mobile);
            $code = rand(10000, 99999);

            $admin->deleteMeta('password_code');
            $admin->setMeta(['password_code' => $code ]);


            SendSms::verify($admin->mobile,$admin->name,$code);

            return redirect()->route('admin.login.verify',$admin->mobile)->with('success','کلمه عبور یک بار مصرف به شماره تماس شما ارسال گردید');
        }
        else
        {
            $admin = Admin::firstWhere('mobile',$request->mobile);
            if($admin !== null)
                 $admin->setMeta(['login' => 'faild'], true,  \Request::getClientIp());

            return redirect()->back()->with('warning','نام کاربری یا کلمه عبور وارد شده معتبر نمی باشد . لطفا دوباره امتحان کنید');
        }

    }
    public function login_get_code($mobile)
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['mobile'] = $mobile;

        return view('auth.admin.login.verify',compact('response'));
    }
    public function login_verify(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
            'token_code' => 'required|digits:5',
            'Capcha'=>'required|captcha',
        ]);

        if (isset($request->mobile) && $request->mobile !== null)
        {
            $admin = Admin::firstWhere('mobile', $request->mobile);


            if ((int)$admin->getMeta('password_code') === (int)$request->token_code)
            {
                $admin->deleteMeta('password_code');
                Auth::guard('admin')->login($admin);
                $admin->setMeta(['login' => 'success'], true,  \Request::getClientIp());

                return redirect()->intended(route('admin.dashboard'));
            }
            else
            {

                $admin->setMeta(['login' => 'faild'], true,  \Request::getClientIp());
                return redirect()->back()->with('warning', 'کلمه عبور یکبار مصرف اشتباه وارد شده است.لطفا دوباره تلاش کنید');
            }
        }
        else
        {

            return redirect()->back()->with('warning', 'شماره تلفن وارد شده اشتباه هست.لطفا دوباره تلاش کنید');
        }

    }




    //***   forget  ***/
    public function forgetform()
    {
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        return view('auth.admin.forget',compact('response'));
    }
    public function forget(Request $request)
    {
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;

        $request->validate([
            'mobile'=>'required|digits:11',
        ]);

        $admin=Admin::firstWhere('mobile',$request->mobile);
        if ($admin !== null)
        {
            $code = rand(10000, 99999);
            $admin->deleteMeta('password_reset_code');
            $admin->setMeta(['password_reset_code' => $code ]);

       
     SendSms::verify($admin->mobile,$admin->name,$code);
            return redirect()->route('admin.reset.form',$admin->mobile)->with('success','کلمه عبور موقت به شماره تماس شما ارسال گردید');
        }
        else
        {
            return redirect()->back()->with('warning','شماره وارد شده معتبر نمی باشد . لطفا دوباره امتحان کنید');
        }

    }



    //***   reset  ***/
    public function resetform($mobile)
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['mobile']=$mobile;
        return view('auth.admin.reset',compact('response'));
    }
    public function reset(Request $request)
    {
        $request->validate([
            'token_code' => 'required|digits:5',
            'password' => 'required|string|confirmed|min:5|max:191'
        ]);

        if (isset($request->mobile) && $request->mobile !== null)
        {
            $admin = Admin::firstWhere('mobile',$request->mobile);
        }

        abort_if((int) $admin->getMeta('password_reset_code') !== (int) $request->token_code, 400, trans('passwords.token'));

        $admin->deleteMeta('password_reset_code');


        $admin->password=Hash::make($request->password);

        $admin->save();

        return redirect()->route('admin.login.show')->with('success','کلمه عبور با موفقیت تغییر یافت');
    }

}
