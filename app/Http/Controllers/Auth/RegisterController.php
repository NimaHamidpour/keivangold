<?php

namespace App\Http\Controllers\Auth;

use App\CustomClass\PersianNum;
use App\CustomClass\SendSms;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $data['mobile']   = PersianNum::convert($data['mobile']);
        $data['tell']   = PersianNum::convert($data['tell']);
        $data['codemeli']   = PersianNum::convert($data['codemeli']);
        $data['password']   = PersianNum::convert($data['password']);
        $data['password_confirmation']   = PersianNum::convert($data['password_confirmation']);




        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'numeric', 'digits:11', 'unique:users'],
            'tell' => ['required', 'numeric', 'unique:users'],
            'codemeli' => ['required', 'numeric', 'digits:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'rule' => ['required'],
            'friendShip' => ['required'],
        ]);
    }


    protected function create(array $data)
    {
        $data['mobile']                  = PersianNum::convert($data['mobile']);
        $data['tell']                    = PersianNum::convert($data['tell']);
        $data['codemeli']                = PersianNum::convert($data['codemeli']);
        $data['password']                = PersianNum::convert($data['password']);
        $data['password_confirmation']   = PersianNum::convert($data['password_confirmation']);

        $user                = new User();
        $user->name          = $data['name'];
        $user->mobile        = $data['mobile'];
        $user->tell          = $data['tell'];
        $user->friendShip    = $data['friendShip'];
        $user->codemeli      = $data['codemeli'];
        $user->password      = Hash::make($data['password']);

        $user->save();

        SendSms::welcome($user->mobile,$user->name);

        return $user;
    }


}
