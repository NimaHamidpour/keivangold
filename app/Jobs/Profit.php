<?php

namespace App\Jobs;

use App\Models\Setting;
use App\Models\Trade;
use App\Models\TradeList;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Profit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public function __construct()
    {
        //
    }


    public function handle()
    {

        /*  قیمت آخرین معامله  */
        $trade_last_amount = optional(Setting::firstWhere('key','trade_last_amount'))->value ;

        if($trade_last_amount !== null)
        {

            $unit = Setting::firstWhere('key','trade_unit')->value;

            /*  لیست معاملات باز  */
            $trades = Trade::where('status', 'open')->orderBy('id', 'ASC')->get();

            /*  محاسبه سود و زیان هر معامله  */
            foreach ($trades as $trade)
            {

                $profit = abs((($trade_last_amount - $trade->amount) / 1000) * ($unit * 230) * $trade->weight);

                if ($trade->type === 'buyer')
                {
                    if ($trade->amount > $trade_last_amount)
                        $profit = $profit * -1;
                }
                else
                {
                    if ($trade->amount < $trade_last_amount)
                        $profit = $profit * -1;
                }

                $trade->profit      = $profit;
                $trade->timestamps  = false;
                $trade->save();

                /* بررسی اینکه حراج بشود یا نه */
                $this->checkProfit($trade);

            }
        }

        return true;

    }


    /* بررسی اینکه حراج بشود یا نه */
    public function checkProfit(Trade $trade)
    {

        $unit = Setting::firstWhere('key','trade_unit')->value;

        /*  مشخص شدن اهرم معامله  */
        $lever=$trade->Portfo->lever;

        /*  در صورتی که بیش از 75 درصد ضرر کرد، حراج خواهد شد  */
        if ($trade->profit < 0 && abs($trade->profit) > ($trade->weight * ($lever * ($unit * 165))))
        {

            $check_old_onsale = TradeList::where('parent_trade',$trade->id)->get();

            if (count($check_old_onsale) === 0)
            {

                /*  قیمت آخرین معامله  */
                $trade_last_amount = optional(Setting::firstWhere('key','trade_last_amount'))->value ;


                if($trade_last_amount > 3500000 && $trade_last_amount < 8000000)
                {
                    /*  مشخص کننده نوع معامله متضرر شده : خریدار یا فروشنده  */
                    /*  حراج معامله با 4 خط اختلاف  */
                    if ($trade->type === 'buyer')
                    {
                        $type   = 'seller';
                        $amount =  $trade_last_amount - 4000;
                    }
                    else
                    {
                        $type   = 'buyer';
                        $amount = $trade_last_amount  + 4000;
                    }

                    /*  حراج معامله  */
                    $tradeList                  =    new TradeList();
                    $tradeList->portfo          =    $trade->portfo;
                    $tradeList->weight          =    $trade->weight;
                    $tradeList->type            =    $type;
                    $tradeList->active          =    'yes';
                    $tradeList->amount          =    $amount;
                    $tradeList->parent_trade    =    $trade->id;

                    $tradeList->save();
                }
            }

        }
    }


}
