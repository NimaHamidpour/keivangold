<?php

namespace App\Console;

use App\Jobs\Profit;
use App\Models\TradeList;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [];


    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {

            $dt = Carbon::now();

            /*  پاک سازی درخواست معاملات  50 ثانیه قبل  */
            $dt->subSeconds(35);
            $formatted_date = $dt->format('Y-m-d H:i:s');
            TradeList::where('created_at', '<', $formatted_date)->delete();


            $x = 2;
            $dt = Carbon::now();
            do
            {
                Profit::dispatch()->onQueue('high');
                time_sleep_until($dt->addSeconds(10)->timestamp);

            } while ($x-- > 0);


        })->everyMinute()->withoutOverlapping;


    }


    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
