<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Trade;
use Illuminate\Console\Command;

class Profit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profit:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $trades = Trade::all();

       foreach ($trades as $trade)
       {
           $trade->profit = 15;
           $trade->save();
       }
    }
}
