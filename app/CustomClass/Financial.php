<?php
namespace App\CustomClass;

use App\Models\FinancialHistory;

class Financial
{

    public static function history($user,$amount,$model,$model_id,$key,$income,$paid)
    {
        $history = new FinancialHistory();
        $history->user       = $user;
        $history->amount     = $amount;
        $history->model      = $model;
        $history->model_id   = $model_id;
        $history->key        = $key;
        $history->income     = $income;
        $history->paid       = $paid;
        return $history->save();
    }
}
