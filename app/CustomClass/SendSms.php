<?php
namespace App\CustomClass;

use Kavenegar\KavenegarApi;

class SendSms
{

    public static function send($receptor,$message)
    {

        $SMS = new KavenegarApi('717845564B797961386B576546675430316E5379436F746B70526748644F3538734F4C727775534B716D673D');
        return  $SMS->Send('10008663', $receptor, $message);

    }


    public static function verify($receptor,$user,$code)
    {

        $user = str_replace(' ', '', $user);

        $token = preg_replace('/\s+/', '', $user);
        $token2 = $code;
        $token3 = '';
        $template = 'forgetPassword';
        $type = "sms";
        $SMS = new KavenegarApi('717845564B797961386B576546675430316E5379436F746B70526748644F3538734F4C727775534B716D673D');
        return  $SMS->VerifyLookup($receptor,$token,$token2,$token3,$template,$type);

    }

    public static function welcome($receptor,$user)
    {

        $user = str_replace(' ', '', $user);

        $token = preg_replace('/\s+/', '', $user);
        $token2 = '';
        $token3 = '';
        $template = 'welcome';
        $type = "sms";
        $SMS = new KavenegarApi('717845564B797961386B576546675430316E5379436F746B70526748644F3538734F4C727775534B716D673D');
        return  $SMS->VerifyLookup($receptor,$token,$token2,$token3,$template,$type);

    }


    public static function closeTrade($receptor,$amount)
    {

        $amount = str_replace(' ', '', $amount);

        $token = $amount;
        $token2 = '';
        $token3 = '';
        $template = 'closeTrade';
        $type = "sms";
        $SMS = new KavenegarApi('717845564B797961386B576546675430316E5379436F746B70526748644F3538734F4C727775534B716D673D');
        return  $SMS->VerifyLookup($receptor,$token,$token2,$token3,$template,$type);

    }
}
