<?php
namespace App\CustomClass;


class PersianNum
{

    public static function convert($string) {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);

        return $convertedPersianNums;
    }
}
