<?php

namespace App\Traits;

trait HasRole
{
    /**
     * @return  string|null
     */
    public function role()
    {
        $role = $this->getMeta('role');
        if (is_array($role)) {
            $role = $role[0];
        }
        return $role;
    }

    /**
     * @param  string $role
     *
     * @return  bool
     */
    public function hasRole(string $role)
    {
        return $this->role() === $role;
    }

    /**
     * @param  string $role
     *
     * @return  void
     */
    public function assignRole(string $role)
    {
        if ($role === 'user') {
            $this->deleteMeta('role');
        } else {
            $this->setMeta(['role' => $role]);
        }
    }

    /**
     * @param  string $role
     *
     * @return  bool
     */
    public function removeRole(string $role)
    {
        if ($this->hasRole($role)) {
            return $this->deleteMeta('role');
        }
    }

    /**
     * @return  array
     */
    public function permission()
    {
        return (array) $this->getMeta('permission', true);
    }

    /**
     * @param  string $permission
     *
     * @return  bool
     */
    public function hasPermission(string $permission)
    {
        return in_array($permission, $this->permission());
    }

    /**
     * @param  string $permission
     *
     * @return  void
     */
    public function assignPermission(string $permission)
    {
        $this->setMeta(['permission' => $permission], true);
    }

    /**
     * @param  string $permission
     *
     * @return  bool
     */
    public function removePermission(string $permission)
    {
        if ($this->hasPermission($permission)) {
            return $this->deleteMeta('permission', $permission);
        }
    }
}
