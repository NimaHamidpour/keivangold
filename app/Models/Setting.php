<?php

namespace App\Models;

use App\Traits\HasMeta;
use App\Traits\HasScope;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasScope,HasMeta;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $visible = [
        'key', 'value'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param  string $value
     *
     * @return  void
     */
    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = \Str::slug($value, '_');
    }

    /**
     * @param  string $value
     *
     * @return  void
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = \Str::slug($value, ' ', '');
    }
}
