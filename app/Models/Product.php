<?php

namespace App\Models;

use App\Traits\HasFile;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFile;

    protected $has_file = [
        'name' => 'title',
        'source' => 'poster',
        'image' => [
            'resize' => [300,300]
        ]
    ];

}
