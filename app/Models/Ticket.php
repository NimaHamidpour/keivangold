<?php

namespace App\Models;

use App\Traits\HasMeta;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasMeta;


    public function User()
    {
        return $this->belongsTo('App\Models\User','user','id');
    }
}
