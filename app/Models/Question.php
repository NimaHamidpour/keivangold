<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = false;

    public function Category()
    {
        return $this->belongsTo('App\Models\Category','category','id');
    }
}
