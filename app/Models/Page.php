<?php

namespace App\Models;

use App\Traits\HasFile;
use App\Traits\HasMeta;
use App\Traits\HasScope;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasScope;
    use HasSlug;
    use HasFile;
    use HasMeta;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string
     */
    protected static $slug_source = 'title';

    /**
     * HasFile Trait initial data.
     */
    protected $has_file = [
        'name' => 'title',
        'source' => 'thumbnail',
        'image' => [
            'fit' => [300,200]
        ]
    ];

    /**
     * @param  string $value
     *
     * @return  void
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = \Str::slug($value, ' ', '');
    }

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo('App\Models\Category','parent');
    }

}
