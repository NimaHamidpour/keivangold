<?php

namespace App\Models;

use App\Traits\HasMeta;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use HasMeta;
    public function User()
    {
        return $this->belongsTo('App\Models\User','writer_id','id');
    }

    public function Admin()
    {
        return $this->belongsTo('App\Models\Admin','writer_id','id');
    }
}
