<?php

namespace App\Models;

use App\Traits\HasMeta;
use App\Traits\HasRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable,HasMeta,HasRole;

    protected $guard='admin';


    protected $fillable = [
        'name', 'mobile','roll'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function Role()
    {
        return $this->belongsTo('App\Models\Setting','role','key');
    }
}
