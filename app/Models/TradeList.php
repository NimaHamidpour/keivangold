<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeList extends Model
{
    public function Portfo()
    {
        return $this->belongsTo('App\Models\Portfo','portfo','id');
    }

    public function Trade()
    {
        return $this->belongsTo('App\Models\Trade','parent_trade','id');
    }
}
