<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;

// RUN API [Profit keyvan gold]
Route::get('/checkOnsaleListKeyvanGold' , function(){
    Artisan::call('schedule:run');
    return 'OK';
});

/*
    دریافت لحظه ای مظنه
    به روز رسانی قیمت یک دقیقه قبل و بررسی شارپ قیمت
    ساخت قیمت های گذشته
 */
Route::namespace('Admin\CronJob')->group(function (){

    Route::get('/getOnlineAmountApi/{key}' ,'OnlineTradeController@update'); //[online Amount Trade]
    Route::get('/getPerMinuteAmount/{key}' ,'OnlineTradeController@CheckAmountPerMinute'); //[CheckPerMinute]
    Route::get('/saveChartAmount/{key}' ,'GoldAmountController@update');
    Route::get('/syncTrade/{key}' ,'SyncTradeController@update');//[Sync trade profit] 5 min

});



// CLEAR CACHE
Route::prefix('cache')->group(function (){
    Route::get('/config', function () {
        $exit_code = Artisan::call('config:cache');
        return 'true';
    });
    Route::get('/view', function () {
        $exit_code = Artisan::call('view:cache');
        return 'true';
    });
    Route::get('/clear', function () {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });
});



// API
Route::prefix('Api')->name('api.')->namespace('Api')->group(function ()
{
    // Refresh Captcha
    Route::get('refresh_captcha', 'CaptchaController@refreshCaptcha')->name('refresh_captcha');

    // Product
    Route::post('/Product','ProductController@index')->name('product');
    Route::prefix('Cart')->name('cart.')->group(function ()
    {
        Route::post('/Add','CartController@addToCart')->name('add');
        Route::get('/Remove/{id}','CartController@removecart')->name('remove');
    });

    //  Blog
    Route::post('/loadBlog','BlogController@blogGroups')->name('load.blog');
    Route::post('/Blog/search', 'BlogController@search')->name('blog.search');

    // Like AND Dislike
    Route::post('/Comment/like', 'CommentController@like')->name('comment.like');
    Route::post('/Comment/dislike', 'CommentController@dislike')->name('comment.dislike');

    // LOAD Trade
    Route::post('/loadTrade','TradeController@index')->name('load.tarde');

    // ADMIN PRODUCT CATEGORY SEARCH
    Route::post('/Admin/User/search', 'UserAdminController@search')->name('admin.user.search');

});



// APP
Route::name('app.')->namespace('App')->group(function ()
{
    Route::get('/','HomeController@index')->name('home');
    Route::get('/about','AboutController@index')->name('about');
    Route::get('/other/{slug}','OtherPageController@index')->name('otherpage');
    Route::get('/cart','CartController@index')->name('cart');

    Route::get('/faq', 'FaqController@index')->name('faq');


    // BLOG
    Route::prefix('blog+')->name('blog.')->group(function ()
    {
        Route::get('/{slug?}', 'BlogController@index')->name('home');
        Route::get('/article/{slug}', 'BlogController@show')->name('show');
        Route::post('/search', 'BlogController@search')->name('search');
        Route::post('/comment/store', 'CommentController@store')->name('comment.store');
    });


});



// LOGIN AND LOGOUT USER
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::prefix('user')->namespace('Auth')->name('user.')->group(function () {

    Route::get('/forget', 'UserController@forgetform')->name('forget.form');
    Route::post('/forget', 'UserController@forget')->name('forget');

    Route::get('/reset/{email}', 'UserController@resetform')->name('reset.form');
    Route::post('/reset', 'UserController@reset')->name('reset');
});



// LOGIN ADMIN
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::prefix('adminwebpanelkeyvangold')->namespace('Auth')->name('admin.')->group(function ()
{
    Route::get('/login', 'AdminController@login_get_mobile')->name('login.show');
    Route::post('/login/sendCode', 'AdminController@login_send_code')->name('login.sendCode');

    Route::get('/login/verify/{mobile}', 'AdminController@login_get_code')->name('login.verify');
    Route::post('/verify', 'AdminController@login_verify')->name('login.submit');


    Route::get('/forget', 'AdminController@forgetform')->name('forget.form');
    Route::post('/forget', 'AdminController@forget')->name('forget');

    Route::get('/reset/{email}', 'AdminController@resetform')->name('reset.form');
    Route::post('/reset', 'AdminController@reset')->name('reset');
});




// USER
Route::prefix('user')->namespace('User')->name('user.')
    ->middleware('auth')->group(function () {

        Route::get('/dashboard', 'IndexController@index')->name('dashboard');

        // USER
        Route::prefix('user')->name('user.')->group(function () {
            Route::get('/edit', 'UserController@edit')->name('edit');
            Route::put('/update/{user}', 'UserController@update')->name('update');

            Route::get('/changepassword', 'UserController@edit_passwrod')->name('edit.password');
            Route::put('/updatepassword', 'UserController@update_passwrod')->name('update.password');

            Route::get('/verify', 'VerifyController@edit')->name('verify.edit');
            Route::post('/verify', 'VerifyController@update')->name('verify.update');
        });

        // WALLET
        Route::prefix('wallet')->name('wallet.')->group(function () {
            Route::get('/create', 'WalletController@create')->name('create');
            Route::post('/sharj', 'WalletController@sharj_wallet')->name('sharj');
        });

        // Withdrawal
        Route::prefix('withdrawal')->name('withdrawal.')->group(function () {
            Route::get('/create', 'WithdrawalController@create')->name('create');
            Route::post('/store', 'WithdrawalController@store')->name('store');
        });

        // HISTORY
        Route::get('/history', 'HistoryController@index')->name('history');

        // PAY
        Route::any('/pay', 'PayController@callBackBank')->name('pay');


        // TICKET
        Route::prefix('ticket')->name('ticket.')->group(function () {
            Route::get('/list', 'TicketController@index')->name('list');
            Route::get('/show/{ticket}', 'TicketController@show')->name('show');
            Route::get('/finished/{ticket}', 'TicketController@finished')->name('finished');
            Route::get('/add', 'TicketController@create')->name('add');
            Route::post('/store', 'TicketController@store')->name('store');
        });
        Route::prefix('conversation')->name('conversation.')->group(function () {
            Route::post('/store', 'ConversationController@store')->name('store');
        });


        // NOTIFICATION
        Route::prefix('notification')->name('notification.')->group(function () {
            Route::get('/list', 'NotificationController@index')->name('list');
            Route::get('/show/{notification}', 'NotificationController@show')->name('show');

        });



        // PORTFO
        Route::prefix('portfo')->name('portfo.')->group(function () {
            Route::get('/list', 'PortfoController@index')->name('list');
            Route::post('/store', 'PortfoController@store')->name('store');
            Route::get('/close/{portfo}', 'PortfoController@close')->name('close');
        });


        // CREATE TRADE
        Route::prefix('tradelist')->name('tradelist.')->group(function () {
            Route::get('/list', 'TradeListController@index')->name('list');
            Route::post('/store/{type}', 'TradeListController@store')->name('store');
            Route::get('/destroy/{id}', 'TradeListController@destroy')->name('destroy');
        });


        // DO TRADE
        Route::prefix('trade')->name('trade.')->group(function () {
            Route::get('/', 'TradeController@tradeList')->name('tradeList');
            Route::get('/list/{portfo}', 'TradeController@index')->name('list');
            Route::post('/store/{tradeList}', 'TradeController@store')->name('store');
            Route::get('/close', 'CloseTradeController@CloseSystemTrade')->name('close');
            Route::get('/video', 'LearnController@index')->name('video');
        });


    });



// ADMIN
Route::prefix('adminwebpanelkeyvangold')->namespace('Admin')->name('admin.')
    ->middleware('auth:admin')->group(function () {

        Route::get('/', 'IndexController@index')->name('dashboard');


        //************ admin  *************
        Route::prefix('admin')->name('admin.')->group(function () {
            Route::get('/edit', 'AdminController@edit')->name('edit');
            Route::put('/update/{user}', 'AdminController@update')->name('update');

            Route::get('/changepassword', 'AdminController@edit_passwrod')->name('edit.password');
            Route::put('/updatepassword', 'AdminController@update_passwrod')->name('update.password');
        });


        //************ close trade *************
        Route::get('/closeTrade', 'CloseTradeController@closeTrade')->middleware('permission:permission_trade_setting')->name('closeTrade');


        //************ portfo trade admin *************
        Route::prefix('portfo')->name('portfo.')->middleware('permission:permission_portfo')->group(function () {
            Route::any('/list', 'PortfoController@index')->name('list');
            Route::get('/trade/{portfo}', 'PortfoController@trade')->name('show');
        });


        //************ portfo trade admin *************
        Route::prefix('trade')->name('trade.')->group(function () {
            Route::any('/list', 'TradeController@index')->middleware('permission:permission_trade')->name('list');
            Route::get('/onsale', 'TradeController@onSale')->middleware('permission:permission_onSale')->name('onsale');
        });


        //************ online trade setting admin *************
        Route::prefix('onlinetradeSetting')->name('onlinetradeSetting.')->middleware('permission:permission_trade_setting')->group(function () {
            Route::get('/trade/edit', 'OnlineTradeSettingController@tradeEdit')->name('trade.edit');
            Route::get('/amount/edit', 'OnlineTradeSettingController@amountEdit')->name('amount.edit');
            Route::Post('/update', 'OnlineTradeSettingController@update')->name('update');

        });



        //************ user admin *************
        Route::prefix('user')->name('user.')->middleware('permission:permission_user')->group(function () {
            Route::any('/list', 'UserController@index')->name('list');
            Route::get('/show/{user}', 'UserController@show')->name('show');
            Route::get('/history/{user}', 'HistoryController@index')->name('history');
            Route::get('/destroy/{user}', 'UserController@destroy')->name('destroy');
            Route::get('/excell', 'UserController@excell')->name('excell');
        });

        Route::prefix('user')->name('user.')->middleware('permission:permission_user')->group(function () {
            Route::get('/wallet/{user}', 'WalletController@create')->name('wallet.create');
            Route::post('/wallet', 'WalletController@store')->name('wallet.store');

        });

        Route::prefix('user/active')->name('user.active.')->middleware('permission:permission_user')->group(function () {
            Route::get('/list', 'VerifyUserController@index')->name('list');
            Route::get('/edit/{user}', 'VerifyUserController@edit')->name('edit');
            Route::post('/update/{user}', 'VerifyUserController@update')->name('update');

        });

        //************ pay admin *************
        Route::prefix('pay')->name('pay.')->group(function () {
            Route::get('/list/{type}', 'PayController@index')->middleware('permission:permission_pay_transection')->name('list');
            Route::any('/information', 'PayController@information')->middleware('permission:permission_pay_info')->name('info');

            Route::any('/withdrawal/list', 'WithdrawalController@index')->middleware('permission:permission_withdrawal')->name('withdrawal.list');
            Route::get('/withdrawal/update/{withdrawal}', 'WithdrawalController@update')->middleware('permission:permission_withdrawal')->name('withdrawal.update');
            Route::get('/withdrawal/excell', 'WithdrawalController@excell')->middleware('permission:permission_withdrawal')->name('withdrawal.excell');
        });



        //*********** ticket admin ***************
        Route::prefix('ticket')->name('ticket.')->middleware('permission:permission_ticket')->group(function () {
            Route::get('/list', 'TicketController@index')->name('list');
            Route::get('/show/{ticket}', 'TicketController@show')->name('show');
            Route::get('/finished/{ticket}', 'TicketController@finished')->name('finished');
            Route::get('/add', 'TicketController@create')->name('add');
            Route::post('/store', 'TicketController@store')->name('store');
        });
        Route::prefix('conversation')->name('conversation.')->middleware('permission:permission_ticket')->group(function () {
            Route::post('/store', 'ConversationController@store')->name('store');
        });


        //************ product admin *************
        Route::prefix('product')->name('product.')->middleware('permission:permission_portfolio')->group(function () {
            Route::get('/list', 'ProductController@index')->name('list');
            Route::get('/add', 'ProductController@create')->name('create');
            Route::post('/store', 'ProductController@store')->name('store');
            Route::get('/edit/{id}', 'ProductController@edit')->name('edit');
            Route::post('/update/{id}', 'ProductController@update')->name('update');
            Route::get('/destroy/{id}', 'ProductController@destroy')->name('destroy');
        });


        //************ notification admin *************
        Route::prefix('notification')->name('notification.')->middleware('permission:permission_notification')->group(function () {
            Route::get('/list', 'NotificationController@index')->name('list');
            Route::get('/add', 'NotificationController@create')->name('add');
            Route::post('/store', 'NotificationController@store')->name('store');
            Route::get('/edit/{notification}', 'NotificationController@edit')->name('edit');
            Route::post('/update/{notification}', 'NotificationController@update')->name('update');
            Route::get('/destroy/{notification}', 'NotificationController@destroy')->name('destroy');
        });


        //************ category admin *************
        Route::prefix('category')->name('category.')->middleware('permission:permission_writer')->group(function () {
            Route::get('/list/{type}', 'CategoryController@index')->name('list');
            Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
            Route::post('/update/{id}', 'CategoryController@update')->name('update');
            Route::post('/add', 'CategoryController@store')->name('store');
            Route::get('/destroy/{id}', 'CategoryController@destroy')->name('destroy');
        });

        //************ blog admin *************
        Route::prefix('blog')->name('blog.')->middleware('permission:permission_writer')->group(function () {
            Route::get('/list/{type}', 'PageController@index')->name('list');
            Route::get('/add/{type}', 'PageController@create')->name('create');
            Route::post('/store', 'PageController@store')->name('store');
            Route::get('/edit/{id}', 'PageController@edit')->name('edit');
            Route::post('/update/{id}', 'PageController@update')->name('update');
            Route::get('/destroy/{id}', 'PageController@destroy')->name('destroy');
        });

        //************ webpage admin *************
        Route::prefix('webpage')->name('webpage.')->middleware('permission:permission_writer')->group(function () {
            Route::get('/list/', 'WebPageController@index')->name('list');
            Route::get('/edit/{id}', 'WebPageController@edit')->name('edit');
            Route::post('/update/{id}', 'WebPageController@update')->name('update');
        });

        // ****************** seo ******************
        Route::prefix('seo')->middleware('permission:permission_writer')->name('seo.')->group(function () {
            Route::get('/list', 'SeoController@index')->name('list');
            Route::get('/show/{meta}', 'SeoController@show')->name('show');
            Route::post('/store/{meta}', 'SeoController@store')->name('store');
            Route::get('/destroy/{meta}', 'SeoController@destroy')->name('destroy');
        });


        //************ question admin *************
        Route::prefix('question')->name('question.')->middleware('permission:permission_writer')->group(function () {
            Route::get('/list', 'QuestionController@index')->name('list');
            Route::get('/add', 'QuestionController@create')->name('create');
            Route::post('/store', 'QuestionController@store')->name('store');
            Route::get('/edit/{question}', 'QuestionController@edit')->name('edit');
            Route::post('/update/{question}', 'QuestionController@update')->name('update');
            Route::get('/destroy/{question}', 'QuestionController@destroy')->name('destroy');
        });

        // ****************** social ************************
        Route::prefix('social')->name('social.')->middleware('permission:permission_setting')->group(function() {
            Route::get('list', 'SocialController@index')->name('list');
            Route::post('store', 'SocialController@store')->name('store');
            Route::get('destroy/{setting}', 'SocialController@destroy')->name('destroy');
        });

        // ****************** setting admin ******************
        Route::name('setting.')->middleware('permission:permission_setting')->group(function () {
            Route::get('/setting', 'SettingController@edit')->name('edit');
            Route::post('/setting', 'SettingController@update')->name('update');
        });



        // ****************** manager admin ******************
        Route::prefix('manager')->name('manager.')->middleware('permission:permission_manager_admin')->group(function () {
            Route::get('/list', 'ManagerController@index')->name('list');
            Route::get('/add', 'ManagerController@create')->name('create');
            Route::post('/store', 'ManagerController@store')->name('store');
            Route::get('/destroy/{admin}', 'ManagerController@destroy')->name('destroy');

        });

        // ****************** permission admin ******************
        Route::prefix('permission')->name('permission.')->middleware('permission:permission_manager_admin')->group(function () {
            Route::get('/list/{admin}', 'PermissionController@index')->name('list');
            Route::post('/store/{admin}', 'PermissionController@store')->name('store');
            Route::get('/destroy/{permission}', 'PermissionController@destroy')->name('destroy');

        });

    });



// FILE MANAGER
Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth:admin']], function () {
    Lfm::routes();
});
