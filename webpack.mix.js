const mix = require('laravel-mix');


mix.styles([
    'resources/css/bootstrap/bootstrap.css',
    'resources/css/bootstrap/bootstrap-grid.min.css',
    'resources/css/bootstrap/sb-admin.min.css',
    'resources/css/google/fontgoogleapi.css',
    'resources/css/google/fontgoogleapi2.css',
    'resources/css/style/style.css',
    'resources/css/style/basic.css',
    'resources/css/style/admin.css',
    'resources/css/style/user.css',
    'resources/css/style/blog.css',
    'resources/css/slider/owl.carousel.min.css',
    'resources/css/slider/owl.theme.default.min.css',
    'resources/css/other/aos.css',
    'resources/css/persianDatePicker/persian-datepicker.css',


],'public/css/app11.css');

mix.scripts([
    'resources/js/vendor/jquery/jquery.min.js',
    'resources/js/vendor/bootstrap/js/bootstrap.bundle.min.js',
    'resources/js/vendor/jquery-easing/jquery.easing.min.js',
    'resources/js/vendor/magnific-popup/jquery.magnific-popup.min.js',
    'resources/js/shop/shop.js',
    'resources/js/counter/counter3.js',
    'resources/js/counter/counter.js',
    'resources/js/counter/counter2.js',
    'resources/js/admin/sb-admin.min.js',
    'resources/js/slider/owl.carousel.min.js',
    'resources/js/aos/aos.js',
    'resources/js/aos/aosnext.js',
    'resources/js/other/sweetalert.js',
    'resources/js/custom/main.js',
    'resources/js/custom/blog.js',
    'resources/js/custom/trade.js',
    'resources/js/custom/refresh.js',
    'resources/js/custom/admin.js',
    'resources/js/persianDatePicker/persian-date.js',
    'resources/js/persianDatePicker/persian-datepicker.js',

],'public/js/app11.js');
