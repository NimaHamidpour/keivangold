<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->integer('portfo')->unsigned();
            $table->integer('amount');
            $table->integer('weight');
            $table->enum('type',['buyer','seller']);
            $table->enum('status',['open','close']);
            $table->enum('end_type',['user', 'system', 'onsale']);
            $table->integer('end_amount')->nullable();
            $table->integer('profit')->nullable();
            $table->timestamps();

            $table->foreign('portfo')->references('id')->on('portfos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
