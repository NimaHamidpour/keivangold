<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('amount');
            $table->string('model');
            $table->integer('model_id');
            $table->enum('key',['wallet','trade']);
            $table->enum('income',['yes','no']);
            $table->enum('paid',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_histories');
    }
}
