<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfos', function (Blueprint $table) {
            $table->id();
            $table->integer('openAmount');
            $table->integer('user')->unsigned();
            $table->integer('weight');
            $table->integer('amount');
            $table->integer('lever');
            $table->integer('weight_buy');
            $table->integer('weight_sell');
            $table->timestamp('end_date')->nullable();
            $table->integer('profit')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfos');
    }
}
