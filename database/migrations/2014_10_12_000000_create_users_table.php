<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('mobile')->unique();
            $table->string('codemeli')->unique();
            $table->string('password');
            $table->integer('wallet')->default(0);
            $table->string('avatar')->nullable();
            $table->string('kartmeli')->nullable();
            $table->string('friendShip')->nullable();
            $table->string('bank')->nullable();
            $table->string('sheba')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('mobile_verified_at')->nullable();

        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
