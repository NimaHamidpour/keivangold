<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_lists', function (Blueprint $table) {
            $table->id();
            $table->integer('portfo')->unsigned();
            $table->integer('amount');
            $table->integer('weight');
            $table->enum('type',['buyer','seller']);
            $table->enum('active',['yes','no'])->default('yes');
            $table->integer('parent_trade')->nullable();

            $table->timestamps();
            $table->foreign('portfo')->references('id')->on('portfos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_lists');
    }
}
