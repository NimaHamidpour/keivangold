function addToCart(id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url:'/Api/Cart/Add',
        method:'post',
        data:{id:id},
        dataType:'json',
        success:function (data)
        {
            if (data['msg'] === "add")
            {
              document.getElementById('btnAdd'+id).innerText='به سبد خرید شما اضافه شد';
              document.getElementById('btnAdd'+id).classList.remove('btn-warning');
              document.getElementById('btnAdd'+id).classList.add('btn-secondary');
              document.getElementById('btnAdd'+id).disabled=true;

            }
            $('#basket-id').text(data['count']);
        }
    });
}

function updatecart(id,quantity) {

    $.ajax({
        url:'/ajax/updateCart',
        method:'post',
        data:{'id':id,'quantity':quantity},
        success:function (data)
        {
            if (data === 'update')
                window.location.reload();
        }
    });
}

function minesCart(id) {

    $.ajax({
        url:'/ajax/minesCart',
        method:'post',
        data:{'id':id},
        success:function (data)
        {
            if (data === "update")
            {
                document.getElementById('counter'+id).innerText= Number(document.getElementById('counter'+id).innerText)-1;
            }
            else if(data === "delete")
            {
                document.getElementById('btnaddcart'+id).classList.remove('d-none');
                document.getElementById('btnaddcart'+id).classList.add('d-block');
                document.getElementById('controller'+id).classList.remove('d-block');
                document.getElementById('controller'+id).classList.add('d-none');
                document.getElementById('counter'+id).innerText= 0;
                document.getElementById('count-id').innerText=Number(document.getElementById('count-id').innerText)-1;
                document.getElementById('shopping-cart').style.animation='none';
            }
        }
    })

}

function removefrombasket(id) {

    $.ajax({
        url:'/ajax/removecart/'+id,
        method:'get',
        success:function (data)
        {
            if(data === "delete")
            {
                window.location.reload();
            }
        }
    });
}


