// CONVERT 3 DIGIT NUMBER
function digitGroup(sourse, target,unit) {
    var value = sourse.value;
    var output = "";
    try {
        value = value.replace(/[^0-9]/g, ""); // remove all chars including spaces, except digits.
        var totalSize = value.length;
        for (var i = totalSize - 1; i > -1; i--) {
            output = value.charAt(i) + output;
            var cnt = totalSize - i;
            if (cnt % 3 === 0 && i !== 0) {
                output = "/" + output; // seperator is " "
            }
        }
    } catch (err) {
        output = value; // it won't happen, but it's sweet to catch exceptions.
    }
    document.getElementById(target).innerHTML = output + ' '+ unit;
}

function calculateAmount(weight,price,target,percent){

    var value = parseInt(((weight * (price / 4.33) * 10 * percent)/100)).toString();
    var output = "";
    try
    {
        value = value.replace(/[^0-9]/g, "");
        var totalSize = value.length;

        for (var i = totalSize - 1; i > -1; i--)
        {
            output = value.charAt(i) + output;
            var cnt = totalSize - i;
            if (cnt % 3 === 0 && i !== 0)
            {
                output = "/" + output;
            }
        }
    }
    catch (err)
    {
        output = value;
    }
    document.getElementById(target).innerHTML = output;
}

function loadProduct(parent,category) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.blog_group').not(parent).removeClass('active');
    $(parent).toggleClass('active');

    $.ajax({
        url:'Api/Product',
        method:'post',
        data:{category:category},
        success:function (data)
        {
            if(data !== '')
            {
                $('#product-view').html(data);
                $("#product-carousel").owlCarousel({
                    loop: true,
                    autoplay: true,
                    items: 5,
                    rtl: true,
                    center: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
            }
            else
            {
                $('#product-view').html('');
            }
        }
    });
}

function pledge_portfo(unit)
{
   var value = Math.floor($('#amount').val() / ( unit * $('#lever').val() * 230)) + '  ';


    document.getElementById('pledge-amount').innerHTML = value + ' '+ 'واحد';
}


function pledge_amount_portfo(unit)
{
    var value = $('#amount').val()

    var output = "";
    try {
        value = value.replace(/[^0-9]/g, ""); // remove all chars including spaces, except digits.
        var totalSize = value.length;
        for (var i = totalSize - 1; i > -1; i--) {
            output = value.charAt(i) + output;
            var cnt = totalSize - i;
            if (cnt % 3 === 0 && i !== 0) {
                output = "/" + output; // seperator is " "
            }
        }
    } catch (err) {
        output = value; // it won't happen, but it's sweet to catch exceptions.
    }
    document.getElementById('help-amount').innerHTML = output + ' تومان ';

    pledge_portfo(unit);
}
