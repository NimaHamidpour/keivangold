function trade(type)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url:'/Api/loadTrade',
        method:'post',
        data:{type:type},
        dataType:'json',
        success:function (data)
        {
            if (data['msg'] === 'close')
            {
                $('#tbody-'+type).html('');
                $('#close_check').val('close');
            }
            else if(data['msg'] !== 'close' && data['output'] !== '')
            {
                $('#tbody-'+type).html(data['output']);
            }
            else
            {
                $('#tbody-'+type).html('');
            }


            if (data['portfo_msg'] === true)
            {

                if (data['portfo_profit_status'] === 'posetive')
                {
                    $('#portfo_profit').html(data['portfo_profit']).css('color','#008000');
                }
                else
                {
                    $('#portfo_profit').html(data['portfo_profit']).css('color','#dc3545');
                }

                $('#portfo_status').html(data['portfo_status']);
                $('#portfo_weight_buy').html(data['weight_buy']);
                $('#portfo_weight_sell').html(data['weight_sell']);
            }
            $('#trade_online_amount').html(data['trade_online_amount']);
            $('#trade_last_amount').html(data['trade_last_amount']);
        }
    });
}

