// search when add user step1
function suggest_user_admin(parent,targetId)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var search=$(parent).val();

    if (search.length > 3)
    {
        $.ajax({
            url:'/Api/Admin/User/search',
            method:'post',
            data:{search:search},
            success:function (data)
            {
                if (data !== '')
                {
                    $('#'+targetId).html(data);
                }
                else
                {
                    $('#'+targetId).html('<div class="alert alert-warning text-center font-size-14">کاربری یافت نشد</div>');
                }
            }
        });
    }
    else
    {
        $('#aj_search_content').html('');
    }
}


// search when add user step2
function setUser(userId,userTitle)
{
    $('#user').val(userId);
    $('#user_search').val(userTitle);
    $('#aj_search_content').html('');
}
