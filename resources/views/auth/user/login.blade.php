@extends('app.index')

@section('title','ورود به حساب کاربری')

@section('content')


    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('message.message')
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                    <div class="d-flex justify-content-center h-100">

                        <div class="user_card">
                            <div class="row">
                                <div class="col-12 text-center mx-auto">
                                    <h2 class="font-family-yekan text-center font-size-17 text-danger mt-0 mt-xl-2 mt-lg-2 font-weight-bold">ورود به حــساب کـاربری</h2>
                                    <hr class="divider"/>
                                </div>
                            </div>



                            <div class="d-flex justify-content-center">
                                <form method="POST" action="{{ route('login') }}" class="px-3">
                                @csrf
                                <!-- USER NAME -->

                                    <div class="form-row">
                                        <label class="font-family-yekan font-size-12 text-right font-weight-bold">
                                            نام کاربری
                                            <span class="text-danger font-size-13">(شماره تماس)</span>
                                        </label>

                                        <div class="input-group mb-3 direction-ltr">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <input type="text" name="mobile" autocomplete="off" class="form-control input_user font-family-iransans font-size-14 text-right" placeholder="شماره تماس">
                                        </div>
                                    </div>

                                    <!-- PASSWORD -->
                                    <div class="form-row mt-1">
                                        <label class="font-family-yekan font-size-12 text-right font-weight-bold">
                                            کلمه عبور
                                        </label>
                                        <div class="input-group mb-2 direction-ltr">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                                            </div>
                                            <input type="password" name="password" class="form-control input_pass font-size-14 font-family-yekan text-right" placeholder="کلمه عبور">
                                        </div>
                                    </div>

                                    <!-- REGISTER -->
                                    <div class="form-group border-bottom">
                                        <div class="custom-control custom-checkbox direction-rtl text-right my-2">
                                            <a href="{{route('register')}}" class="text-decoration-none">
                                                <span class="font-family-yekan font-size-14 user-danger">تا الان ثبت نام نکردید،</span>
                                                <span class="font-family-yekan font-size-14 user-success">ثبت نام کنید</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="form-row my-4">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                            <label class="admin-input-label d-block text-right" for="Capcha">حاصل جمع عبارت امنیتی</label>
                                            <input type="text" class="user-input form-control my-2 rounded shadow-none direction-ltr text-center" name="Capcha" id="Capcha">
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                            <div id="captcha" class="d-inline-block" style="width: 200px">
                                                {!! captcha_img('math') !!}
                                            </div>
                                            <button type="button"
                                                    class="btn btn-light border rounded btn-refresh mt-1" style="height: 50px">
                                                <i class="fas fa-redo text-danger font-size-22"></i>
                                            </button>
                                        </div>

                                    </div>


                                    <!-- LOGIN -->
                                    <div class="d-flex justify-content-center mt-3 login_container">
                                        <button type="submit" name="loginadmin" class="btn login_btn font-family-yekan">ورود</button>
                                    </div>

                                </form>
                            </div>

                            <div class="mt-4">
                                <div class="d-flex justify-content-center links">
                                    <a href="{{route('user.forget.form')}}" class="text-decoration-none user-danger font-family-yekan font-size-13 font-weight-bold">رمز عبور خود را فراموش کرده ام</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection


@section('script')
    <script type="text/javascript">



        $(".btn-refresh").click(function(){

            $.ajax({

                type:'GET',
                url:'/Api/refresh_captcha',
                success:function(data){
                    $("#captcha").html(data.captcha);
                }
            });
        });
    </script>
@endsection
