@extends('app.index')

@section('title','ثبت نام کاربران')

@section('content')
    <section class="page-section my-5">
        <div class="container">
            @include('message.message')

            <section class="direction-rtl bg-white mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                            <form action="{{route('register')}}" method="POST"
                                  class="form text-right direction-rtl border my-3  py-3 px-4 rounded">
                                @csrf

                                <h2 class="font-yekan font-size-19 text-center text-danger mt-3 font-weight-bold">ایجاد
                                    حــساب کــاربری</h2>
                                <hr class="divider"/>

                                <div class="form-row">
                                    <div class="col-md-6 mx-auto mb-2">
                                        <label for="name-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbspنام</label>
                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-user" style="width: 15px"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control text-center user-input rounded-0 @error('name') is-invalid @enderror"
                                                   id="name-id" name="name" placeholder="نام و فامیل"
                                                   value="{{old('name')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mx-auto  mb-2">
                                        <label for="mobile-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbspشماره تلفن
                                            همراه</label>

                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-mobile" style="width: 15px"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control text-center user-input direction-ltr rounded-0 @error('mobile') is-invalid @enderror"
                                                   id="mobile-id" name="mobile" placeholder="۰۹ ..."
                                                   value="{{old('mobile')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mx-auto  mb-2">
                                        <label for="tell-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;تلفن ثابت </label>

                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-phone" style="width: 15px"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control text-center user-input direction-ltr rounded-0 @error('codemeli') is-invalid @enderror"
                                                   id="tell-id" name="tell" placeholder="0213***"
                                                   value="{{old('tell')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6 mx-auto  mb-2">
                                        <label for="codemeli-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کد ملی </label>

                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-passport" style="width: 15px"></i></span>
                                            </div>
                                            <input type="text"
                                                   class="form-control text-center user-input direction-ltr rounded-0 @error('codemeli') is-invalid @enderror"
                                                   id="codemeli-id" name="codemeli" placeholder="1234567891"
                                                   value="{{old('codemeli')}}">
                                        </div>
                                    </div>


                                    <div class="col-md-6 mb-2">
                                        <label for="friendShip" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;راه آشنایی
                                        </label>

                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;">
                                                    <i class="fas fa-handshake" style="width: 15px"></i></span>
                                            </div>
                                            <select class="form-control admin-input" style="text-align-last: center"
                                                    name="friendShip" id="friendShip">
                                                <option value="" hidden>لطفا راه آشنایی با ما را انتخاب کنید</option>
                                                <option value="instagram">اینستاگرام</option>
                                                <option value="kaveh">ایجنت کـاوه</option>
                                                <option value="website">وب سایت</option>
                                                <option value="telegram">تلگرام</option>
                                                <option value="aparat">آپارات</option>
                                                <option value="family">آشنایی قبلی</option>
                                                <option value="other">سایر</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-row">
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-2">
                                        <label for="password-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbspکلمه عبور</label>
                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-lock" style="width: 15px"></i></span>
                                            </div>
                                            <input type="password"
                                                   class="form-control text-center user-input rounded-0 @error('password') is-invalid @enderror"
                                                   id="password-id" name="password" placeholder="کلمه عبور"
                                                   value="{{old('password')}}">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-2">
                                        <label for="password_confirmation-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;تکرار کلمه
                                            عبور</label>
                                        <div class="input-group mb-3 direction-rtl">
                                            <div class="input-group-append">
                                                <span class="input-group-text" style="border-radius: 0!important;"><i
                                                        class="fas fa-lock" style="width: 15px"></i></span>
                                            </div>
                                            <input type="password"
                                                   class="form-control text-center user-input  rounded-0 @error('password_confirmation') is-invalid @enderror"
                                                   id="password_confirmation-id" name="password_confirmation"
                                                   placeholder="تکرار کلمه عبور" value="{{old('password')}}">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12
                                                font-yekan font-size-14  mx-auto mb-2 text-justify
                                                line-35 border rounded p-3" readonly>
                                        <h2 class="font-size-14 font-weight-bold text-danger">قوانین و مقررات</h2>
                                        {!! $response['app_rule'] !!}
                                    </div>

                                    <label class="font-size-13 font-weight-bold mt-3" for="rule">
                                        قوانین و مقررات سایت را خوانده و قبول دارم
                                    </label>
                                    <input type="checkbox" value="confirm" name="rule" id="rule" class="my-3 mx-1">
                                </div>

                                <div class="form-row mt-3">
                                    <input type="submit"
                                           class="col-md-6 btn user-submit  mt-3 font-weight-bold font-yekan"
                                           value="ثبت نام ">
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
