@extends('app.index')

@section('title','فراموشی کلمه عبور')

@section('content')

    <section class="page-section">
        <div class="container">
            @include('message.message')

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                    <div class="d-flex justify-content-center h-100">
                        <div class="user_card" style="height: 450px!important;">

                            <div class="row">
                                <div class="col-12 text-center mx-auto">
                                    <h2 class="font-family-yekan text-center font-size-22 text-danger font-weight-bold">
                                        فراموشی کلمه عبور
                                    </h2>
                                    <hr class="divider"/>
                                </div>
                            </div>
                            <form method="POST" action="{{ route('user.forget') }}" class="px-3">
                            @csrf


                            <!-- USER NAME -->
                                <div class="form-row">
                                    <label class="user-input-label" for="email">شماره موبایل</label>
                                    <div class="input-group mb-4 direction-ltr">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" name="mobile" id="mobile"
                                               class="form-control input_user font-family-iransans font-size-14 text-right"
                                               placeholder="شماره موبایل">
                                    </div>
                                </div>


                                <div class="form-row my-4">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                        <label class="admin-input-label d-block text-right" for="Capcha">حاصل جمع عبارت
                                            امنیتی</label>
                                        <input type="text"
                                               class="user-input form-control my-2 rounded shadow-none direction-ltr text-center"
                                               name="Capcha" id="Capcha">
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                        <div id="captcha" class="d-inline-block" style="width: 200px">
                                            {!! captcha_img('math') !!}
                                        </div>
                                        <button type="button"
                                                class="btn btn-light border rounded btn-refresh mt-1"
                                                style="height: 50px">
                                            <i class="fas fa-redo text-danger font-size-22"></i>
                                        </button>
                                    </div>

                                </div>

                                <!-- LOGIN -->
                                <div class="d-flex justify-content-center mt-4 login_container">
                                    <button type="submit" name="loginadmin" class="btn login_btn font-yekan mt-3">
                                        بازیابی کلمه عبور
                                    </button>
                                </div>

                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script type="text/javascript">


        $(".btn-refresh").click(function () {

            $.ajax({

                type: 'GET',
                url: '/Api/refresh_captcha',
                success: function (data) {
                    $("#captcha").html(data.captcha);
                }
            });
        });
    </script>
@endsection
