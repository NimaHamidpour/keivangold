@extends('app.index')

@section('title','ویرایش کلمه عبور')

@section('content')

    <section class="page-section">
        <div class="container">
            @include('message.message')

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                    <div class="user_card mx-auto" style="height: 520px!important;">

                        <div class="d-flex justify-content-center">

                            <form method="POST" action="{{ route('user.reset') }}" class="px-3 text-right">
                                @csrf
                                <div class="row">
                                    <div class="col-12 text-center mx-auto">
                                        <h2 class="font-family-yekan text-center font-size-17 text-danger font-weight-bold">
                                            تغییر کلمه عبور
                                        </h2>
                                        <hr class="divider"/>
                                    </div>
                                </div>

                                <input type="hidden" value="{{$response['mobile']}}" name="mobile" readonly>
                                <label class="font-family-yekan font-size-12 text-right font-weight-bold text-danger">
                                    کد دریافتی از طریق پیامک
                                </label>
                                <div class="input-group mb-3 direction-ltr">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" name="token_code"
                                           class="form-control border admin-input font-yekan text-right"
                                           placeholder="کد دریافتی" value="{{old('code')}}">
                                </div>

                                <label class="font-family-yekan font-size-12 text-right font-weight-bold text-danger">
                                    کلمه عبور جدید
                                </label>
                                <div class="input-group mb-3 direction-ltr">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <input type="password" name="password"
                                           class="form-control border admin-input font-yekan text-right"
                                           placeholder="رمز عبور">
                                </div>

                                <label class="font-family-yekan font-size-12 text-right font-weight-bold text-danger">
                                    تکرار کلمه عبور جدید
                                </label>
                                <div class="input-group mb-3 direction-ltr">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                    </div>
                                    <input type="password" name="password_confirmation"
                                           class="form-control border admin-input font-yekan text-right"
                                           placeholder="تکرار رمز عبور">
                                </div>


                                <div class="form-row my-4">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                        <label class="admin-input-label d-block text-right" for="Capcha">حاصل جمع عبارت
                                            امنیتی</label>
                                        <input type="text"
                                               class="user-input form-control my-2 rounded shadow-none direction-ltr text-center"
                                               name="Capcha" id="Capcha">
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                        <div id="captcha" class="d-inline-block" style="width: 200px">
                                            {!! captcha_img('math') !!}
                                        </div>
                                        <button type="button"
                                                class="btn btn-light border rounded btn-refresh mt-1"
                                                style="height: 50px">
                                            <i class="fas fa-redo text-danger font-size-22"></i>
                                        </button>
                                    </div>

                                </div>

                                <!-- LOGIN -->
                                <div class="d-flex justify-content-center mt-3 login_container">
                                    <button type="submit" name="loginadmin" class="btn login_btn font-yekan">تغییر
                                        رمز عبور
                                    </button>
                                </div>

                            </form>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script type="text/javascript">


        $(".btn-refresh").click(function () {

            $.ajax({

                type: 'GET',
                url: '/Api/refresh_captcha',
                success: function (data) {
                    $("#captcha").html(data.captcha);
                }
            });
        });
    </script>
@endsection
