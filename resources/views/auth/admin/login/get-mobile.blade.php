<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon"  type="image/jpg" sizes="96x96" href="{{url((isset($response['app_logo']) ? $response['app_logo'] : 'front/img/structure/websazandeh.png'))}}">

    <title> ورود به پـنل مدیریت:: {{$response['app_name']}}</title>

    <link rel="stylesheet" href="{{url('/css/app11.css?v=11')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">
    <meta name="theme-color" content="#000000" />
    <meta name="msapplication-navbutton-color" content="#000000" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000" />
</head>
<body class="bg-admin pt-5">
<div id="mainNav"></div>
<div class="container h-100">
    @include('message.message')
    <div class="d-flex justify-content-center h-100 mt-5">
        <div class="admin_card mt-3">

            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <img src="{{url('app/img/login.png')}}" class="brand_logo" alt="Logo">
                </div>
            </div>
            <div class="d-flex justify-content-center px-2" style="margin-top: 60px!important;">
                <form method="POST" action="{{ route('admin.login.sendCode') }}">
                @csrf

                   <!-- USER NAME -->
                    <div class="form-row text-right mt-3">
                        <label class="text-right font-family-yekan text-admin font-size-14 font-weight-bold" for="email">نام کاربری</label>
                        <input type="text" name="mobile" id="mobile"
                               class="form-control input_user font-family-iransans font-size-14 text-center" placeholder="شماره تلفن همراه">
                    </div>

                    <!-- PASSWORD -->
                    <div class="form-row text-right mt-3">
                        <label class="text-right font-family-yekan text-admin font-size-14 font-weight-bold" for="password">کلمه عبور</label>
                        <input type="password" name="password" id="password"
                               class="form-control input_user font-family-iransans font-size-14 text-center" placeholder="کلمه عبور">
                    </div>

                    <div class="form-row my-4">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                            <label class="admin-input-label d-block text-right text-admin font-family-yekan" for="Capcha">حاصل جمع عبارت امنیتی</label>
                            <input type="text" placeholder="حاصل جمع عبارت امنیتی"
                                   class="user-input form-control my-2 rounded shadow-none direction-ltr text-center" name="Capcha" id="Capcha">
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center mt-2">
                            <div id="captcha" class="d-inline-block" style="width: 200px">
                                {!! captcha_img('math') !!}
                            </div>
                            <button type="button"
                                    class="btn btn-light border rounded btn-refresh mt-1" style="height: 50px">
                                <i class="fas fa-redo text-danger font-size-22"></i>
                            </button>
                        </div>

                    </div>

                    <!-- LOGIN -->
                    <div class="d-flex justify-content-center login_container">
                        <button type="submit" name="loginadmin" class="btn login_btn font-family-yekan">ارسال کلمه عبور یک بار مصرف</button>
                    </div>

                    <div class="mt-4">
                        <div class="d-flex justify-content-center links">
                            <a href="{{route('admin.forget.form')}}" class="text-decoration-none user-danger font-family-yekan font-size-13 font-weight-bold">رمز عبور خود را فراموش کرده ام</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script src="{{url('/js/app11.js?v=11')}}"></script>
</body>
</html>

