<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/jpg" sizes="96x96"
          href="{{url((isset($response['app_logo']) ? $response['app_logo'] : 'front/img/structure/websazandeh.png'))}}">

    <title> ورود به مدیریت {{$response['app_name']}}</title>

    <link rel="stylesheet" href="{{url('/css/app11.css?v=11')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">
    <meta name="theme-color" content="#000000"/>
    <meta name="msapplication-navbutton-color" content="#000000"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000"/>
</head>
<body class="bg-admin pt-5">

<div class="container h-100">
    <div class="row">
        <div class="col-12">
            @include('message.message')
        </div>
    </div>
    <div class="d-flex justify-content-center h-100 my-3">
        <div class="admin_card mt-5">

            <div class="d-flex justify-content-center">
                <div class="brand_logo_container">
                    <img
                        src="{{url(isset($response['app_logo']) ? $response['app_logo'] : 'front/img/structure/websazandeh.png')}}"
                        class="brand_logo" alt="Logo">
                </div>
            </div>
            <div class="d-flex justify-content-center form_container">
                <form method="POST" action="{{ route('admin.login.submit') }}" class="px-3">
                @csrf

                <!-- USER NAME -->
                    <div class="input-group mt-5 direction-ltr">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="mobile" class="form-control input_user font-yekan text-right"
                               placeholder="نام کاربری">
                    </div>

                    <!-- PASSWORD -->
                    <div class="input-group my-3 direction-ltr">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" name="password" class="form-control input_pass font-yekan text-right"
                               placeholder="کلمه عبور">
                    </div>

                    <div class="form-row my-4">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                            <label class="admin-input-label d-block text-right" for="Capcha">حاصل جمع عبارت
                                امنیتی</label>
                            <input type="text"
                                   class="user-input form-control my-2 rounded shadow-none direction-ltr text-center"
                                   name="Capcha" id="Capcha">
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                            <div id="captcha" class="d-inline-block" style="width: 200px">
                                {!! captcha_img('math') !!}
                            </div>
                            <button type="button"
                                    class="btn btn-light border rounded btn-refresh mt-1"
                                    style="height: 50px">
                                <i class="fas fa-redo text-danger font-size-22"></i>
                            </button>
                        </div>

                    </div>

                    <!-- LOGIN -->
                    <div class="d-flex justify-content-center mt-3 login_container">
                        <button type="submit" name="loginadmin" class="btn login_btn font-yekan">ورود</button>
                    </div>

                </form>
            </div>

            <div class="mt-4">
                <div class="d-flex justify-content-center links">
                    <a href="{{route('admin.forget.form')}}"
                       class="decoration-none text-danger font-yekan font-size-12 font-weight-bold ">رمز عبور خود را
                        فراموش کرده ام</a>
                </div>
            </div>

        </div>
    </div>

</div>

<script src="{{url('/js/app11.js?v=11')}}"></script>

<script type="text/javascript">


    $(".btn-refresh").click(function () {

        $.ajax({

            type: 'GET',
            url: '/Api/refresh_captcha',
            success: function (data) {
                $("#captcha").html(data.captcha);
            }
        });
    });
</script>
</body>
</html>

