<section id="footer">

    <div class="container-fluid  footer-style">

        <div class="row row-footer">
            <div class="col-md-12  repeatable">

                <div class="container my-5">

                    <div class="row my-5">

                        <div class="col-md-6 mb-4">
                            <h2 class="text-right text-gold font-size-17 p-footer-style">
                               {{$response['app_name']}}
                            </h2>
                            <div class="text-justify text-white bold line-35 font-size-14"  id="txt-footer">
                                {!! $response['app_short_text'] !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h2 class="text-right text-gold font-size-17">
                                ارتباط با ما
                            </h2>
                            <ul  class="list-unstyled text-right mt-3 p-0">
                                @foreach(explode ("-", $response['app_tell']) as $tell)
                                    <li>
                                        <a class="text-white decoration-none font-size-14 fa-num text-right"
                                           href="tel:{{$tell}}">
                                            <i class="fas fa-phone text-gold"></i>
                                            &nbsp;  {{$tell}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="col-md-3">

                            <h2 class="text-right text-gold font-size-17">
                                لینک شبکه های اجتماعی
                            </h2>

                            <ul  class="list-unstyled text-right mt-3 p-0">
                                @foreach($response['socials'] as $social)
                                    @switch($social->key)
                                        @case ('instagram')
                                        <li class="list-inline-item">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال اینستگرام"
                                               data-placement="bottom"
                                               href="https://www.instagram.com/{{$social->value}}">
                                                <i class="fab fa-instagram fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('telegram')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال تلگرام"
                                               data-placement="bottom"
                                               href="https://www.t.me/{{$social->value}}">
                                                <i class="fab fa-telegram fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('linkedin')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال لینکدین"
                                               data-placement="bottom"
                                               href="https://www.linkedin.com/{{$social->value}}">
                                                <i class="fab fa-linkedin-in fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('youtube')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال یوتوب"
                                               data-placement="bottom"
                                               href="https://www.youtube.com/{{$social->value}}">
                                                <i class="fab fa-youtube fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('facebook')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال فیسبوک"
                                               data-placement="bottom"
                                               href="https://www.facebook.com/{{$social->value}}">
                                                <i class="fab fa-facebook-f fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('twitter')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال توییتر"
                                               data-placement="bottom"
                                               href="https://www.twitter.com/{{$social->value}}">
                                                <i class="fab fa-twitter fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('whatsapp')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال وتس اپ"
                                               data-placement="bottom"
                                               href="https://wa.me/{{$social->value}}">
                                                <i class="fab fa-whatsapp fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                    @endswitch


                                @endforeach
                            </ul>

                            <div>
                                <a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=174391&amp;Code=3MA5slFSulpfDkKn4q7s"><img referrerpolicy="origin" src="https://Trustseal.eNamad.ir/logo.aspx?id=174391&amp;Code=3MA5slFSulpfDkKn4q7s" alt="" style="cursor:pointer" id="3MA5slFSulpfDkKn4q7s"></a>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</section>


<!-- Footer -->
<footer class="bg-black py-2">
    <div class="container">
        <div class="small text-center text-gold font-weight-600">
            *کلیه حقوق مادی و معنوی این وبسایت متعلق به وب سایت  کیوان گلد می باشد*
        </div>
    </div>
</footer>

<footer class="bg-gold py-2">
    <div class="container">
        <div class="small text-center text-black font-weight-600">
            <a href="https://www.websazandeh.ir" class="decoration-none font-size-14 text-dark">
                طراحی توسط آکادمی نیما حمیدپور
            </a>
        </div>
    </div>
</footer>
