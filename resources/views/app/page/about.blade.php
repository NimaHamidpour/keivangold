@extends('app.index')

@section('canonical',\Request::url())

@section('ogtag')
    <meta property="og:type" content="Website" />
@endsection

@section('title',isset($response['meta']['title']) ?
            $response['meta']['title'] : 'درباره ما' )

@section('keywords',isset($response['meta']['keyword']) ?
            $response['meta']['keyword'] : $response['app_issue'])

@section('description',isset($response['meta']['description']) ?
            $response['meta']['description'] : $response['app_issue'])

@section('content')



    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="font-size-14 text-center text-black font-weight-bold mt-4">درباره ما</h2>
                    <hr class="divider"/>
                    <div class="font-yekan font-size-14 line-35 text-justify">
                        {!! $response['app_description'] !!}
                    </div>
                </div>

                <div class="col-12">
                    <h2 class="font-size-14 text-center text-black font-weight-bold mt-4">آدرس</h2>
                    <hr class="divider"/>
                    <div class="font-yekan font-size-14 line-35 text-center">
                        {!! $response['app_address'] !!}
                    </div>
                </div>

                <div class="col-12">
                    <h2 class="font-size-14 text-center text-black font-weight-bold mt-4">تلفن تماس</h2>
                    <hr class="divider"/>
                    <div class="font-yekan font-size-14 line-35 text-center mx-auto">
                            @foreach(explode ("-", $response['app_tell']) as $tell)
                                    <a class="text-dark decoration-none font-size-14 fa-num "
                                       href="tel:{{$tell}}">
                                        &nbsp;  {{$tell}} |
                                    </a>
                            @endforeach

                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection

@section('script')
    <script>

        $("#product-carousel").owlCarousel({
            loop: true,
            autoplay: true,
            items: 3,
            rtl: true,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
    </script>
@endsection
