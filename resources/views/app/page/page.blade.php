@extends('app.index')

@section('canonical',\Request::url())

@section('ogtag')
    <meta property="og:type" content="Website" />
@endsection

@section('title',isset($response['meta']['title']) ?
            $response['meta']['title'] : $response['page']->title )

@section('keywords',isset($response['meta']['keyword']) ?
            $response['meta']['keyword'] : $response['app_issue'])

@section('description',isset($response['meta']['description']) ?
            $response['meta']['description'] : $response['app_issue'])

@section('content')




    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3">
                    <img src="{{$response['page']->thumbnail}}" alt="{{$response['page']->title}}" class="img-fluid rounded">
                </div>
                <div class="col-12">
                    <h2 class="font-size-19 text-center text-black font-weight-bold mt-5">
                        {{$response['page']->title}}
                    </h2>
                    <hr class="divider"/>
                    <div class="font-yekan font-size-14 line-35 text-justify">
                        {!! $response['page']->content !!}
                    </div>
                </div>



            </div>
        </div>
    </section>



@endsection

@section('script')
    <script>

        $("#product-carousel").owlCarousel({
            loop: true,
            autoplay: true,
            items: 3,
            rtl: true,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
    </script>
@endsection
