@extends('app.index')

@section('title','سبد خرید شما')

@section('content')


    <section class="page-section">
        <div class="container text-center">
            <h2 class="font-yekan font-weight-bold font-size-22">سبد خرید شما</h2>
            <hr class="divider mb-5">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 mx-auto">
                    @if(session()->get('cart'))
                        <table class="table table-striped table-bordered text-center">
                            <tr>
                                <th>حذف</th>
                                <th>نام محصول</th>
                                <th>وزن</th>
                                <th>قیمت</th>
                            </tr>
                            @foreach(session()->get('cart') as $cart)
                                <tr>
                                    <td>
                                        <a class="decoration-none" href="{{route('api.cart.remove',$cart['id'])}}"><i class="fa fa-times text-danger"></i></a>
                                    </td>
                                    <td class="font-yekan font-size-14 text-primary font-weight-bold">
                                        {{$cart['title']}}
                                    </td>
                                    <td class="fa-num font-size-14 text-danger font-weight-bold">
                                        {{$cart['weight']}} گرم
                                    </td>
                                    <td class="fa-num font-size-14 text-success font-weight-bold">
                                        {{number_format($cart['price'])}} تومان
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>جمع کل</td>
                                <td colspan="3" class="fa-num font-weight-bold text-info">
                                    {{number_format($response['sum'])}}تومان
                                </td>
                            </tr>
                        </table>

                        <a class="btn btn-success btn-sm font-yekan font-size-14 text-decoration-none my-4"
                           href="#">پرداخت انلاین و خرید محصول</a>
                    @else
                        <div class="alert alert-warning">متاسفانه سبد خرید شما خالی می باشد</div>
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection


