@extends('app.index')

@section('canonical',\Request::url())

@section('ogtag')
    <meta property="og:type" content="Website" />
@endsection

@section('title',isset($response['meta']['title']) ?
            $response['meta']['title'] : $response['app_name'] )

@section('keywords',isset($response['meta']['keyword']) ?
            $response['meta']['keyword'] : $response['app_issue'])

@section('description',isset($response['meta']['description']) ?
            $response['meta']['description'] : $response['app_issue'])

@section('content')


    <!-- MASTHEAD -->
    <header class="masthead border-top-main">
        <div class="container h-100">
            <div class="row justify-content-center text-center">
                <div class="col-lg-10 mt-2">

                </div>

            </div>
        </div>
    </header>


    <!-- PHYSICAL TABLE -->
    {{--    <section class="page-section d-none">--}}
{{--        <div class="container-fluid text-center">--}}
{{--            <div class="row">--}}
{{--                <div class="col-12 text-center">--}}
{{--                    <h2 class="font-size-19 font-weight-bold">خرید و فروش نقدی</h2>--}}
{{--                    <hr class="divider">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row mx-0 mx-xl-5 mx-lg-5">--}}
{{--                <div class="col-md-6 border rounded p-3">--}}
{{--                    <h2 class="font-yekan font-weight-bold font-size-15 mt-3 text-primary">لیست خریداران</h2>--}}
{{--                    <hr class="divider mb-1">--}}
{{--                    <div class="table-responsive user-div-table mt-3">--}}
{{--                        <table class="table table-bordered table-striped">--}}
{{--                            <tr class="bg-primary text-white">--}}
{{--                                <td class="font-size-13">کد</td>--}}
{{--                                <td class="font-size-13">واحد</td>--}}
{{--                                <td class="font-size-13">مظنه(تومان)</td>--}}
{{--                                <td class="font-size-13">معامله</td>--}}
{{--                            </tr>--}}
{{--                            <tbody>--}}
{{--                            @foreach($response['buyer'] as $buyer)--}}
{{--                                <tr class="text-center font-size-13">--}}
{{--                                    <td class="user-danger">{{$buyer->id}}</td>--}}
{{--                                    <td class="user-black">{{number_format($buyer->weight)}}</td>--}}
{{--                                    <td class="user-black">{{number_format($buyer->amount)}}</td>--}}
{{--                                    <td>--}}
{{--                                        <a href="{{route('user.order.add',$buyer->id)}}" class="btn btn-outline-primary btn-sm font-yekan font-size-14"> معامله</a>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-6 border rounded p-3">--}}
{{--                    <h2 class="font-yekan font-weight-bold font-size-15 mt-3 text-danger">لیست فروشندگان</h2>--}}
{{--                    <hr class="divider mb-1">--}}
{{--                    <div class="table-responsive user-div-table mt-3 ">--}}
{{--                            <table class="table table-striped table-bordered border">--}}
{{--                                <tr class="font-size-13 text-white bg-danger">--}}
{{--                                    <td class="font-size-13">کد</td>--}}
{{--                                    <td class="font-size-13">واحد</td>--}}
{{--                                    <td class="font-size-13">مظنه(تومان)</td>--}}
{{--                                    <td class="font-size-13">معامله</td>--}}
{{--                                </tr>--}}
{{--                                <tbody>--}}
{{--                                @foreach($response['seller'] as $seller)--}}
{{--                                    <tr class="text-center font-size-13">--}}
{{--                                        <td class="user-danger">{{$seller->id}}</td>--}}
{{--                                        <td class="user-black">{{number_format($seller->weight)}}</td>--}}
{{--                                        <td class="user-black">{{number_format($seller->amount)}}</td>--}}
{{--                                        <td>--}}
{{--                                            <a href="{{route('user.order.add',$seller->id)}}" class="btn btn-outline-danger btn-sm font-yekan font-size-14"> معامله</a>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}


    <!-- INFO  -->


    <section class="page-section" style="background: #f8f7f7">
        <div class="container">

            <div class="row">
                <div class="col-12 text-center mx-auto">
                    <h2 class="font-yekan font-size-15 font-weight-bold text-center mt-3">{{$response['help']->title}}</h2>
                    <hr class="divider"/>
                </div>
            </div>
            <div class="row my-2">

                <div class="col-md-5">
                    <img src="{{url('app/img/help.jpg')}}" alt="{{$response['help']->title}}" class="img-fluid rounded">
                </div>
                <div class="col-md-7">
                    <article class="font-yekan font-size-14 line-35 text-justify direction-rtl">
                        {!! mb_substr( $response['help']->content,0,1000 ) !!} ...
                    </article>
                    <a href="{{route('app.otherpage','راهنمای-خرید')}}"
                       class="btn btn-outline-info btn-sm font-size-13 my-3">مشاهده ادامه مطلب</a>
                </div>
            </div>

        </div>
    </section>


    <!-- PRODUCT -->
    <section class="page-section blog mx-auto text-center">
        <div class="container pos-z-index">
            <h2 class="font-yekan font-weight-bold font-size-22">نمونه کارها</h2>
            <hr class="divider mb-1">
            <div class="tabs my-4">
                <a class="blog_group cursor-pointer font-size-12 font-weight-bold active" onclick="loadProduct(this,'all')">همه</a>

                @foreach($response['product_category'] as $category)
                    <a class="blog_group cursor-pointer font-size-12 font-weight-bold" onclick="loadProduct(this,{{$category->id}})">
                        {{$category->title}}
                    </a>
                @endforeach
            </div>

            <div id="product-view">
                <div class="carousel owl-carousel" id="product-carousel">
                    @foreach($response['product'] as $product)
                        <div class="item border border-warning rounded mx-1">
                              <img src="{{url($product->poster)}}" alt="{{$product->title}}"
                                         class="img-fluid" style="height: 260px;width: 320px">
                              <h2 class="title pt-3 border-top border-warning bg-light">{{$product->title}}</h2>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </section>



    <!-- BLOG -->
    @if(isset($response['blog_groups']) && count($response['blog_groups']) > 0 )
        <section class="blog mb-5" style="background: #f8f7f7">
            <div class="container pos-z-index">
                <div class="row mb-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="text-center mt-5 font-size-22 font-yekan font-weight-bold">بلاگ</h3>
                        <hr class="divider mb-1">
                    </div>
                </div>
                <div class="tabs">
                    @php $active='active' @endphp
                    @foreach($response['blog_groups'] as $bloggroup)
                        <a class="{{$active}} blog_group cursor-pointer" onclick="loadBlog(this,{{$bloggroup->id}})">{{$bloggroup->title}}</a>
                        @php $active='' @endphp
                    @endforeach
                </div>

                <div id="blogs-view">
                    <div class="carousel owl-carousel" id="blog-carousel">
                        @foreach($response['blogs'] as $blog)
                            <div class="item">
                                <a href="{{route('app.blog.show',$blog->slug)}}" class="text-decoration-none">
                                    <figure>
                                        <img src="{{url($blog->thumbnail)}}" alt="{{$blog->title}}">
                                    </figure>
                                    <h2 class="title text-center" style="min-height: 40px">{{$blog->title}}</h2>
                                    <p class="desc text-justify" style="min-height: 80px">
                                        {!! mb_substr(strip_tags($blog->content),0,140,'UTF8') !!}...
                                    </p>
                                    <div class="fa-num direction-ltr text-black font-size-14 font-weight-bold">
                                        {{jdate($blog->created_at)->format('Y %B')}}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </section>
    @endif


@endsection

@section('script')
    <script>

        $("#product-carousel").owlCarousel({
            loop: false,
            autoplay: false,
            items: 5,
            rtl:true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
        $("#blog-carousel").owlCarousel({
            margin: 40,
            loop: true,
            autoplay: true,
            items: 4,
            rtl: true,
            responsive: {
                0: {
                    items: 1
                },
                500: {
                    items: 2
                },
                800: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        });


    </script>
@endsection
