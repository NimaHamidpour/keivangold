@extends('app.index')

@section('title','سوالات متداول')

@section('content')


    <!-- FAQ -->
    <section class="page-section">
        <div class="container">

            @foreach($response['question'] as $category => $questions)
                <div class="row justify-content-center">
                    <div class="col-lg-10 text-center">
                        <div class="row mb-4">
                            <div class="accordion col-12">
                                <h2 class="font-size-14 bg-gold p-2 border-radius-5 font-weight-bold">
                                    {{$questions[0]->Category->title}}
                                </h2>
                                @foreach($questions as $question)
                                    <div class="card border">
                                        <div class="card-header py-1" id="question">
                                            <h2 class="align-right mb-0">
                                                <button
                                                    class="btn btn-link text-dark font-family-yekan align-right
                                                            font-weight-bold font-size-13 decoration-none collapsed"
                                                    data-toggle="collapse" data-target="#question{{$question->id}}"
                                                    aria-expanded="false" aria-controls="question{{$question->id}}">
                                                    {{$question->question}}
                                                </button>
                                                <i class="fas fa-plus float-left font-size-22 mt-2 text-gold"></i>
                                            </h2>
                                        </div>
                                        <div id="question{{$question->id}}" class="align-right direction-rtl collapse"
                                             aria-labelledby="question{{$question->id}}">
                                            <div class="card-body font-family-yekan font-size-13 mx-2 text-justify py-3 direction-rtl">
                                                {{$question->answer}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </section>

@endsection
