<!-- FIRST NAV -->
<div class="container-fluid bg-gold py-1">
    <div class="row direction-ltr">
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 text-xl-left text-lg-left text-md-left text-center direction-ltr">
                <span>
                    <a href="mailto:info@keivangold.ir" class="decoration-none font-size-11 font-weight-bold text-dark" data-toggle="tooltip" title="ایمیل" data-placement="bottom">
                        <i class="orange-color fa fa-envelope my-2"></i>&nbsp;
                        info@keivangold.ir
                    </a>
                </span>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12  text-xl-right text-lg-right text-md-right text-center ">
            &nbsp;<span class="mb-2 fa-num font-size-11 font-weight-bold text-dark">{{jdate()->format('%A, %d %B %y')}}</span>
        </div>
    </div>
</div>

<!-- LOGO -->
<div class="container-fluid  direction-ltr"
     style="background-image: url(app/img/banner.jpg)!important">

    <div class="row text-center py-2">
        <img src="{{url('app/img/logo.png')}}"  class="img-fluid mx-auto" alt="">
    </div>

</div>


<div class="container-fluid sticky-top">
    <!-- MENU -->
    <div class="row sticky-top">

        <!-- MENU DESKTOP -->
        <nav class="navbar navbar-expand-lg w-100 p-0 m-0 border-bottom-red direction-ltr d-none d-lg-block" id="blogNav">
            <div class="container-fluid">

                <div id="search_div">
                    @auth
                        <a href="{{route('login')}}" class="btn btn-warning btn-sm text-black">
                            {{\Illuminate\Support\Facades\Auth::user()->name . ' عزیز خوش آمدید '}}
                        </a>
                    @else
                        <a href="https://keivangold.ir/login" class="btn btn-warning btn-sm text-black">ورود | ثبت نام</a>
                    @endauth

                </div>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0 direction-rtl">

                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.home')}}"> صفحه اصلی</a>
                        </li>

                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr text-gold font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.otherpage','راهنمای-خرید-و-فروش-لحظه-ای')}}"> راهنمای خرید و فروش  نقدی</a>
                        </li>


                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.blog.home','آموزش')}}">مطالب آموزشی</a>
                        </li>
                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.blog.home')}}">بلاگ</a>
                        </li>

                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.faq')}}">سوالات متداول</a>
                        </li>

                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="{{route('app.about')}}">درباره ما</a>
                        </li>
                        <li class="nav-item align-right">
                            <a class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2" href="#footer">تماس با ما</a>
                        </li>

                    </ul>
                </div>

            </div>
        </nav>
        <!-- MENU DESKTOP -->

        <!-- MENU MOBILE -->
        <div class="col-12 d-block d-lg-none bg-blog-main direction-rtl">
            <div class="d-flex justify-content-between position-relative d-md-none d-block p-1">

                <button class="menuBtn btn " onclick="openMenu()" >
                    <i class="fas fa-bars font-size-22 color-blog-secondary bold"></i>
                </button>

                <div class="d-inline-block mx-auto mt-2">
                    <a href="{{route('app.blog.home')}}" class="font-family-yekan text-decoration-none  font-size-14  font-weight-bold color-blog-secondary">{{ $response['app_name']}}</a>
                </div>

                <button class="bg-transparent border-none shadow-none" id="search_btn" onclick="openSearchMobile()">
                    <i class="fa fa-search color-blog-secondary"></i>
                </button>
            </div>

            <nav class="mainNav" id="mobileMenu">
                <div class="mb-5 d-flex d-md-none w-100 justify-content-between align-items-center">
                    <a href="{{route('app.blog.home')}}" class="font-family-yekan text-decoration-none text-white font-size-17 font-weight-bold">
                        <img src=" {{url($response['app_logo'])}}" height="50px">
                    </a>
                    <button class="close font-38" onclick="openMenu()">
                        <i class="fas fa-times text-white"></i>
                    </button>
                </div>

                <ul class="list-unstyled bold">

                    <li>
                        @auth
                            <a href="{{route('login')}}" class="btn btn-warning btn-sm text-black">
                                {{\Illuminate\Support\Facades\Auth::user()->name . ' عزیز خوش آمدید '}}
                            </a>
                        @else
                            <a href="{{route('login')}}" class="btn btn-warning btn-sm text-black">ورود | ثبت نام</a>
                        @endauth
                    </li>

                    <li>
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="{{route('app.home')}}"> صفحه اصلی</a>
                    </li>

                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 text-gold text-decoration-none" href="{{route('app.otherpage','راهنمای-خرید-و-فروش-لحظه-ای')}}"> راهنمای خرید و فروش نقدی</a>
                    </li>

                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="{{route('app.blog.home','آموزش')}}">مطالب آموزشی</a>
                    </li>

                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="{{route('app.blog.home')}}">بلاگ</a>
                    </li>

                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="{{route('app.faq')}}">سوالات متداول</a>
                    </li>

                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="{{route('app.about')}}">درباره ما</a>
                    </li>
                    <li class="nav-item align-right">
                        <a class="direction-ltr font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none" href="#footer">تماس با ما</a>
                    </li>
                </ul>
            </nav>

            <div class="mainNav" id="mobileSearch">

                <div class="mb-5 d-flex d-md-none w-100 justify-content-between align-items-center">
                    <button class="close font-size-36" onclick="openSearchMobile()">
                        <i class="fas fa-times text-white"></i>
                    </button>
                </div>

                <div class="row">
                    <div class="col-11 search_form m-0 p-0" id="search_box_id">
                        <form action="{{route('app.blog.search')}}" method="post" class="mr-2">
                            @csrf
                            <h2 class="font-size-14 text-white text-center mb-4 font-family-yekan">جستجو</h2>
                            <input type="text" style="height: 34px!important;"
                                   class="form-control text-right font-family-iransans font-size-11
                                       shadow-none direction-rtl text-white border-bottom border-top-0 border-left-0 border-right-0 rounded-0 bg-black" autocomplete="off"
                                   onkeyup="suggest_search_blog_mobile()" id="search_text_mobile_id" name="search_text_name">
                            <div class="mt-3 pt-3 direction-rtl" id="aj_search_content_mobile">

                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- MENU MOBILE -->

    </div>
</div>





