@extends('user.index')


@section('content')

    @include('message.message')
    <div class="container">

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3 d-none">
                <a class="decoration-none" href="{{route('user.wallet.create')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-plus p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">شارژ کیف پول</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3 d-none">
                <a class="decoration-none" href="{{route('user.withdrawal.create')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-minus p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">برداشت کیف پول</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('user.portfo.list')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-chart-bar p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">لیست پرتفو</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('user.notification.list')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-bell p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">اطلاعیه ها</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                            {{$response['notification_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('user.ticket.list')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-comment p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">پیام های بی پاسخ</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                            {{$response['unread_message_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('user.user.edit')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-user p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">ویرایش مشخصات</div>
                        </div>
                        <div
                            class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                            id="count_waiting_id">
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
@endsection
