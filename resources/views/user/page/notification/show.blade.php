@extends('user.index')

@section('content')

   <div class="container px-4">

       <div class="row border rounded py-4">
           <div class="col-12 text-center mx-auto">
               <h2 class="font-yekan text-center font-size-17
                            text-danger mt-0 mt-xl-2 mt-lg-2 font-weight-bold">
                   {{$response['notification']->title}}
               </h2>
               <hr class="divider"/>
           </div>


           <div class="col-12 text-justify direction-rtl font-size-14 line-35">
               {!! $response['notification']->content !!}
           </div>
       </div>

   </div>

@endsection
