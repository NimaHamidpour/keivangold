@extends('user.index')

@section('content')

    @include('message.message')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped ">
            <tr class="user-table-hr-row">
                <th class="user-table-hr">ردیف</th>
                <th class="user-table-hr">عنوان</th>
                <th class="user-table-hr">تاریخ</th>
                <th class="user-table-hr">مشاهده</th>
            </tr>
            <tbody>
                @foreach($response['notification'] as $row => $notification)
                    <tr class="text-center font-size-13">
                        <td class="user-black">{{++$row}}</td>
                        <td class="user-black">{{$notification->title}}</td>

                        <td class="user-info">{{jdate($notification->created_at)->format('Y/m/d')}}</td>
                        <td>
                            <a href="{{route('user.notification.show',$notification->id)}}"
                               class="btn btn-outline-info font-size-13 pt-1 px-2 btn-sm">
                                مشاهده
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['notification']->links()}}
    </div>

@endsection
