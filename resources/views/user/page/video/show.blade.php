@extends('user.index')

@section('content')

    @include('message.message')
   <div class="container">
       <div class="row">
           <div class="col-12">
               <video class="w-100" controls>
                   <source src="{{url($response['video']->value)}}" type="video/mp4">
               </video>
           </div>
       </div>
   </div>

@endsection
