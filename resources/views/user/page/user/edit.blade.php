@extends('user.index')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-right">
                @include('message.message')
                <form action="{{route('user.user.update',$response['user']->id)}}" enctype="multipart/form-data"
                      method="POST" class="form my-3 admin-form">
                    @method('put')
                    @csrf

                    <div class="form-row mb-3">
                        <div class="col-md-6">
                            <div class="align-right mr-2">
                                <div class="image-upload">
                                    <label for="avatar">
                                        <span class="admin-input-label d-block">تصویر پروفایل</span>
                                        <img
                                            src="{{isset($response['user']->avatar) ? url($response['user']->avatar) : url('/app/img/avatar.jpg')}}"
                                            id="avatar-img"
                                            class="rounded-circle admin-form-img"/>
                                    </label>
                                    <input type="file" name="avatar" id="avatar" onchange="readURL(this,'avatar-img');"
                                           accept="image/x-png,image/gif,image/jpeg" class="d-none">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="align-right mr-2">
                                <div class="image-upload">
                                    <label for="kartmeli">
                                        <span class="admin-input-label d-block">
                                            تصویر کارت ملی
                                            <span class="text-danger font-size-11">(حداکثر حجم فایل انتخابی 1 مگابایت)</span>
                                        </span>
                                        <img
                                            src="{{isset($response['user']->kartmeli) ? url($response['user']->kartmeli) : url('/app/img/kartmeli.svg')}}"
                                            id="kartmeli-img"
                                            class="rounded admin-form-img"/>
                                    </label>
                                    <input type="file" name="kartmeli" id="kartmeli"
                                           onchange="readURL(this,'kartmeli-img');"
                                           accept="image/x-png,image/gif,image/jpeg" class="d-none">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="name-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbspنام</label>
                            <input type="text"
                                   class="form-control text-center user-input @error('name') is-invalid @enderror"
                                   id="name-id" name="name" placeholder="نام و فامیل"
                                   value="{{$response['user']->name}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="mobile-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbspشماره تلفن همراه</label>
                            <input type="text"
                                   class="form-control fa-num  text-center direction-ltr @error('mobile') is-invalid @enderror"
                                   id="mobile-id" name="mobile" placeholder="۰۹***"
                                   value="{{$response['user']->mobile}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>

                        <div class="col-md-6 mx-auto mb-4">
                            <label for="tell-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbspشماره تلفن ثابت</label>
                            <input type="text"
                                   class="form-control fa-num font-size-14 text-center direction-ltr @error('tell') is-invalid @enderror"
                                   id="tell-id"
                                   name="tell" placeholder="021***" value="{{$response['user']->tell}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="codemeli-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کد ملی</label>
                            <input type="text"
                                   class="form-control fa-num font-size-14 text-center direction-ltr @error('codemeli') is-invalid @enderror"
                                   id="codemeli-id" name="codemeli" placeholder="1234567891"
                                   value="{{$response['user']->codemeli}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                        <div class="col-md-6 mx-auto  mb-2">
                            <label for="friendShip" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;راه آشنایی </label>

                            <select class="form-control admin-input" style="text-align-last: center"
                                    name="friendShip" id="friendShip">
                                <option
                                    value="website" {{$response['user']->friendShip === 'website' ? 'selected' : ''}}>وب
                                    سایت
                                </option>
                                <option
                                    value="telegram" {{$response['user']->friendShip === 'telegram' ? 'selected' : ''}}>
                                    تلگرام
                                </option>
                                <option
                                    value="instagram" {{$response['user']->friendShip === 'instagram' ? 'selected' : ''}}>
                                    اینستاگرام
                                </option>
                                <option value="aparat" {{$response['user']->friendShip === 'aparat' ? 'selected' : ''}}>
                                    آپارات
                                </option>
                                <option value="family" {{$response['user']->friendShip === 'family' ? 'selected' : ''}}>
                                    آشنایی قبلی
                                </option>
                                <option value="other" {{$response['user']->friendShip === 'other' ? 'selected' : ''}}>
                                    سایر
                                </option>
                            </select>
                        </div>
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="sheba-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;شماره شبا</label>
                            <input type="text"
                                   class="form-control fa-num  font-size-14 text-center direction-ltr @error('sheba') is-invalid @enderror"
                                   id="sheba-id" name="sheba" placeholder="IR***" value="{{$response['user']->sheba}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="bankcard-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;شماره کارت</label>
                            <input type="text"
                                   class="form-control fa-num font-size-14 text-center direction-ltr @error('bankcard') is-invalid @enderror"
                                   id="bankcard-id" name="bankcard" placeholder="شماره کارت 16 رقمی"
                                   value="{{$response['user']->bankcard}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                        <div class="col-md-6 mx-auto mb-4">
                            <label for="bank-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام بانک</label>
                            <input type="text"
                                   class="form-control fa-num  font-size-14 text-center direction-ltr @error('bank') is-invalid @enderror"
                                   id="bank-id" name="bank" placeholder="نام بانک" value="{{$response['user']->bank}}"
                                {{\Illuminate\Support\Facades\Auth::user()->active === 'yes' ? 'disabled' : ''}}>
                        </div>
                    </div>

                    @if(\Illuminate\Support\Facades\Auth::user()->active !== 'yes')
                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit"
                                   value="ویرایش">
                        </div>
                    @endif

                </form>
            </div>
        </div>

    </div>

@endsection


@section('script')
    <script type="text/javascript">

        function readURL(input, target) {


            var uploadField = $(input);
            if(input.files[0].size > 102400){
                Swal.fire(
                    'حجم فایل انتخابی بالا می باشد',
                    'حجم فایل انتخابی باید زیر 1 مگابایت باشد',
                    'error'
                )
                $(input).val('');
            }
            else
            {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + target).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

        }
    </script>
@endsection
