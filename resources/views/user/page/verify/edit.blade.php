@extends('user.index')


@section('content')

    @include('message.message')
    <div class="container">

        <div class="row">
            <div class="col-12">
                <form class="user-form"
                      action="{{route('user.user.verify.update')}}" method="post">
                    @csrf
                    <div class="form-row text-right my-4">
                        <div class="col-6 mx-auto">
                            <label class="admin-input-label text-danger">شماره تلفن همراه</label>
                            <input class="form-control admin-input text-center font-size-14 fa-num"
                                   type="text" value="{{\Illuminate\Support\Facades\Auth::user()->mobile}}" readonly>
                        </div>
                    </div>

                    <div class="form-row text-right my-4">
                        <div class="col-6 mx-auto">
                            <label for="verifyCode" class="admin-input-label text-danger">کد دریافتی</label>
                            <input class="form-control admin-input text-center font-size-14 fa-num"
                                   placeholder="- - - - -"
                                   type="number" id="verifyCode" name="verifyCode">
                        </div>
                    </div>


                    <div class="form-row mt-5">
                        <input type="submit" class="btn col-md-4 font-weight-normal admin-submit" value="تایید شماره تلفن همراه">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
