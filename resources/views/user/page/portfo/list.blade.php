@extends('user.index')

@section('content')

    @include('message.message')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ضمانت(تومان)</td>
                <td class="font-size-13">اهرم</td>
                <td class="font-size-13"> واحد مجاز خرید</td>
                <td class="font-size-13"> واحد مجاز فروش</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">سود/زیان(تومان)</td>
                <td class="font-size-13">نحوه اتمام معامله</td>
                <td class="font-size-13"> لیست معاملات </td>
                <td class="font-size-13">پایان معاملات</td>
            </tr>
            <tbody>
                @foreach($response['portfo'] as $portfo)
                    <tr class="text-center font-size-13">
                        <td class="user-info">
                            {{number_format($portfo->amount)}}
                        </td>
                        <td class="user-success">{{$portfo->lever . ' خط '}}</td>

                        <td class="user-info">
                            {{$portfo->weight_buy }}
                        </td>

                        <td class="user-danger">
                            {{$portfo->weight_sell }}
                        </td>


                        @if($portfo->end_date === null && count(\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->get()) > 0)
                            <td class="user-info">
                                باز
                                ({{\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->sum('weight') . ' واحد '}}
                                {{\App\Models\Trade::firstWhere([['portfo',$portfo->id],['status','open']])->type === 'seller' ? 'فروش':'خرید'}})

                            </td>
                        @elseif($portfo->end_date === null && count(\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->get()) === 0)
                            <td class="user-danger">باز بدون معامله انجام شده</td>
                        @else
                            <td class="user-black">بسته</td>
                        @endif

                        <td class="{{$portfo->profit >= 0 ? 'user-success' : 'user-danger'}}">
                            <div class="direction-ltr d-inline-block fa-num">
                                {{number_format($portfo->profit)}}
                            </div>
                        </td>


                        @if($portfo->end_date !== null)
                            <td class="user-danger">پایان زمان معاملات</td>
                        @else
                            <td class="user-success">پایان نیافته</td>
                        @endif

                        <td>
                            <a href="{{route('user.trade.list',$portfo->id)}}"
                               class="btn btn-outline-primary btn-sm font-size-13">
                                 مشاهده
                            </a>
                        </td>

                        <td>
                            @if($portfo->weight_buy === $portfo->weight_sell && $portfo->end_date === null)
                                <a href="#" onclick="closeConfitm('{{route('user.portfo.close',$portfo->id)}}')"
                                   class="btn btn-outline-info btn-sm font-size-13">
                                    درخواست پایان معامله
                                </a>
                            @elseif($portfo->weight_buy === $portfo->weight_sell && $portfo->end_date !== null)
                                <button class="btn btn-success btn-sm font-size-13" disabled>تسویه شده</button>
                            @elseif($portfo->weight_buy !== $portfo->weight_sell)
                                <button class="btn btn-secondary btn-sm font-size-13" disabled>درخواست پایان معامله</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['portfo']->links()}}
    </div>

    @if($response['portfo_open'] === null)
        <section class="page-section direction-rtl  pt-0 border-top my-5">
        <div class="container-fluid my-5">
            <h2 class="font-size-13 text-danger line-35 text-justify direction-rtl font-weight-bold fa-num">
               توجه داشته باشید هر چه تعداد خط اهرم شما کمتر باشد مقدار وجه تضمینی مورد نیاز برای یک واحد معامله نیز کمتر است ولی با اهرم بالا در صورت ایجاد نوسان شدید احتمال کال مارجین (اتمام وجه تضمین و بسته شدن خودکار معامله) بیشتر می باشد.هر خط در مظنه در هر واحد برابر با
                {{ number_format($response['unit_trade'] * 230) }}
                تومان می باشد،
                که در نتیجه حداقل مبلغ برای یک واحد معامله با اهرم 50 خط برابر
                {{number_format($response['unit_trade'] * 230 * 50) }}
                تومان می باشد.
            </h2>
            <form  action="{{route('user.portfo.store')}}" method="POST" class="form user-form">
                @csrf
                <div class="form-row mt-3">

                    <div class="col-md-6 mb-4">
                        <label for="amount" class="user-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                                میزان وجه تضمین شما
                            <span class="text-danger font-size-13 fa-num">
                                ( تومان )
                            </span>
                        </label>
                        <input type="number" class="form-control user-input shadow-none"
                               name="amount" id="amount" placeholder="1"
                               autocomplete="off" value="{{old('amount')!= '' ? old('amount') : ''}}"
                                onkeyup="pledge_amount_portfo('{{$response['unit_trade']}}')">

                        <div class="font-weight-bold text-danger text-left direction-rtl font-size-11 fa-num"
                             id="help-amount">

                        </div>

                    </div>

                    <div class="col-md-6 mb-4">
                        <label for="lever" class="user-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i> اهرم </label>
                        <select class="form-control user-input" id="lever" name="lever" onchange="pledge_portfo('{{$response['unit_trade']}}')">
                            <option value="100">1X (100 خط)</option>
                            <option value="50" selected>2X (50 خط)</option>

                        </select>
                    </div>
                </div>


                <div class="alert alert-info text-center mt-3 font-weight-bold">
                    میزان ضمانت برابر است با :
                    <span class="fa-num border-bottom border-danger">( {{$response['unit_trade'] * 230 }} *  میزان مظنه ) * تعداد واحد</span>
                </div>

                <div class="alert alert-success text-center mt-3 font-weight-bold">
                   تعداد واحد مجاز معامله :
                    <span class="fa-num border-bottom border-danger" id="pledge-amount">
                            0
                    </span>
                </div>
                <div class="form-row mt-5">
                    <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 user-submit" value="تایید" >
                </div>

            </form>
        </div>
    </section>
    @endif
@endsection


@section('script')
    <script>
        function closeConfitm(url) {
            Swal.fire({
                title: 'آیا از بستن پرتفو اطمینان دارید؟',
                text: "وضعیت پرتفو مجددا قابل تغییر نمی باشد",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#08632d',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله',
                cancelButtonText: 'خیر'
            }).then(function (result) {
                if(result.value === true)
                {
                    location.href = url;
                }
            })

        }
    </script>
@endsection
