@extends('user.index')

@section('content')

    @include('message.message')

    <div class="container-fluid p-0 m-0">

        <!-- LAST TRADE AMOUNT -->
        <div class="row px-4 {{$response['trade_active_online_api_show'] === 'no' ? 'd-none' : ''}}">
            <div class="col-md-12 mx-auto text-center alert alert-warning font-weight-bold font-size-13">
                مظنه بازار :
                <span class="fa-num" id="trade_online_amount">
                    {{number_format($response['trade_online_amount'])}}
                </span>
                تومان
            </div>
        </div>

        <div class="row px-4">
            <div class="col-md-12 mx-auto text-center alert alert-info font-weight-bold font-size-13">
                آخرین معامله :
                <span class="fa-num" id="trade_last_amount">
                    {{number_format($response['trade_last_amount'])}}
                </span>
                تومان
            </div>
        </div>

        @if(isset($response['last_offer']) && $response['last_offer'] !== null)
        <div class="row">
            <div class="col-md-3 mx-2 text-xl-right text-lg-right text-md-center text-sm-center text-xs-center">
                <a href="{{route('user.tradelist.destroy',$response['last_offer']->id)}}"
                   class="form-control btn btn-outline-danger font-weight-bold font-size-11 fa-num">
                  حذف آخرین پیشنهاد شما :
                    ({{number_format($response['last_offer']->amount) . 'تومان'}})
                </a>
            </div>
        </div>
        @endif

        <!-- UNIT CAN TRADE -->
        @if(isset($response['portfo']) && $response['portfo'] !== null)
            <div class="row px-4">
                <div class="table-responsive  mt-3 ">
                    <table class="table table-bordered table-striped text-center">
                        <tr>
                            <th class="text-black font-size-11">وضعیت</th>
                            <th class="text-primary font-size-11">واحد مجاز خرید</th>
                            <th class="text-danger font-size-11">واحد مجاز فروش</th>
                            <th class="text-black font-size-11">سود و زیان</th>
                        </tr>
                        <tr>
                            <td class="font-size-11" id="portfo_status">
                                @if($response['portfo']->end_date === null && count(\App\Models\Trade::where([['portfo',$response['portfo']->id],['status','open']])->get()) > 0)
                                    <span class="user-info font-size-11">
                                    باز
                                    ({{\App\Models\Trade::where([['portfo',$response['portfo']->id],['status','open']])->sum('weight') . ' واحد '}}
                                        {{\App\Models\Trade::firstWhere([['portfo',$response['portfo']->id],['status','open']])->type === 'seller' ? 'فروش':'خرید'}}
                                    )

                                </span>
                                @elseif($response['portfo']->end_date === null && count(\App\Models\Trade::where([['portfo',$response['portfo']->id],['status','open']])->get()) === 0)
                                    <span class="user-danger font-size-11">باز بدون معامله انجام شده</span>
                                @else
                                    <span class="user-black font-size-11">بسته</span>
                                @endif
                            </td>
                            <td class="text-primary fa-num font-weight-bold font-size-11"
                                id="portfo_weight_buy">{{$response['portfo']->weight_buy}}</td>
                            <td class="text-danger fa-num font-weight-bold font-size-11"
                                id="portfo_weight_sell"> {{$response['portfo']->weight_sell }} </td>
                            <td id="portfo_profit"
                                class="font-size-11 fa-num font-weight-bold direction-ltr"
                                style="color: {{$response['portfo']->profit >= 0 ? '#008000' : '#dc3545'}}">
                                {{number_format($response['portfo']->profit)}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
    @endif


       <!-- TRADE LIST TABLE -->
        <div class="row">

            <!-- BUYER -->
            <div class="col-md-6">
                <div class="row">

                    <!-- CREATE BUYER -->
                    <div class="col-12">
                        <form action="{{route('user.tradelist.store','seller')}}"
                              onsubmit="$('#sell_submit').attr('disabled','disabled')"
                              method="POST" class="form user-form">
                            @csrf

                            <div class="row">
                                <div class="col-12 text-center">
                                    <h2 class="font-size-15 font-weight-bold text-danger">فروش طلا</h2>
                                    <hr class="divider"/>
                                </div>
                            </div>
                            <div class="form-row mt-3">
                                <div class="col-md-4 mb-4">
                                    <input type="hidden" value="" name="sell-code" id="sell-code">

                                    <input type="number"
                                           class="form-control user-input shadow-none direction-ltr text-center"
                                           name="sell_weight" id="sell_weight" placeholder="تعداد واحد"
                                           autocomplete="off" value="{{old('sell_weight')}}">
                                    <div id="help-weight"
                                         class="font-size-12 text-danger font-weight-bold fa-num text-left"></div>

                                </div>
                                <div class="col-md-4 mb-4">

                                    <input type="text"
                                           class="form-control user-input shadow-none direction-ltr text-center"
                                           name="sell_amount" id="sell_amount" placeholder="قیمت مظنه"
                                           autocomplete="off" value="{{old('sell_amount')}}" maxlength="4">
                                    <div id="help-sell"
                                         class="font-size-12 text-danger font-weight-bold fa-num text-left"></div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <input type="submit" class="btn form-control btn-sm btn-danger" id="sell_submit"
                                           value="تایید">
                                </div>

                            </div>
                        </form>

                    </div>

                    <!-- LIST BUYER -->
                    <div class="col-12">
                        <div class="table-responsive user-div-table mt-3 user-form">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <h2 class="font-size-15 font-weight-bold text-danger">فروش به بهترین پیشنهادهای
                                        خریداران</h2>
                                    <hr class="divider"/>
                                </div>
                            </div>
                            <div class="table-responsive  mt-3">
                                <table class="table table-striped">
                                    <tr class="bg-danger text-center text-white">
                                        <td class="font-size-14 border-left d-none d-xl-block d-lg-block">کد</td>
                                        <td class="font-size-14">واحد</td>
                                        <td class="font-size-14">مظنه</td>
                                        <td class="font-size-14">واحد مدنظر</td>
                                        <td class="font-size-14">معامله</td>
                                    </tr>
                                    <tbody id="tbody-buyer">
                                    @foreach($response['buyer'] as $buyer)
                                        <tr class="text-center font-size-13">
                                            <td class="user-danger border-left d-none d-xl-block d-lg-block">
                                                {{$buyer->id}}
                                            </td>

                                            <td class="user-info">
                                                {{number_format($buyer->weight)}}
                                                @if($buyer->parent_trade !== null)
                                                    <span class="text-danger font-size-11 font-weight-bold">(حـراج)</span>
                                                @endif
                                            </td>

                                            <td class="user-info">
                                                {{number_format($buyer->amount / 1000)}}
                                            </td>

                                            <td>
                                                @if($buyer->portfo == $response['portfo']->id)
                                                    <span class="text-user p-0 m-0">
                                                       <img src="{{url('app/img/star.svg')}}" class="m-0 p-0"
                                                            alt="star" height="13px" width="13">
                                                    </span>
                                                @else
                                                    <input type="number" value="{{$buyer->weight}}"
                                                           placeholder="تعداد واحد"
                                                           class="form-control font-size-13 mx-auto fa-num text-center direction-ltr"
                                                           style="width: 83px;height: 30px"
                                                           onkeyup="$('#order_weight_buyer_{{$buyer->id}}').val(this.value)">
                                                @endif
                                            </td>

                                            <td>
                                                @if($buyer->portfo == $response['portfo']->id)
                                                    <span class="text-user p-0 m-0">
                                                       <img src="{{url('app/img/star.svg')}}" class="m-0 p-0"
                                                            alt="star" height="13px" width="13">
                                                    </span>
                                                @else
                                                    <form action="{{route('user.trade.store',$buyer->id)}}"
                                                          method="post">
                                                        @csrf
                                                        <input type="hidden" name="order_weight_buyer"
                                                               id="order_weight_buyer_{{$buyer->id}}"
                                                               value="{{$buyer->weight}}">
                                                        <input type="submit" value="معامله"
                                                               class="btn btn-outline-danger btn-sm font-size-13"/>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- SELLER  -->
            <div class="col-md-6">
                <div class="row">

                    <!-- CREATE SELLER -->
                    <div class="col-12">
                        <form action="{{route('user.tradelist.store','buyer')}}"
                              onsubmit="$('#buy_submit').attr('disabled','disabled')"
                              method="POST" class="form user-form">
                            @csrf
                            <div class="row">
                                <div class="col-12 text-center">
                                    <h2 class="font-size-15 font-weight-bold text-primary">خرید طلا</h2>
                                    <hr class="divider"/>
                                </div>
                            </div>
                            <div class="form-row mt-3">
                                <div class="col-md-4 mb-4">
                                    <input type="hidden" value="" name="buy-code" id="buy-code">

                                    <input type="number"
                                           class="form-control user-input shadow-none direction-ltr text-center"
                                           name="buy_weight" id="buy_weight" placeholder="تعداد واحد"
                                           autocomplete="off" value="{{old('buy_weight')}}">
                                    <div id="help-weight"
                                         class="font-size-12 text-danger font-weight-bold fa-num text-left"></div>

                                </div>
                                <div class="col-md-4 mb-4">

                                    <input type="text"
                                           class="form-control user-input shadow-none direction-ltr text-center"
                                           maxlength="4"
                                           name="buy_amount" id="buy_amount" placeholder="قیمت مظنه"
                                           autocomplete="off" value="{{old('buy_amount')}}">
                                    <div id="help-buy"
                                         class="font-size-12 text-danger font-weight-bold fa-num text-left"></div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <input type="submit" class="btn form-control btn-sm btn-primary" id="buy_submit"
                                           value="تایید">
                                </div>

                            </div>


                        </form>

                    </div>

                    <!-- LIST SELLER -->
                    <div class="col-12">
                        <div class="table-responsive user-div-table mt-3 user-form">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <h2 class="font-size-15 font-weight-bold text-primary">خرید از بهترین پیشنهادهای
                                        فروشندگان</h2>
                                    <hr class="divider"/>
                                </div>
                            </div>
                            <div class="table-responsive mt-3 ">
                                <table class="table table-striped">
                                    <tr class="bg-primary text-center text-white">
                                        <td class="font-size-14 border-left d-none d-xl-block d-lg-block">کد</td>
                                        <td class="font-size-14">واحد</td>
                                        <td class="font-size-14">مظنه</td>
                                        <td class="font-size-14">واحد مدنظر</td>
                                        <td class="font-size-14">معامله</td>
                                    </tr>
                                    <tbody id="tbody-seller">
                                    @foreach($response['seller'] as $seller)
                                        <tr class="text-center font-size-13">
                                            <td class="user-danger border-left d-none d-xl-block d-lg-block">
                                                {{$seller->id}}
                                            </td>
                                            <td class="user-info">
                                                {{number_format($seller->weight) }}
                                                @if($seller->parent_trade !== null)
                                                    <span class="text-danger font-size-11 font-weight-bold">(حـراج)</span>
                                                @endif
                                            </td>
                                            <td class="user-info">
                                                {{number_format($seller->amount / 1000)}}
                                            </td>

                                            <td>
                                                @if($seller->portfo == $response['portfo']->id)
                                                    <span class="text-user p-0 m-0">
                                                       <img src="{{url('app/img/star.svg')}}" class="m-0 p-0"
                                                            alt="star" height="13px" width="13">
                                                    </span>
                                                @else
                                                    <input type="number" value="{{$seller->weight}}"
                                                           placeholder="تعداد واحد"
                                                           class="form-control fa-num font-size-13 mx-auto text-center direction-ltr"
                                                           style="width: 83px;height: 30px"
                                                           onkeyup="$('#order_weight_seller_{{$seller->id}}').val(this.value)">
                                                @endif

                                            </td>

                                            <td>
                                                @if($seller->portfo == $response['portfo']->id)
                                                    <span class="text-user p-0 m-0">
                                                       <img src="{{url('app/img/star.svg')}}" class="m-0 p-0"
                                                            alt="star" height="13px" width="13">
                                                    </span>
                                                @else
                                                    <form action="{{route('user.trade.store',$seller->id)}}"
                                                          method="post">
                                                        @csrf
                                                        <input type="hidden" name="order_weight_seller"
                                                               id="order_weight_seller_{{$seller->id}}"
                                                               value="{{$seller->weight}}">
                                                        <input type="submit" value="معامله"
                                                               class="btn btn-outline-primary btn-sm font-size-13"/>
                                                    </form>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <input type="hidden" id="close_check" value="">
        </div>

    </div>



@endsection

@section('script')
    <script>
        /*   دریافت معاملات جدید بر اساس خریدار یا فروشنده بودن  */
        var flag = 1;
        var x = setInterval(function () {
            if (flag === 1) {
                trade('buyer');
                flag = 0;
            } else {
                trade('seller');
                flag = 1;
            }
            if (document.getElementById('close_check').value === 'close') {
                location.reload();
            }
        }, 3500);
    </script>
@endsection
