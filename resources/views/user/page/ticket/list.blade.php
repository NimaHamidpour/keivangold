@extends('user.index')

@section('content')

    @include('message.message')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped ">
            <tr class="user-table-hr-row">
                <th class="user-table-hr"> شماره تیکت</th>
                <th class="user-table-hr">موضوع پیام</th>
                <th class="user-table-hr">الویت</th>
                <th class="user-table-hr">وضعیت</th>
                <th class="user-table-hr">تاریخ</th>
                <th class="user-table-hr">مشاهده</th>
            </tr>
            <tbody>
                @foreach($response['tickets'] as $ticket)
                    <tr class="text-center font-size-13">
                        <td class="user-black direction-ltr">{{$ticket->id}}</td>
                        <td class="user-black">{{$ticket->title}}</td>

                        <td class="user-danger">
                            @if($ticket->priority === 'low')
                                پایین
                            @elseif($ticket->priority === 'medium')
                                متوسط
                            @else
                                زیاد
                            @endif
                        </td>
                        <td>
                            @if($ticket->status==='admin-answer')
                                <span class="user-info">در انتظار پاسخ کارشناس پشتیبانی</span>
                            @elseif($ticket->status==='user-answer')
                                <span class="user-danger">در انتظار پاسخ کـاربر</span>
                            @else
                                <span class="user-success">پایان یافته</span>
                            @endif

                        </td>
                        <td class="user-info">{{jdate($ticket->created_at)->format('Y/m/d')}}</td>
                        <td>
                            <a href="{{route('user.ticket.show',$ticket->id)}}" class="btn btn-info pt-1 px-2 btn-sm">
                                مشاهده
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['tickets']->links()}}
    </div>

@endsection
