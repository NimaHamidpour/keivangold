@extends('user.index')

@section('content')

        <div class="table-responsive user-div-table mt-3">
            <table class="table table-striped ">
                <tr class="user-table-hr-row">
                    <td class="font-size-13">مبلغ(تومان)</td>
                    <td class="font-size-13">وضعیت</td>
                    <td class="font-size-13">تاریخ درخواست</td>
                </tr>
                <tbody>
                    @foreach($response['withdrawal'] as $withdrawal)
                        <tr class="text-center">
                            <td class="admin-success">{{number_format($withdrawal->amount)}}</td>
                            <td class="{{$withdrawal->status === 'waiting' ? 'admin-danger': 'admin-success'}}">{{$withdrawal->status === 'waiting' ? 'در حال انتظار': 'تسویه شده'}}</td>
                            <td class="admin-info direction-ltr">{{jdate($withdrawal->created_at)->format('Y/m/d')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-right">
                    @include('message.message')
                    <form  action="{{route('user.withdrawal.store')}}" method="POST" class="form my-3 admin-form">
                        @csrf


                        <div class="form-row mt-3">
                            <div class="col-md-6 mb-4">
                                <label for="amount-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                                    مبلغ قابل برداشت
                                </label>
                                <input type="text" class="form-control text-right user-input @error('amount') is-invalid @enderror"
                                       id="amount-id" name="amount" placeholder="5000000" autocomplete="off"
                                        onkeyup="digitGroup(this,'wallet-amount','تومان')">
                                <div class="text-danger font-size-12 mt-2 font-weight-bold text-left fa-num" id="wallet-amount">

                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="پرداخت">
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
