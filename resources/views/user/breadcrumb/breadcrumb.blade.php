<nav class="col-12">
    <ol class="user-breadcrumb">
        <li class="font-yekan font-size-14 text-black font-weight-bold">{{$response['page_title']}}</li>
    </ol>
</nav>
@if(\Illuminate\Support\Facades\Auth::user()->mobile_verified_at === null)
    <div class="container">
    <div class="row">
        <div class="col-12 px-4">
            <p class="alert alert-warning font-weight-bold font-size-13 text-justify direction-rtl">
                لطفا جهت استفاده از تمامی امکانات وب سایت طلای کیوان (انجام معاملات،برداشت از حساب و ...) از
                <a href="{{route('user.user.verify.edit')}}"
                   class="font-size-14 text-decoration-none text-danger font-italic font-weight-bold">این لینک</a>
                شماره تلفن همراه خود را تایید کنید.
            </p>
        </div>
    </div>
    </div>
@endif
