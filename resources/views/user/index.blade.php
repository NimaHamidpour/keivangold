<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon"  type="image/jpg" sizes="96x96" href="{{url((isset($response['app_logo']) ? $response['app_logo'] : 'front/img/structure/websazandeh.png'))}}">

    <title>پـنل کاربران {{$response['app_name']}}</title>

    <link rel="stylesheet" href="{{url('/css/app11.css?v=11')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="direction-rtl">

    @include('user.top.navbar')



    <div id="wrapper" class="wrapper-background">

        @include('user.side.sidebar')

        <!-- Main -->
            <div id="content-wrapper">
                <div class="container-fluid">

                    <!-- Breadcrumbs-->
                    @include('user.breadcrumb.breadcrumb')
                    <!-- Breadcrumbs-->

                    <div id="maindiv">
                        @yield('content')
                    </div>

                </div>
            </div>
         <!-- Main -->

    </div>

{{--    @include('back.down.footer')--}}


    <!-- Scroll to Top Buton-->
    <a class="scroll-to-top rounded" href="#wrapper">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- Scroll to Top Button-->

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="direction:rtl!important;text-align:right;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">

                        <div class="col-11 text-right">
                            <h5 class="modal-title" id="exampleModalLabel">از صفحه خارج می شوید؟</h5>
                        </div>
                        <div class="col-1 text-left mt-2">
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" style="float:right!important;">×</span>
                            </button>
                        </div>

                </div>
                <div class="modal-body">
                    اگر مطمئن هستید که می خواهید صفحه را ترک کنید روی گزینه خروج کلیک کنید.
                </div>
                <div class="modal-footer">
                    <form action="{{route('logout')}}" method="post">
                        @csrf
                        <input type="submit" class="btn btn-danger bold font-size-14" value="خروج">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Logout Modal-->

    <script src="{{url('/js/app11.js?v=11')}}"></script>
    @yield('script')

    <script>
        if ($(window).width() <768) {
            $(".sidebar").addClass("toggled");
        }
    </script>

</body>
</html>
