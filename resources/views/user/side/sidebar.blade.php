<!-- Sidebar -->
<ul class="sidebar navbar-nav p-0 m-0 sidebar-user-width sidebar-background ">


    <!-- NAME -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width pr-0 position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-angle-down arrow-left mt-3 text-gold d-none d-xl-inline-block d-lg-inline-block"></i>

            <img
                src="{{isset(\Illuminate\Support\Facades\Auth::user()->avatar) ? url(\Illuminate\Support\Facades\Auth::user()->avatar) : url('app/img/avatar.jpg')}}"
                style="height: 40px"
                class="rounded-circle mx-2 mb-1">

            <span class="font-family-yekan font-size-13 text-black font-weight-bolder" style="display: inline!important;">
                     {{\Illuminate\Support\Facades\Auth::user()->name }}
            </span>

            <div class="mx-auto text-center">
                <span class="font-size-11 text-danger font-weight-bold d-none d-xl-inline-block d-lg-inline-block">
                    موجودی :
                </span>
                <span class="fa-num text-success font-size-11 font-weight-bold">
                   {{number_format($response['wallet'])}}
                    تومان
                </span>
            </div>

        </a>
        <div class="dropdown-menu p-0 m-2 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('user.user.edit')}}" class="nav-link text-right text-dark">
                <i class="fas fa-user text-gold"></i>
                <span class="font-family-yekan font-size-13 font-weight-bold">ویرایش مشخصات</span>
            </a>

            <a href="{{route('user.user.edit.password')}}" class="nav-link text-right text-dark">
                <i class="fas fa-lock text-gold"></i>
                <span class="font-family-yekan font-size-13 font-weight-bold">تغییر کلمه عبور</span>
            </a>
        </div>
    </li>


    <!-- WALLET -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width position-relative w-100"
           href="#"  data-toggle="dropdown">
            <i class="fas fa-wallet font-size-22 ml-1 text-success"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold d-inline">
                   کیف پول
                <i class="fas fa-angle-down text-success arrow-left mt-1  d-none d-xl-inline-block d-lg-inline-block"></i>
            </span>

        </a>
        <div class="dropdown-menu p-0 m-2 position-static" aria-labelledby="pagesDropdown">

            <a href="https://keivangold.ir/user/wallet/create" class="nav-link text-right text-dark w-100  d-none">
                <i class="fas fa-plus text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">شارژ کیف پول</span>
            </a>

            <a href="{{route('user.withdrawal.create')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-money-check text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">درخواست برداشت وجه</span>
            </a>

            <a href="{{route('user.history')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-list text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">تاریخچه حساب</span>
            </a>

        </div>
    </li>


    <!-- DASHBOARD -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width w-100" href="{{route('user.dashboard')}}">
            <i class="fas fa-home font-size-22 ml-1 text-gold"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold d-inline">
                داشبورد
            </span>
        </a>
    </li>


    <!-- NOTIFICATION -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width w-100"
           href="{{route('user.notification.list')}}">
            <i class="fas fa-bell font-size-22 ml-1 text-gold"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold d-inline">
                اطلاعیه ها
            </span>
        </a>
    </li>


    <!-- TRADE -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width position-relative w-100" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-chart-bar font-size-22 text-gold"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold d-inline">
                معاملات نقدی
                <i class="fas fa-angle-down text-gold arrow-left mt-1  d-none d-xl-inline-block d-lg-inline-block"></i>
            </span>

        </a>
        <div class="dropdown-menu p-0 m-2 position-static" aria-labelledby="pagesDropdown">

            <hr class="my-0"/>
            <a href="{{route('user.portfo.list')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-layer-group text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">مشاهده پرتفو</span>
            </a>
            <a href="{{route('user.trade.tradeList')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-trademark text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">معاملات در جریان</span>
            </a>
            <a href="{{route('user.trade.video')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-graduation-cap text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">آموزش معاملات</span>
            </a>
        </div>
    </li>


    <!-- CHAT -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width position-relative w-100" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope font-size-22 ml-1 text-gold"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold d-inline">
                درخواست پشتیبانی
                <i class="fas fa-angle-down text-gold arrow-left mt-1 d-none d-xl-inline-block d-lg-inline-block"></i>
            </span>

        </a>
        <div class="dropdown-menu p-0 m-2 position-static" aria-labelledby="pagesDropdown">

            <hr class="my-0"/>
            <a href="{{route('user.ticket.add')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-envelope text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">ایجاد درخواست جدید</span>
            </a>

            <a href="{{route('user.ticket.list')}}" class="nav-link text-right text-dark w-100">
                <i class="fas fa-list text-gold"></i>
                <span class="text-dark font-size-12 font-weight-bold d-inline">لیست درخواست ها</span>
            </a>
        </div>
    </li>

</ul>
