@extends('blog.index')

@section('canonical',\Request::url())

@section('ogtag')
    <meta property="og:type" content="Website" />
@endsection

@section('title',isset($response['meta']['title']) ?
            $response['meta']['title'] : $response['app_name'] )

@section('keywords',isset($response['meta']['keyword']) ?
            $response['meta']['keyword'] : $response['app_issue'])

@section('description',isset($response['meta']['description']) ?
            $response['meta']['description'] : $response['app_issue'])

@section('content')

    <section class="page-section direction-rtl p-0">
        <div class="container-fluid bg-white pt-5">
            <div class="row">

                <!-- LAST UPLOAD -->
                <div class="col-lg-7 col-md-7 col-md-12 col-sm-12 col-xs-12 text-center mx-auto">
                    <div class="row direction-rtl mb-4">
                        @foreach($response['latest_content'] as $latest_content)
                            <div class="card border-none mb-4 mr-4">
                                <a class="d-content text-decoration-none cursor-pointer blog-hover"  href="{{route('app.blog.show',$latest_content->slug)}}">
                                    <div class="row no-gutters">

                                        <!-- IMG -->
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 pr-2">
                                            <img src="{{url($latest_content->thumbnail)}}"  class="card-img card-img-blog" alt="{{$latest_content->title}}">
                                        </div>

                                        <!-- TEXT -->
                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <div class="card-body">
                                                <h2 class="card-title text-right font-size-15 font-family-yekan font-weight-bold text-black">{{$latest_content->title}}</h2>
                                                <div class="col-12 text-right my-2 mx-0 px-0">
                                                    <a class="blog-category-link font-family-yekan" href="{{route('app.blog.home',$latest_content->Category->slug)}}">
                                                        {{$latest_content->Category->title}}
                                                    </a>
                                                    <span class="card-title text-right font-size-11 text-secondary fa-num mr-1">
                                                             {{jdate($latest_content->created_at)->format('Y.m.d')}}
                                                     </span>
                                                    <span class="view-counter fa-num">{{$latest_content->views}}</span>
                                                </div>
                                                <div class="card-text text-justify text-muted font-family-yekan mt-3 font-size-14">
                                                    {!! mb_substr(strip_tags($latest_content->content),0,200,'UTF8').'...' !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </div>
                        @endforeach
                        {{$response['latest_content']->links()}}
                    </div>
                </div>

                <!-- MOST VIEW -->
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 text-center mx-auto">

                    <div class="row">
                        <h3 class="font-weight-bold font-family-yekan border-bottom-secondary font-size-15 text-black pb-2 mx-auto width-20"> پربازدید ترین مقالات </h3>
                        @foreach($response['most_visited'] as $most_visited)
                            <div class="card border-none mx-3 pt-2">
                                <a class="text-decoration-none cursor-pointer blog-hover"  href="{{route('app.blog.show',$most_visited->slug)}}">
                                    <img class="card-img-top card-img-blog" src="{{$most_visited->thumbnail}}" alt="{{$most_visited->title}}">
                                    <div class="card-body px-0">
                                        <h2 class="card-title font-size-15 text-right font-family-yekan text-dark">{{$most_visited->title}}</h2>
                                        <div class="text-right font-size-11 text-secondary fa-num mb-2">
                                            {{jdate($most_visited->created_at)->format('Y.m.d')}}
                                            <span class="view-counter fa-num">{{$most_visited->views}}</span>
                                        </div>
                                        <div class="card-text font-size-14 text-muted text-right">
                                            {!! mb_substr(strip_tags($most_visited->content),0,150,'UTF8').'...' !!}
                                        </div>

                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>


                </div>

            </div>
        </div>
    </section>

@endsection


