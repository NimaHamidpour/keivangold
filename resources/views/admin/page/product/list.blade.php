@extends('admin.index')

@section('content')

    <section class="bg-white pt-0" id="welcome">

        <div class="table-responsive admin-div-table">
            <table class="table table-striped">
                <tr class="admin-table-hr-row">
                    <th class="admin-table-hr">تصویر</th>
                    <th class="admin-table-hr">عنوان</th>
                    <th class="admin-table-hr">وزن</th>
                    <th class="admin-table-hr">حذف</th>
                </tr>
                <tbody>
                @foreach($response['products'] as $product)
                        <tr class="text-center font-size-14">
                            <td><img src="{{url($product->poster)}}" class="admin-table-img"></td>
                            <td class="admin-info">{{$product->title}}</td>
                            <td class="admin-success">{{$product->weight}}</td>
                            <td>
                                <a href="{{route('admin.product.destroy',$product->id)}}" class="btn btn-outline-danger btn-sm font-yekan">
                                    حذف
                                </a>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{$response['products']->links()}}

        <div class="container-fluid my-5">
            @include('message.message')
            <form  action="{{route('admin.product.store')}}"
                   method="POST"  enctype="multipart/form-data"
                   class="form admin-form">
                @csrf
                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="poster">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{url('/app/img/image-add-button.png')}}" id="preview"
                                     class="img-thumbnail mt-2"
                                     height="100px" width="100px" style="border: 1px dashed #cecece"/>
                            </label>

                            <input type="file" name="poster" id="poster" onchange="readURL(this);"
                                   class="d-none">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-md-4 mb-4">
                        <label for="title_id" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            عنوان
                        </label>
                        <input type="text" class="form-control admin-input @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder=" عنوان ">
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="weight_id" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            وزن
                        </label>
                        <input type="text" class="form-control admin-input @error('weight') is-invalid @enderror"
                               id="weight_id" name="weight" placeholder=" وزن به گرم">
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="category" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            دسته بندی
                        </label>
                        <select class="form-control" name="category" id="category">
                           @foreach($response['category'] as $category)
                               <option value="{{$category->id}}">{{$category->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-12 mb-4">
                        <label for="description" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                           توضیحات
                        </label>
                        <textarea class="form-control admin-input @error('description') is-invalid @enderror"
                                  id="description" name="description" placeholder="توضیحات" style="height: 100px">{{old('description')}}</textarea>
                    </div>
                </div>

                <input type="hidden"  name="parent">
                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 btn admin-submit" value="اضافه کردن" >
                </div>
            </form>
        </div>
    </section>


@endsection

@section('script')
    <script>
        // **** PRIVIEW IMAGE
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
