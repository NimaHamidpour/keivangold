@extends('admin.index')


@section('content')
    @include('message.message')


    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-14">تصویر</td>
                <td class="font-size-14">نام</td>
                <td class="font-size-14">موبایل</td>
                <td class="font-size-14">تاریخ ثبت نام</td>
                <td class="font-size-14">سطح دسترسی</td>
                <td class="font-size-14">حذف</td>

            </tr>
            <tbody>
                @foreach($response['admins'] as $admin)
                    <tr class="text-center">
                        <td>
                            <img src="{{$admin->avatar !='' ? url($admin->avatar) : url('/app/img/avatar.jpg')}}" class="admin-table-img rounded-circle">
                        </td>
                        <td class="admin-black pt-4">{{$admin->name}}</td>
                        <td class="admin-danger pt-4">
                            {{$admin->mobile}}
                        </td>

                        <td class="admin-info pt-4">{{jdate($admin->created_at)->format('Y/m/d')}}</td>

                        <td>
                            <a class="btn btn-outline-success btn-sm text-decoration-none font-size-12 font-weight-bold mt-2"
                               href="{{route('admin.permission.list',$admin->id)}}">
                                سطح دسترسی
                            </a>
                        </td>

                        <td>
                            <a class="btn btn-outline-danger btn-sm text-decoration-none font-size-12 font-weight-bold mt-2"
                               href="{{route('admin.manager.destroy',$admin->id)}}">
                                حذف
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['admins']->links()}}
    </div>

@endsection
