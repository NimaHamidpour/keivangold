@extends('admin.index')


@section('content')
    @include('message.message')


    @if(count($response['permission_list']) > 0)
        <section class="direction-ltr bg-white pt-0">
            <div class="container-fluid my-5">
                <h1 class="admin-h1">ثبت سطح دسترسی جدید</h1>
                <form action="{{route('admin.permission.store',$response['admin']->id)}}"
                      method="POST" class="form admin-form">
                    @csrf

                    <div class="form-row mt-3">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label for="permission" class="admin-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;عنوان</label>
                            <select class="form-control admin-input" id="permission" name="permission">
                                @foreach($response['permission_list'] as $permission_list)
                                    <option class="admin-input"
                                            value="{{$permission_list->id}}">{{$permission_list->title}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <div class="form-row my-4">
                        <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                               value="ثبت">
                    </div>
                </form>
            </div>
        </section>

    @endif


    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-14">ردیف</td>
                <td class="font-size-14">سطح دسترسی</td>
                <td class="font-size-14">حذف</td>
            </tr>
            <tbody>
            @foreach($response['permission'] as $row => $permission)
                <tr class="text-center">
                    <td class="admin-danger">{{++$row}}</td>
                    <td class="admin-info">{{$permission->Permission->title}}</td>
                    <td>
                        <a class="btn btn-outline-danger btn-sm text-decoration-none font-size-12 font-weight-bold"
                           href="{{route('admin.permission.destroy',$permission->id)}}">
                            حذف
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@endsection
