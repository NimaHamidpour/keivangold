@extends('admin.index')

@section('content')

        <div class="container">

            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12 mx-auto text-right">
                    @include('message.message')
                    <form  action="{{route('admin.admin.update',$response['admin']->id)}}" enctype="multipart/form-data"
                           method="POST" class="form my-3 admin-form">
                        @method('put')
                        @csrf

                        <div class="form-row mb-3">
                            <div class="align-right mr-2">
                                <div class="image-upload">
                                    <label for="avatar">
                                        <span class="admin-input-label d-block">تصویر پروفایل</span>
                                        <img src="{{isset($response['admin']->avatar) && $response['admin']->avatar != '' ? url($response['admin']->avatar) : url('/app/img/avatar.jpg')}}" id="photo-img"
                                             class="rounded-circle admin-form-img"/>
                                    </label>
                                    <input type="file" name="avatar" id="avatar" onchange="readURL(this);"
                                           accept="image/x-png,image/gif,image/jpeg" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-3">
                            <div class="col-xl-12 col-lg-4 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                                <label for="name-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbspنام</label>
                                <input type="text" class="form-control text-right user-input @error('name') is-invalid @enderror" id="name-id" name="name" placeholder="نام و فامیل" value="{{$response['admin']->name}}">
                            </div>
                            <div class="col-xl-12 col-lg-4 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-4">
                                <label for="mobile-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbspشماره تلفن همراه</label>
                                <input type="text" class="form-control fa-num  text-right direction-ltr @error('mobile') is-invalid @enderror" id="mobile-id" name="mobile" placeholder="۰۹ ..." value="{{$response['admin']->mobile}}">
                            </div>
                        </div>

                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="ویرایش">
                        </div>

                    </form>
                </div>
            </div>

        </div>

@endsection


@section('script')
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
