@extends('admin.index')

@section('content')

    @include('message.message')

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">عنوان</th>
                <th class="admin-table-hr">تعداد بازدید</th>
                <th class="admin-table-hr">ویرایش </th>
                <th class="admin-table-hr">حذف </th>
            </tr>
            <tbody>
            @foreach($response['notification'] as $notification)
                <tr class="text-center font-size-13">

                    <td class="admin-info">{{$notification->title}}</td>
                    <td class="admin-info">
                        {{\App\Models\Visit::where([['model','Notification'],['model_id',$notification->id]])->count()}}
                    </td>

                    <td>
                        <a href="{{route('admin.notification.edit',$notification->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                            ویرایش
                        </a>
                    </td>
                    <td>
                        <a href="{{route('admin.notification.destroy',$notification->id)}}" class="btn btn-outline-danger btn-sm admin-a">
                            حذف
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['notification']->links()}}
    </div>

@endsection
