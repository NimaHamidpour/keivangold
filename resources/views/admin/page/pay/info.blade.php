@extends('admin.index')

@section('content')
    @include('message.message')


    <div class="container">

        <div class="row">

            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-primary text-white text-center">
                            <td class="font-size-13">تعداد پرداخت موفق کاربران</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['success_pay'])}}</td>
                            <td class="admin-info">{{number_format($response['success_pay']->sum('amount'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-info text-white text-center">
                            <td class="font-size-13">تعداد کیف پول های فعال</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['wallet'])}}</td>
                            <td class="admin-info">{{number_format($response['wallet']->sum('wallet'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-info text-white text-center">
                            <td class="font-size-13">تعداد پرتفو فعال</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['portfo'])}}</td>
                            <td class="admin-info">{{number_format($response['portfo']->sum('amount'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-info text-white text-center">
                            <td class="font-size-13">تعداد معاملات باز</td>
                            <td class="font-size-13">تعداد معاملات حراجی</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['open_trade'])}}</td>
                            <td class="admin-danger">{{count($response['onsale_trade'])}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-primary text-white text-center">
                            <td class="font-size-13">تعداد معاملات (کارمزد)</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['wage'])}}</td>
                            <td class="admin-info">{{number_format($response['wage']->sum('amount'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-primary text-white text-center">
                            <td class="font-size-13">تعداد پرداختی به کاربران</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['success_pay_to_user'])}}</td>
                            <td class="admin-info">{{number_format($response['success_pay_to_user']->sum('amount'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 my-4">
                <div class="table-responsive admin-div-table">
                    <table class="table table-striped table-bordered">
                        <tr class="bg-info text-white text-center">
                            <td class="font-size-13">تعداد پرداختی به کاربران در لیست انتظار</td>
                            <td class="font-size-13">مبلغ (تومان)</td>
                        </tr>
                        <tbody>
                        <tr class="text-center">
                            <td class="admin-danger">{{count($response['waiting_pay_to_user'])}}</td>
                            <td class="admin-info">{{number_format($response['waiting_pay_to_user']->sum('amount'))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div></div>


        </div>

    </div>


@endsection
