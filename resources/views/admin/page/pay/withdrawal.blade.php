@extends('admin.index')

@section('content')
    @include('message.message')

    <!-- SEARCH -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                <form class="admin-form pb-4 border-bottom direction-rtl" method="post" action="{{route('admin.pay.withdrawal.list')}}">
                    @csrf

                    <div class="form-row">

                        <div class="col-md-3 text-right my-2">
                            <input class="form-control admin-input" autocomplete="off" name="mobile" placeholder="موبایل" value="{{isset($response['mobile']) ? $response['mobile'] :''}}">
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <select class="form-control admin-input font-size-14" name="status" id="status" style="text-align-last: center">
                                <option value="all" hidden>وضعیت درخواست را انتخاب کنید</option>
                                <option value="success" {{isset($response['status']) && $response['status'] === 'success'  ? 'selected':''}}>تسویه شده</option>
                                <option value="waiting" {{isset($response['status']) && $response['status'] === 'waiting'  ? 'selected':''}}>لیست انتظار</option>
                            </select>
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input type="text" class="admin-input form-control px-3" id="start_date"
                                   placeholder="{{isset($response['start_date']) && $response['start_date'] !== null ? jdate($response['start_date'])->format('Y/m/d') : 'از تاریخ'}}"
                                   autocomplete="off">
                            <input type="text" class="d-none" id="start_date_hidden"
                                   value="{{isset($response['start_date']) && $response['start_date'] !== null ? $response['start_date'] : ''}}"
                                   name="start_date"/>
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input type="text" class="admin-input form-control  px-3" id="end_date"
                                   placeholder="{{isset($response['end_date']) && $response['end_date'] !== null ? jdate($response['end_date'])->format('Y/m/d') : 'تا تاریخ'}}"
                                   autocomplete="off">
                            <input type="text" class="d-none" id="end_date_hidden"
                                   value="{{isset($response['end_date']) && $response['end_date'] !== null ? $response['end_date'] : ''}}"
                                   name="end_date"/>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="جستجو" >
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">کاربر</td>
                <td class="font-size-13">مبلغ(تومان)</td>
                <td class="font-size-13">تاریخ درخواست</td>
                <td class="font-size-13">وضعیت</td>
            </tr>
            <tbody>
            @foreach($response['withdrawal'] as $row => $withdrawal)
                <tr class="text-center">
                    <td class="admin-danger pt-3">{{++$row}}</td>
                    <td>
                        <span class="admin-info d-block font-size-13">{{$withdrawal->User->name}}</span>
                        <span class="admin-danger font-size-13">{{$withdrawal->User->mobile}}</span>
                    </td>
                    <td class="admin-success pt-4">{{number_format($withdrawal->amount)}}</td>
                    <td class="admin-info direction-ltr pt-4">{{jdate($withdrawal->created_at)->format('Y/m/d')}}</td>
                    <td>
                        <a href="{{route('admin.pay.withdrawal.update',$withdrawal->id)}}" class="btn btn-sm  font-size-13 mt-2
                                        {{$withdrawal->status === 'waiting' ? 'btn-outline-danger': 'btn-outline-success'}}">
                            {{$withdrawal->status === 'waiting' ? 'در لیست انتظار': 'تسویه شده'}}
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

        {{$response['withdrawal']->appends(\Illuminate\Support\Facades\Request::all())->render()}}


    </div>

    <div class="row text-left direction-ltr">
        <a  href="{{route('admin.pay.withdrawal.excell')}}"
            class="btn btn-info col-md-3 btn-sm mx-5">
            <i class="fa fa-download"></i>
            خروجـی اکـسل
        </a>
    </div>

@endsection



@section('script')
    <script>
        $(document).ready(function () {
            $("#start_date").persianDatepicker({
                altField: '#start_date_hidden',
                altFormat: "X",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
                maxDate: 'today',
            });
            $("#end_date").persianDatepicker({
                altField: '#end_date_hidden',
                altFormat: "X",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
                maxDate: 'today',
            });
        });
    </script>
@endsection
