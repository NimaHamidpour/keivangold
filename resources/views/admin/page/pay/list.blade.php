@extends('admin.index')

@section('content')
    @include('message.message')

    <div class="table-responsive admin-div-table">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">مشخصات</th>
                <th class="admin-table-hr">کد رهگیری / پیگیری</th>
                <th class="admin-table-hr">مبلغ</th>
                <th class="admin-table-hr">زمان پرداخت</th>
            </tr>
            <tbody>
                @foreach($response['pays'] as $pay)
                    <tr class="text-center">
                        <td>
                            <span class="admin-info d-block">{{$pay->User->name}}</span>
                            <span class="admin-success">{{$pay->User->mobile}}</span>
                        </td>
                        <td>
                            <span class="admin-success d-block">{{$pay->authority}}</span>
                            <span class="admin-info">{{$pay->refid}}</span>
                        </td>
                        <td class="admin-danger">{{number_format($pay->amount) . ' تومان '}}</td>
                        <td class="admin-info direction-ltr ">{{jdate($pay->created_at)->format('Y/m/d - H:i')}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['pays']->links()}}
    </div>


@endsection
