@extends('admin.index')

@section('content')

    @include('message.message')

   <div class="row text-right">
       <div class="col-12 px-4">
           <a class="text-decoration-none btn btn-info text-right
                    btn-sm font-yekan font-size-14"
                    href="{{$response['type']=== 'blog' ? route('admin.blog.create','blog') : route('admin.analysis.create','analysis')}}">
               {{$response['type']=== 'blog' ? 'افزودن بلاگ جدید':'افزودن تحلیل جدید'}}
           </a>
       </div>
   </div>
    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">ایکون</th>
                <th class="admin-table-hr">عنوان</th>
                <th class="admin-table-hr">دسته</th>
                <th class="admin-table-hr">تعداد بازدید</th>
                <th class="admin-table-hr"> بازدید منحصر به فرد</th>
                <th class="admin-table-hr">ویرایش دسته</th>
                <th class="admin-table-hr">حذف دسته</th>
            </tr>
            <tbody>
                @foreach($response['pages'] as $page)
                    <tr class="text-center font-size-13">

                        <td>
                            <img class="admin-table-img" src="{{$page->thumbnail!='' ? url($page->thumbnail) : url('/front/img/structure/image-add-button.png')}}">
                        </td>
                        <td class="admin-info">{{$page->title}}</td>
                        <td class="admin-success">{{$page->type ==='blog' ? optional($page->Category)->title : optional($page->Category)->title}}</td>
                        <td class="admin-info">
                            {{\App\Models\Visit::where([['model','Page'],['model_id',$page->id]])->count()}}
                        </td>
                        <td class="admin-danger">
                            {{\App\Models\Visit::where([['model','Page'],['model_id',$page->id]])->distinct('ip')->count()}}
                        </td>
                        <td>
                            <a href="{{route('admin.blog.edit',$page->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                        </td>
                        <td>
                            <a href="{{route('admin.blog.destroy',$page->id)}}" class="btn btn-outline-danger btn-sm admin-a">
                                حذف
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['pages']->links()}}
    </div>

@endsection
