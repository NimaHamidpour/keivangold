@extends('admin.index')

@section('content')
    <section class="direction-ltr bg-white pt-0" id="welcome">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.blog.update', $response['page']->id)}}" method="POST" enctype="multipart/form-data"
                    class="form admin-form">
                @csrf
                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{ $response['page']->thumbnail!='' ? url( $response['page']->thumbnail) : url('/back/img/image-add-button.png')}}" id="slideshowfileimg"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="thumbnail" id="photo" onchange="readURL(this);"
                                   class="d-none" value="{{ $response['page']->thumbnail}}">
                        </div>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="title_id" class="admin-input-label bold"><i class="fas fa-pencil-alt prefix text-right"></i> عنوان مطلب  </label>
                        <input type="text" name="title" id="title_id" value="{{ $response['page']->title}}" class="admin-input form-control" placeholder="نام سایت">
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="category_id" class="admin-input-label bold"><i class="fas fa-pencil-alt prefix text-right"></i>دسته بندی مطلب </label>
                        <select id="category_id" name="category" class="form-control admin-input">
                            @foreach($response['parent'] as $parent)
                                <option value="{{$parent->id}}" class="admin-input" {{ $response['page']->parent==$parent->id ? 'selected' : ''}}>{{$parent->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="type_id" class="admin-input-label bold"><i class="fas fa-pencil-alt prefix text-right"></i> نوع مطلب </label>
                        <select id="type_id" name="type" class="form-control admin-input">
                            <option value="blog" class="admin-input" {{$response['page']->type === 'blog' ? 'selected' : ''}}>بلاگ</option>
                            <option value="analysis" class="admin-input" {{$response['page']->type === 'analysis' ? 'selected' : ''}}>تحلیل</option>
                        </select>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="description_id" class="admin-input-label bold"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp; محتوا</label>
                        <textarea class="form-control admin-input p-2 content" id="description_id" name="description" placeholder="محتوا">{{ $response['page']->content}}</textarea>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="ویرایش مطلب" >
                </div>
            </form>
        </div>
    </section>

@endsection


@section('script')

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script type="text/javascript">

        tiny();
        function tiny() {
            var editor_config = {
                path_absolute : "/",
                selector: "textarea.content",
                directionality:'rtl',
                height:'350px',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect fontselect fontsizeselect forecolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 22px 24px 36px",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
                    if (type === 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };
            tinymce.init(editor_config);
        }

        //** priview image
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection
