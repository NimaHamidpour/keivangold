@extends('admin.index')


@section('content')

    <div class="container">
        <div class="row">

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.user.list')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-user p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold"> کاربران سایت</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                             id="count_waiting_id">
                            {{$response['user_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.blog.list','blog')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-blog p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">مطالب منتشر شده </div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                             id="count_waiting_id">
                            {{$response['blog_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.pay.list','paid')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-check p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">پرداخت های موفق </div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                             id="count_waiting_id">
                           {{$response['paid_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.pay.list','unpaid')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-times p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">پرداخت های ناموفق </div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                             id="count_waiting_id">
                            {{$response['unpaid_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.portfo.list')}}">
                    <div class="card text-white bg-user o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-store p-3"></i>
                            </div>
                            <div class="text-center font-weight-bold">پرتفو های باز</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer fa-num  font-size-20 font-weight-bold"
                             id="count_waiting_id">
                            {{$response['portfo_count']}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="container">
                <div class="row px-3">
                    <div class="col-12 border border-danger rounded">

                        <div class="text-right mt-3">
                            <span class="user-danger font-size-15 font-family-yekan font-weight-bold">بازدید کلی : </span>
                            <span class="user-info font-size-14 font-family-iransans font-weight-bold"> {{ $response['view_all']}}</span>
                        </div>
                        <div class="post-stats__graph">
                            @if($response['max_view_count'] > 0 )
                                @for($i=10 ;$i >= 0;$i--)
                                    <div class="post-stats__bar"
                                         style="height: {{(($response['view_count'][$i] * 100)/$response['max_view_count'])}}px">
                                        <div class="post-stats__visits fa-num">{{$response['view_count'][$i]}}</div>
                                        <div class="post-stats__title fa-num">{{$response['view_date'][$i]}}</div>
                                    </div>
                                @endfor
                            @endif
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
