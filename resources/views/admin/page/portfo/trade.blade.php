@extends('admin.index')

@section('content')

    @include('message.message')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped ">
            <tr class="user-table-hr-row">
                <td class="font-size-13"> کد </td>
                <td class="font-size-13">نوع</td>
                <td class="font-size-13" >واحد</td>
                <td class="font-size-13">مظنه شروع (تومان)</td>
                <td class="font-size-13">مظنه پایان (تومان)</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">سود/زیان(تومان)</td>
                <td class="font-size-13">حد مجاز ضرر(تومان)</td>
                <td class="font-size-13">تاریخ</td>
            </tr>
            <tbody>
                @foreach($response['trade'] as $trade)
                    <tr class="text-center font-size-13">
                        <td class="user-black direction-ltr">{{$trade->id}}</td>
                        <td class="user-danger">
                            @if($trade->type === 'buyer')
                                خریدار
                            @else
                                فروشنده
                            @endif
                        </td>
                        <td class="user-black">{{number_format($trade->weight)}}</td>
                        <td class="user-black">{{number_format($trade->amount)}}</td>
                        <td class="user-black">{{$trade->end_amount != 0 ? number_format($trade->end_amount) : '--'}}</td>

                        <td>
                            @if($trade->status === 'open')
                                <span class="user-danger">باز</span>
                            @else
                                @if($trade->end_type === 'user')
                                    <span class="user-info">بسته</span>
                                @elseif($trade->end_type === 'onsale')
                                    <span class="user-danger">بسته (کال مارجین)</span>
                                @else
                                    <span class="user-info">بسته (پایان زمان معاملات)</span>
                                @endif
                            @endif
                        </td>
                        <td  class="direction-ltr {{$trade->profit >= 0 ? 'user-success' : 'user-danger'}}">
                              {{number_format($trade->profit) }}
                        </td>
                        <td class="user-danger">
                                @if($trade->type === 'buyer')
                                    {{number_format($trade->amount - (($trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                                @else
                                    {{number_format($trade->amount + (($trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                                @endif
                        </td>
                        <td class="user-secondary">{{jdate($trade->created_at)->format('Y/m/d')}}</td>

                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
