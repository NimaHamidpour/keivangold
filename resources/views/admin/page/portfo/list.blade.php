@extends('admin.index')

@section('content')
    @include('message.message')

    <!-- SEARCH -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                <form class="admin-form pb-4 border-bottom direction-rtl" method="post" action="{{route('admin.portfo.list')}}">
                   @csrf

                    <div class="form-row">

                        <div class="col-md-3 text-right my-2">
                            <input class="form-control admin-input" autocomplete="off" name="portfo" placeholder="شماره پرتفو" value="{{isset($response['portfo_id']) ? $response['portfo_id'] :''}}">
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input class="form-control admin-input" autocomplete="off" name="name" placeholder="نام" value="{{isset($response['user_name']) ? $response['user_name'] :''}}">
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input class="form-control admin-input" autocomplete="off" name="mobile" placeholder="موبایل" value="{{isset($response['user_mobile']) ? $response['user_mobile'] :''}}">
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <select class="form-control admin-input font-size-14" name="status" id="status" style="text-align-last: center">
                                <option value="all" hidden>وضعیت پرتفو را انتخاب کنید</option>
                                <option value="open" {{isset($response['status']) && $response['status'] === 'open' ? 'selected' : ''}}>
                                    پرتفوهای باز
                                </option>
                                <option value="openWithProgress" {{isset($response['status']) && $response['status'] === 'openWithProgress' ? 'selected' : ''}}>
                                    پرتفو های باز و در جریان
                                </option>
                                <option value="close" {{isset($response['status']) && $response['status'] === 'close' ? 'selected' : ''}}>
                                    پرتفوهای بسته
                                </option>
                            </select>
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input type="text" class="admin-input form-control px-3" id="start_date"
                                   placeholder="از تاریخ" autocomplete="off">
                            <input type="text" class="d-none" id="start_date_hidden" value="" name="start_date"/>
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <input type="text" class="admin-input form-control  px-3" id="end_date"
                                   placeholder="تا تاریخ " autocomplete="off">
                            <input type="text" class="d-none" id="end_date_hidden" value="" name="end_date"/>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="جستجو" >
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">کاربر</td>
                <td class="font-size-13">ضمانت(تومان)</td>
                <td class="font-size-13">اهرم</td>
                <td class="font-size-13"> مجاز خرید</td>
                <td class="font-size-13"> مجاز فروش</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">سود/زیان(تومان)</td>
                <td class="font-size-13">تاریخ ایجاد</td>
                <td class="font-size-13"> لیست معاملات </td>
            </tr>
            <tbody>
            @foreach($response['portfo'] as $row => $portfo)
                <tr class="text-center font-size-13">
                    <td class="admin-danger pt-3">{{++$row}}</td>
{{--                    <td>--}}
{{--                        <span class="admin-info font-size-12">{{$portfo->User->name}}</span>--}}
{{--                        <span class="admin-danger font-size-12 d-block">{{$portfo->User->mobile}}</span>--}}
{{--                    </td>--}}
                    <td class="admin-info font-size-12">{{$portfo->User->name}}</td>
                    <td class="user-black">
                        {{number_format($portfo->amount)}}
                    </td>
                    <td class="user-success">{{$portfo->lever}}</td>

                    <td class="user-info">
                        {{$portfo->weight_buy }}
                    </td>

                    <td class="user-danger">
                        {{$portfo->weight_sell }}
                    </td>


                    @if($portfo->end_date === null && in_array($portfo->id,$response['openTrade']))
                        <td class="user-info font-size-12">
                            باز
                            ({{\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->sum('weight') . ' واحد '}}
                            {{\App\Models\Trade::firstWhere([['portfo',$portfo->id],['status','open']])->type === 'seller' ? 'فروش':'خرید'}})

                        </td>
                    @elseif($portfo->end_date === null && count(\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->get()) === 0)
                        <td class="user-danger font-size-12">باز بدون معامله در جریان</td>
                    @else
                        <td class="user-black font-size-12">بسته</td>
                    @endif

                    <td class="{{$portfo->profit >= 0 ? 'user-success' : 'user-danger'}}">
                        <div class="direction-ltr d-inline-block fa-num">
                            {{number_format($portfo->profit)}}
                        </div>
                    </td>


                    <td class="user-secondary font-size-12">
                        {{jdate($portfo->created_at)->format('Y/m/d') }}
                    </td>

                    <td>
                        <a href="{{route('admin.portfo.show',$portfo->id)}}"
                           class="btn btn-outline-primary btn-sm font-size-13">
                            مشاهده
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['portfo']->appends(\Illuminate\Support\Facades\Request::all())->render()}}
    </div>


@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#start_date").persianDatepicker({
                altField: '#start_date_hidden',
                altFormat: "X",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
                maxDate: 'today',
            });
            $("#end_date").persianDatepicker({
                altField: '#end_date_hidden',
                altFormat: "X",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
                maxDate: 'today',
            });
        });
    </script>
@endsection
