@extends('admin.index')

@section('content')

    @include('message.message')

    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_writer'))
       <div class="row text-right">
           <div class="col-12 px-4">
               <a class="text-decoration-none btn btn-info text-right
                        btn-sm font-yekan font-size-14"
                        href="{{route('admin.question.create')}}">
                   افزودن سوال جدید
               </a>
           </div>
       </div>
    @endif

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">سوال</th>
                <th class="admin-table-hr">دسته</th>
                <th class="admin-table-hr">ویرایش دسته</th>
                <th class="admin-table-hr">حذف دسته</th>
            </tr>
            <tbody>
                @foreach($response['question'] as $question)
                    <tr class="text-center font-size-13">

                        <td class="admin-info">{{$question->answer}}</td>
                        <td class="admin-success">{{ optional($question->Category)->title }}</td>
                        <td>
                            <a href="{{route('admin.question.edit',$question->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                        </td>
                        <td>
                            <a href="{{route('admin.question.destroy',$question->id)}}" class="btn btn-outline-danger btn-sm admin-a">
                                حذف
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['question']->links()}}
    </div>

@endsection
