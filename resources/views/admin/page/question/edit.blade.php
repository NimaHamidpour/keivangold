@extends('admin.index')

@section('content')



    <section class="direction-ltr bg-white pt-0" id="welcome">
        <div class="container-fluid my-5">
            @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('writer_permission'))
                <div class="row text-right">
                    <div class="col-12 px-4">
                        <a class="text-decoration-none btn btn-info text-right
                        btn-sm font-yekan font-size-14"
                           href="{{route('admin.question.list')}}">
                            لیست سوالات
                        </a>
                    </div>
                </div>
            @endif
            @include('message.message')
            <form  action="{{route('admin.question.update',$response['question']->id)}}" method="POST"
                   class="form admin-form">
                @csrf

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="category_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i> دسته بندی سوال   </label>
                        <select id="category_id" name="category" class="form-control admin-input">
                            @foreach($response['parent'] as $parent)
                                <option value="{{$parent->id}}" class="admin-input" {{$response['question']->category === $parent->id ? 'selected' :''}}>
                                    {{$parent->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="question" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            سوال
                        </label>
                        <textarea class="form-control admin-input p-2 " id="question" name="question" placeholder="سوال">{{$response['question']->question}}</textarea>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="answer" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            پاسخ
                        </label>
                        <textarea class="form-control admin-input p-2 " id="answer" name="answer" placeholder="پاسخ">{{$response['question']->answer}}</textarea>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="ویرایش اطلاعات" >
                </div>
            </form>
        </div>
    </section>

@endsection


