@extends('admin.index')

@section('content')

    <div class="container-fluid direction-ltr pt-0">

        @foreach($response['reply'] as $reply)
            <div class="row px-3 text-right direction-rtl">
                <div
                    class="col-12 pt-3 text-right border rounded mt-2
                        {{$reply->writer ==='Admin' ? 'bg-light-pink' : 'bg-light'}}">
                     <span class="text-right {{$reply->writer ==='Admin' ? 'admin-info':'admin-danger'}}">
                            {{$reply->writer ==='Admin'  ? 'کارشناس پشتیبانی :  '.$reply->Admin->name : 'کـاربر : '. $reply->User->name}}
                      </span>
                    <span class="float-left {{$reply->writer ==='Admin' ? 'admin-info':'admin-danger'}}">
                           {{jdate($reply->created_at)->format('Y.m.d')}}
                     </span>
                    <hr/>
                    <p class="font-family-yekan font-size-14 line-35 text-right text-justify mb-4">
                        {!! str_replace(PHP_EOL,"<br/>",$reply->message) !!}
                    </p>

                    @if(count($reply->MetaKey('file')->get()) > 0)
                        <span class="text-black font-family-yekan font-size-13 font-weight-bold">
                        پیوست
                        </span>
                        <div class="row direction-rtl border-top mt-2 p-2">
                            @foreach($reply->MetaKey('file')->get() as $row => $item)
                                <a href="{{url($item->value)}}" target="_blank"
                                   class="text-decoration-none
                                   fa-num font-size-13 btn btn-info btn-sm mx-2">
                                    <i class="fa fa-download"></i>
                                    فایل
                                    {{++$row}}
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @endforeach


            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-xs-12 mt-5 text-center mx-auto">
                    <a href="{{route('admin.ticket.finished',$response['ticket']->id)}}"
                       class="btn btn-success decoration-none font-size-14 font-family-yekan mx-auto px-5">بستن درخواست
                        پشتیبانی</a>
                </div>
            </div>

    </div>


    <!-- CHAT BOX -->

        <section class="page-section direction-rtl bg-white pt-0 border-top my-5">
            <div class="container-fluid my-5">
                @include('message.message')
                <form action="{{route('admin.conversation.store')}}" method="POST" enctype="multipart/form-data"
                      class="form text-right">
                    @csrf
                    <div class="form-row mt-3 border-bottom">
                        <input hidden value="Ticket" name="parentType">
                        <input hidden value="{{$response['ticket']->id}}" name="parent">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label for="description" class="admin-input-label">
                                <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;محتوا پیام</label>
                            <textarea class="form-control admin-input shadow-none text-right direction-rtl"
                                      style="height: 250px" id="message" name="message"
                                      placeholder="پیام">{{old('message')}}</textarea>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                            <label for="attchment">
                            <span class="bg-info text-white px-4 rounded" style="cursor: pointer">
                                <i class="fa fa-plus text-white"></i>&nbsp;
                                افزودن فایل</span>
                                <span class="admin-success font-size-14 d-block m-3" id="upload_count"></span>
                                <ul class="mt-4 mx-0 px-0">
                                    <li class="font-size-13 font-family-yekan m-3 text-danger font-weight-bold direction-rtl">
                                        فایل
                                        های مجاز : <span
                                            class="text-primary border-bottom border-primary">png - jpg  - jpeg</span>
                                    </li>
                                    <li class="font-size-13 font-family-yekan m-3 text-danger font-weight-bold direction-rtl">
                                        فایل
                                        ها را به صورت <span
                                            class="text-primary border-bottom border-primary">یکجا</span>
                                        انتخاب کنید.
                                    </li>
                                    <li class="font-size-13 font-family-yekan m-3 text-danger font-weight-bold direction-rtl">
                                        حداکثر تعداد فایل : <span
                                            class="text-primary border-bottom border-primary">2</span>
                                    </li>

                                </ul>

                            </label>
                            <input type="file" name="attchment[]" id="attchment" class="d-none"
                                   accept="image/x-png,image/jpeg" multiple onchange="uploadcount(this)">
                        </div>
                    </div>

                    <div class="form-row mt-3 direction-rtl">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                            <label class="admin-input-label d-block text-right" for="Capcha">وارد کردن تصویر
                                امنیتی</label>

                            <div class="row direction-ltr">

                                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12  mb-3">
                                    {!! captcha_img('math') !!}
                                </div>

                                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <input type="text"
                                           class="admin-input form-control rounded shadow-none direction-ltr text-center"
                                           name="Capcha" id="Capcha">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-row mt-5">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit"
                               value="تایید">
                    </div>


                </form>
            </div>
        </section>


@endsection



@section('script')
    <script>
        function uploadcount(parent) {
            var text = ' تعداد فایل های آپلود شده : ' + $("input:file")[0].files.length + ' عدد ';
            $('#upload_count').text(text);
        }
    </script>
@endsection
