@extends('admin.index')

@section('content')

    @include('message.message')



    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">ش.تیکت</td>
                <td class="font-size-13">نام کاربر</td>
                <td class="font-size-13">موضوع پیام</td>
                <td class="font-size-13">الویت</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">تاریخ</td>
                <td class="font-size-13">مشاهده</td>
            </tr>
            <tbody>
                @foreach($response['tickets'] as $row => $ticket)
                    <tr class="text-center font-size-13">
                        <td class="admin-danger">{{++$row}}</td>
                        <td class="admin-black direction-ltr">{{$ticket->id}}</td>
                        <td class="admin-info">{{$ticket->User->name}}</td>

                        <td class="admin-black">{{$ticket->title}}</td>

                        <td class="admin-black">
                            @if($ticket->priority === 'low')
                                پایین
                            @elseif($ticket->priority === 'medium')
                                متوسط
                            @else
                                زیاد
                            @endif
                        </td>
                        <td>
                            @if($ticket->status==='admin-answer')
                                <span class="admin-danger">در انتظار پاسخ کارشناس </span>
                            @elseif($ticket->status==='user-answer')
                                <span class="admin-info">در انتظار پاسخ کـاربر </span>
                            @else
                                <span class="admin-success">پایان یافته</span>
                            @endif

                        </td>
                        <td class="admin-info">{{jdate($ticket->created_at)->format('Y/m/d')}}</td>
                        <td>
                            <a href="{{route('admin.ticket.show',$ticket->id)}}" class="btn btn-outline-info font-size-13 pt-1 px-2 btn-sm">
                                مشاهده
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['tickets']->links()}}
    </div>

@endsection
