@extends('admin.index')

@section('content')
    <!-- CHAT BOX -->
    <section class="page-section direction-rtl pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form  action="{{route('admin.ticket.store')}}" method="POST" enctype="multipart/form-data"
                   class="form user-form">
                @csrf

                <div class="form-row mt-3">

                    <div class="col-md-4 mb-4">
                        <label for="category" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام کاربر</label>
                        <input type="text" class="form-control admin-input" autocomplete="off"
                               id="user_search" name="user_search" placeholder="نام کاربر"
                               onkeyup="suggest_user_admin(this,'aj_search_content')"
                                value="{{old('user_search')}}">

                        <div class="direction-rtl text-center" id="aj_search_content">

                        </div>
                        <input type="hidden" name="user" id="user" value="{{old('user')}}">
                    </div>

                    <div class="col-md-4 mb-4">
                        <label for="title" class="user-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;موضوع</label>
                        <input type="text" class="form-control user-input shadow-none"
                               name="title" id="title" placeholder="موضوع پیام"
                               autocomplete="off" value="{{old('title')}}">
                    </div>

                    <div class="col-md-4 mb-4">
                        <label for="priority" class="user-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;اولویت</label>
                        <select class="form-control user-input" id="priority" name="priority">
                            <option value="low">کم</option>
                            <option value="medium">متوسط</option>
                            <option value="high">زیاد</option>

                        </select>
                    </div>
                </div>


                <div class="form-row mt-3 border-bottom">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="message" class="user-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;پیام</label>
                        <textarea class="form-control user-input shadow-none" style="height: 250px" id="message" name="message" placeholder="پیام">{{old('message')}}</textarea>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="attchment">
                            <span class="bg-info text-white px-4 rounded" style="cursor: pointer">
                                <i class="fa fa-plus text-white"></i>&nbsp;
                                افزودن فایل</span>
                            <span class="admin-success font-size-14 d-block m-3" id="upload_count"></span>
                            <ul class="mt-4 mx-0 px-0">
                                <li class="font-size-13 font-yekan m-3 text-danger font-weight-bold direction-rtl">فایل های مجاز : <span class="text-primary border-bottom border-primary">png - jpg  - jpeg</span> </li>
                                <li class="font-size-13 font-yekan m-3 text-danger font-weight-bold direction-rtl">فایل ها را به صورت <span class="text-primary border-bottom border-primary">یکجا</span> انتخاب کنید.</li>

                            </ul>

                        </label>
                        <input type="file" name="attchment[]" id="attchment" class="d-none" accept="image/x-png,image/jpeg" multiple onchange="uploadcount(this)">
                    </div>
                </div>

                <div class="form-row mt-3 direction-rtl">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                        <label class="admin-input-label d-block text-right" for="Capcha">وارد کردن تصویر امنیتی</label>

                        <div class="row direction-ltr">

                            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12  mb-3">
                                {!! captcha_img('math') !!}
                            </div>

                            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="admin-input form-control rounded shadow-none direction-ltr text-center" name="Capcha" id="Capcha">
                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-row mt-5">
                    <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 user-submit" value="تایید" >
                </div>


            </form>
        </div>
    </section>
    <!-- WELCOME -->


@endsection



@section('script')
    <script>
        function uploadcount(parent) {
            var text=' تعداد فایل های آپلود شده : ' + $("input:file")[0].files.length + ' عدد ';
            $('#upload_count').text(text);
        }
    </script>
 @endsection
