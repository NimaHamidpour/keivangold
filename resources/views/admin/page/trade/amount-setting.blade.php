@extends('admin.index')

@section('content')

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')

            <form action="{{route('admin.onlinetradeSetting.update')}}" method="POST"
                  class="form admin-form">
                @csrf

                <div class="form-row  rounded p-4">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="trade_last_amount" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            آخرین قیمت معامله شده
                            <span class="text-danger font-size-13 font-weight-bold">(تومان)</span>
                        </label>
                        <input type="number"
                               class="form-control  admin-input text-center direction-ltr  @error('trade_last_amount') is-invalid @enderror"
                               id="trade_last_amount" name="trade_last_amount" placeholder="آخرین قیمت مظنه"
                               onkeyup="digitGroup(this,'help-last-amount','تومان')" autocomplete="off"
                               value="{{$response['trade_last_amount']!='' ? $response['trade_last_amount']->value : 0}}">
                        <div class="text-danger font-size-12 font-weight-bold fa-num text-left mt-2"
                             id="help-last-amount">
                            {{$response['trade_last_amount']!='' ? number_format($response['trade_last_amount']->value) .'تومان' : 0}}
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="trade_max_dispute" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            بیشترین حد مجاز اختلاف قیمت وارد شده
                            <span class="text-danger font-size-13 font-weight-bold fa-num">(20 خط برابر 20,000 تومان می باشد)</span>
                        </label>
                        <input type="text"
                               class="form-control  admin-input text-center direction-ltr  @error('trade_max_dispute') is-invalid @enderror"
                               id="trade_max_dispute" name="trade_max_dispute"
                               placeholder="بیشترین حد مجاز اختلاف قیمت وارد شده"
                               value="{{$response['trade_max_dispute']!='' ? $response['trade_max_dispute']->value : 0}}">
                    </div>
                </div>

                <div class="form-row">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ویرایش اطلاعات">
                </div>

            </form>

            <form action="{{route('admin.onlinetradeSetting.update')}}" method="POST"
                  class="form admin-form">
                @csrf

                <div class="form-row rounded p-4">
                    <div class="col-md-4 mb-4">
                        <label for="trade_online_amount" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            آخرین قیمت مظنه API
                            <span class="text-danger font-size-13 font-weight-bold">(تومان)</span>
                        </label>
                        <input type="number"
                               class="form-control  admin-input text-center direction-ltr  @error('trade_last_amount') is-invalid @enderror"
                               id="trade_online_amount" name="trade_online_amount" placeholder="آخرین قیمت مظنه"
                               onkeyup="digitGroup(this,'help-api','تومان')" autocomplete="off"
                               value="{{$response['trade_online_amount']!='' ? $response['trade_online_amount']->value : 0}}">
                        <div class="text-danger font-size-12 font-weight-bold fa-num text-left mt-2" id="help-api">
                            {{$response['trade_online_amount']!='' ? number_format($response['trade_online_amount']->value) .'تومان' : 0}}
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="trade_active_online_api" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            فعال بودن API
                            <span class="text-danger font-size-13">(دریافت اتوماتیک API)</span>
                        </label>
                        <select name="trade_active_online_api" id="trade_active_online_api"
                                class="form-control admin-input" style="text-align-last: center">
                            <option
                                value="yes" {{$response['trade_active_online_api']->value === 'yes' ? 'selected' : ''}}>
                                فعال
                            </option>
                            <option
                                value="no" {{$response['trade_active_online_api']->value === 'no' ?  'selected' : ''}}>
                                غیر فعال
                            </option>
                        </select>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="trade_active_online_api_show" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            نمایش API برای کاربران
                        </label>
                        <select name="trade_active_online_api_show" id="trade_active_online_api_show"
                                class="form-control admin-input" style="text-align-last: center">
                            <option
                                value="yes" {{$response['trade_active_online_api_show']->value === 'yes' ? 'selected' : ''}}>
                                فعال
                            </option>
                            <option
                                value="no" {{$response['trade_active_online_api_show']->value === 'no' ?  'selected' : ''}}>
                                غیر فعال
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ویرایش اطلاعات">
                </div>
            </form>

        </div>
    </section>

@endsection

