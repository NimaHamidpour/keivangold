@extends('admin.index')

@section('content')

    @include('message.message')

    <!-- SEARCH -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                <form class="admin-form pb-4 border-bottom direction-rtl" method="post" action="{{route('admin.trade.list')}}">
                    @csrf

                    <div class="form-row">

                        <div class="col-md-3 text-right my-2">
                            <input class="form-control admin-input" autocomplete="off"
                                   name="trade" placeholder="کد معامله"
                                   value="{{isset($response['trade_id']) ? $response['trade_id'] :''}}">
                        </div>


                        <div class="col-md-3 text-right my-2">
                            <select class="form-control admin-input font-size-14" name="type" id="type" style="text-align-last: center">
                                <option value="buyer"
                                    {{isset($response['trade_type']) && $response['trade_type'] === 'buyer' ? 'selected' : ''}}>
                                    خریدار
                                </option>
                                <option value="seller"
                                    {{isset($response['trade_type']) && $response['trade_type'] === 'seller' ? 'selected' : ''}}>
                                    فروشنده
                                </option>
                            </select>
                        </div>

                        <div class="col-md-3 text-right my-2">
                            <select class="form-control admin-input font-size-14" name="sort" id="sort" style="text-align-last: center">
                                <option value="loss">پر ضررترین ها</option>
                                <option value="profit"
                                    {{isset($response['trade_sort']) && $response['trade_sort'] === 'profit' ? 'selected' : ''}}>
                                    پرسودترین ها
                                </option>
                            </select>
                        </div>




                    </div>

                    <div class="form-row mt-3">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="جستجو" >
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">کد</td>
                <td class="font-size-13">کاربر</td>
                <td class="font-size-13">نوع</td>
                <td class="font-size-13">اهرم</td>
                <td class="font-size-13">واحد</td>
                <td class="font-size-13">مظنه شروع (تومان)</td>
                <td class="font-size-13">مظنه آخرین معامله (تومان)</td>
                <td class="font-size-13">سود/زیان(تومان)</td>
                <td class="font-size-13">حد مجاز ضرر(تومان)</td>
                <td class="font-size-13">تاریخ</td>
            </tr>
            <tbody>
                @foreach($response['trade'] as $row => $trade)
                    <tr class="text-center font-size-13">
                        <td class="admin-danger">{{++$row}}</td>

                        <td class="admin-black direction-ltr">{{$trade->id}}</td>

                        <td class="admin-black d-block font-size-13">
                            {{$trade->Portfo->User->name}}
                        </td>

                        <td>
                            @if($trade->type === 'buyer')
                                <span class="admin-danger">خریدار</span>
                            @else
                                <span class="admin-info">فروشنده</span>
                            @endif
                        </td>
                        <td class="admin-danger d-block font-size-13">
                            {{$trade->Portfo->lever}}
                        </td>
                        <td class="admin-success">{{number_format($trade->weight)}}</td>

                        <td class="admin-black">{{number_format($trade->amount)}}</td>

                        <td class="admin-black">{{number_format($response['trade_last_amount'])}}</td>

                        <td  class="direction-ltr {{$trade->profit >= 0 ? 'user-success' : 'user-danger'}}">
                              {{number_format($trade->profit / $trade->weight) }}
                        </td>

                        <td class="admin-danger">
                                @if($trade->type === 'buyer')
                                    {{number_format($trade->amount - (($trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                                @else
                                    {{number_format($trade->amount + (($trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                                @endif
                        </td>

                        <td class="admin-secondary">{{jdate($trade->created_at)->format('Y/m/d')}}</td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['trade']->appends(\Illuminate\Support\Facades\Request::all())->render()}}

    </div>

@endsection
