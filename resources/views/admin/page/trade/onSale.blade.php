@extends('admin.index')

@section('content')

    @include('message.message')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped ">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13"> کد </td>
                <td class="font-size-13"> کاربر </td>
                <td class="font-size-13">نوع</td>
                <td class="font-size-13" >واحد</td>
                <td class="font-size-13">مظنه شروع (تومان)</td>
                <td class="font-size-13">مظنه حراج (تومان)</td>
                <td class="font-size-13">حد مجاز ضرر(تومان)</td>
                <td class="font-size-13">تاریخ</td>
            </tr>
            <tbody>
                @foreach($response['onsale_trade'] as $row => $onsale_trade)
                    <tr class="text-center font-size-13">
                        <td class="user-danger">{{++$row}}</td>
                        <td class="user-black direction-ltr">{{$onsale_trade->id}}</td>
                        <td class="user-black direction-ltr">
                            <span class="admin-info d-block font-size-13">{{$onsale_trade->Portfo->User->name}}</span>
                            <span class="admin-danger font-size-13">{{$onsale_trade->Portfo->User->mobile}}</span>
                        </td>
                        <td class="user-danger">
                            @if($onsale_trade->type === 'buyer')
                                خریدار
                            @else
                                فروشنده
                            @endif
                        </td>
                        <td class="admin-success">{{number_format($onsale_trade->weight)}}</td>
                        <td class="admin-info">{{number_format($onsale_trade->Trade->amount)}}</td>
                        <td class="admin-black">{{number_format($onsale_trade->amount)}}</td>

                        <td class="admin-danger">
                            @if($onsale_trade->Trade->type === 'buyer')
                                {{number_format($onsale_trade->Trade->amount - (($onsale_trade->Trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                            @else
                                {{number_format($onsale_trade->Trade->amount + (($onsale_trade->Trade->Portfo->lever * 175 * $response['unit']) / 23 ))}}
                            @endif
                        </td>
                        <td class="admin-secondary">{{jdate($onsale_trade->created_at)->format('Y/m/d')}}</td>
                    </tr>
                @endforeach
            {{$response['onsale_trade']->links()}}
            </tbody>
        </table>
    </div>

@endsection
