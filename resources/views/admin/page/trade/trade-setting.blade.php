@extends('admin.index')

@section('content')

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.onlinetradeSetting.update')}}" method="POST"
                  class="form admin-form">
            @csrf



                <div class="form-row rounded p-4">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="trade_start_time" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            ساعت آغاز معاملات
                            <span class="text-danger font-size-13 font-weight-bold fa-num">(0 الی 24)</span>
                        </label>
                        <input type="text" class="form-control  admin-input text-center direction-ltr  @error('trade_start_time') is-invalid @enderror"
                               id="trade_start_time" name="trade_start_time" placeholder="بیشترین حد مجاز اختلاف قیمت وارد شده"
                               value="{{$response['trade_start_time']!='' ? $response['trade_start_time']->value : 10}}">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="trade_end_time" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            ساعت پایان معاملات
                            <span class="text-danger font-size-13 font-weight-bold fa-num">(0 الی 24)</span>
                        </label>
                        <input type="text" class="form-control  admin-input text-center direction-ltr  @error('trade_end_time') is-invalid @enderror"
                               id="trade_end_time" name="trade_end_time" placeholder="بیشترین حد مجاز اختلاف قیمت وارد شده"
                               value="{{$response['trade_end_time']!='' ? $response['trade_end_time']->value : 10}}">
                    </div>
                </div>


                <div class="form-row border-top border-danger rounded p-4">

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="title_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            وضعیت بازار

                        </label>
                        <select class="form-control admin-input font-size-14" name="trade_close" id="trade_close" style="text-align-last: center">
                            <option value="no" {{$response['trade_close']->value === 'no' ?'selected' : ''}}>باز بودن بازار و اجازه خرید و فروش کاربران</option>
                            <option value="yes" {{$response['trade_close']->value === 'yes' ?'selected' : ''}}>تعطیلی بازار</option>
                            <option value="close">تعطیلی بازار به همراه تسویه حساب کلیه پرتفوهای باز</option>
                        </select>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <ul>
                            <li class="admin-info mt-2 font-size-12">
                                جهت
                                <mark>تعطیلی موقت بازار</mark>
                                گزینه دوم یعنی
                                &nbsp;
                                <span class="admin-danger border-danger border-bottom font-size-12">تعطیلی بازار</span>
                                &nbsp;
                                را انتخاب کنید.
                            </li>

                            <li class="admin-info mt-4 line-35 font-size-12">
                                جهت
                                <mark>تعطیلی بازار و بستن کلیه پرتفوهای باز</mark>
                                گزینه سوم یعنی
                                &nbsp;
                                <span class="admin-danger border-danger border-bottom font-size-12">تعطیلی بازار به همراه تسویه حساب کلیه پرتفوهای باز</span>
                                &nbsp;
                                را انتخاب کنید.

                            </li>
                        </ul>
                    </div>

                </div>

                <div class="form-row my-4">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ویرایش اطلاعات">
                </div>

            </form>
        </div>
    </section>

@endsection

