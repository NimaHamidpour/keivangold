@extends('admin.index')

@section('content')

    @include('message.message')

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">تصویر مطلب</th>
                <th class="admin-table-hr">عنوان</th>
                <th class="admin-table-hr">تعداد بازدید کلی</th>
                <th class="admin-table-hr">تعداد بازدید امروز</th>
                <th class="admin-table-hr">ویرایش دسته</th>
            </tr>
            <tbody>
                @foreach($response['pages'] as $page)
                    <tr class="text-center font-size-13">
                        <td>
                            <img src="{{$page->thumbnail}}" class="img-fluid rounded" style="height:50px"></td>
                        <td class="admin-info pt-3">{{$page->title}}</td>
                        <td class="admin-info pt-3">
                            {{\App\Models\Visit::where([['model','Page'],['model_id',$page->id]])->get()->count()}}
                        </td>
                        <td class="admin-info pt-3">
                            {{\App\Models\Visit::where([['model','Page'],['model_id',$page->id]])->whereDate('created_at', \Carbon\Carbon::today())->distinct('ip')->get()->count()}}
                        </td>
                        <td>
                            <a href="{{route('admin.webpage.edit',$page->id)}}"
                               class="btn btn-outline-primary btn-sm admin-a mt-1">
                                ویرایش
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['pages']->links()}}
    </div>

@endsection
