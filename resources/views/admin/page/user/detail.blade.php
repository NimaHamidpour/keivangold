@extends('admin.index')

@section('content')
    @include('message.message')


    <div class="table-responsive admin-div-table my-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">تصویر</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">موبایل</td>
                <td class="font-size-13">کد ملی</td>
                <td class="font-size-13">راه آشنایی</td>
                <td class="font-size-13">تاریخ ثبت نام</td>
            </tr>
            <tbody>
            <tr class="text-center">
                <td>
                    <img
                        src="{{$response['user']->avatar !='' ? url($response['user']->avatar) : url('/app/img/avatar.jpg')}}"
                        class="admin-table-img rounded-circle">
                </td>
                <td class="admin-black pt-4">{{$response['user']->name}}</td>
                <td class="admin-black pt-4"> {{$response['user']->mobile}} </td>
                <td class="admin-black pt-4"> {{$response['user']->codemeli}} </td>
                <td class="admin-info pt-4">
                    @switch($response['user']->friendShip)
                        @case('instagram')
                        اینستاگرام
                        @break
                        @case('kaveh')
                        ایجنت کـاوه
                        @break
                        @case('telegram')
                        تلگرام
                        @break
                        @case('website')
                        وب سایت
                        @break
                        @case('aparat')
                        آپارات
                        @break
                        @case('family')
                        آشنایی قبلی
                        @break
                        @default
                        سایر
                        @break
                    @endswitch
                </td>
                <td class="admin-secondary pt-4">{{jdate($response['user']->created_at)->format('Y/m/d')}}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="table-responsive admin-div-table mt-3 mb-5">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">موجودی (تومان)</td>
                <td class="font-size-13">شماره شبا</td>
                <td class="font-size-13">نام بانک</td>
            </tr>
            <tbody>
            <tr class="text-center">

                <td class="admin-black">{{number_format($response['user']->wallet)}}</td>
                <td class="admin-danger"> {{$response['user']->sheba}} </td>
                <td class="admin-danger"> {{$response['user']->bank}} </td>
            </tr>
            </tbody>
        </table>
    </div>

    <hr/>

    <div class="table-responsive user-div-table my-5">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">مبلغ ضمانت(تومان)</td>
                <td class="font-size-13">اهرم</td>
                <td class="font-size-13">واحد مجاز خرید</td>
                <td class="font-size-13">واحد مجاز فروش</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">سود/زیان(تومان)</td>
                <td class="font-size-13">تاریخ ایجاد</td>
                <td class="font-size-13"> لیست معاملات </td>
            </tr>
            <tbody>
            @foreach($response['portfo'] as $row => $portfo)
                <tr class="text-center font-size-13">
                    <td class="admin-danger">{{++$row}}</td>
                    <td class="user-info">
                        {{number_format($portfo->amount)}}
                    </td>
                    <td class="user-success">{{$portfo->lever}}</td>

                    <td class="user-info">
                        {{$portfo->weight_buy }}
                    </td>

                    <td class="user-danger">
                        {{$portfo->weight_sell }}
                    </td>


                    @if($portfo->end_date === null && in_array($portfo->id,$response['openTrade']))
                        <td class="user-info">
                            باز
                            ({{\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->sum('weight') . ' واحد '}}
                            {{\App\Models\Trade::firstWhere([['portfo',$portfo->id],['status','open']])->type === 'seller' ? 'فروش':'خرید'}})

                        </td>
                    @elseif($portfo->end_date === null && count(\App\Models\Trade::where([['portfo',$portfo->id],['status','open']])->get()) === 0)
                        <td class="user-danger">باز بدون معامله در جریان</td>
                    @else
                        <td class="user-black">بسته</td>
                    @endif

                    <td class="{{$portfo->profit >= 0 ? 'user-success' : 'user-danger'}}">
                        <div class="direction-ltr d-inline-block fa-num">
                            {{number_format($portfo->profit)}}
                        </div>
                    </td>



                    <td class="user-secondary">
                        {{jdate($portfo->created_at)->format('Y/m/d') }}
                    </td>

                    <td>
                        <a href="{{route('admin.portfo.show',$portfo->id)}}"
                           class="btn btn-outline-primary btn-sm font-size-13">
                            مشاهده
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['portfo']->appends(\Illuminate\Support\Facades\Request::all())->render()}}
    </div>

    <hr/>

    <div class="table-responsive admin-div-table my-5">
        <table class="table table-striped ">
            <tr class="admin-table-hr-row">
                <td class="font-size-13"> شماره تیکت</td>
                <td class="font-size-13">اطلاعات کاربری</td>
                <td class="font-size-13">نام کاربری</td>
                <td class="font-size-13">موضوع پیام</td>
                <td class="font-size-13">وضعیت</td>
                <td class="font-size-13">تاریخ</td>
                <td class="font-size-13">مشاهده</td>

            </tr>
            <tbody>
            @foreach($response['ticket'] as $ticket)
                <tr class="text-center font-size-13">
                    <td class="admin-black direction-ltr">{{$ticket->id}}</td>
                    <td>
                        <span class="admin-danger direction-ltr d-block">{{$ticket->User->email}}</span>
                        <span class="admin-info direction-ltr">{{$ticket->User->mobile}}</span>
                    </td>
                    <td class="admin-black direction-ltr">{{$ticket->User->name}}</td>
                    <td class="admin-black direction-ltr">{{$ticket->title}}</td>
                    <td class="admin-black">
                        @if($ticket->status==='admin-answer')
                            <span class="text-danger">در انتظار پاسخ مدیر</span>
                        @elseif($ticket->status==='user-answer')
                            <span class="text-info">در انتظار پاسخ کاربر</span>
                        @else
                            <span class="text-success">پایان یافته</span>
                        @endif

                    </td>
                    <td class="admin-info">{{jdate($ticket->created_at)->format('Y/m/d')}}</td>
                    <td>
                        <a href="{{route('admin.ticket.show',$ticket->id)}}" class="btn btn-info pt-1 px-2 btn-sm">
                            مشاهده
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['ticket']->links()}}
    </div>


@endsection
