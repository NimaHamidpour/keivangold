@extends('admin.index')

@section('content')

    <div class="table-responsive user-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="user-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">مبلغ (تومان)</td>
                <td class="font-size-13">بابت</td>
                <td class="font-size-13">جزئیات</td>
                <td class="font-size-13">تاریخ</td>
            </tr>
            <tbody>
            @foreach($response['history'] as $row => $history)
                <tr class="text-center font-size-13">

                    <td class="admin-danger">{{++$row}}</td>
                    <td class="user-info direction-ltr">
                        {{number_format($history->amount)}}
                    </td>
                    <td>
                        @if($history->income === 'yes')
                            <span class="user-success">ورود به کیف پول</span>
                        @else
                            <span class="user-danger">کسر از کیف پول</span>
                        @endif
                    </td>

                    <td class="user-info">
                        @if($history->model === 'Portfo')
                           معاملات لحظه ای
                        @elseif($history->model === 'Pay')
                            شارژ کیف پول
                        @elseif($history->model === 'Withdrawal')
                            برداشت از کیف پول
                        @endif
                    </td>
                    <td class="user-secondary direction-ltr">{{jdate($history->created_at)->format('Y/m/d - H:i')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['history']->links()}}
    </div>

@endsection
