@extends('admin.index')

@section('content')

    @include('message.message')

    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">موبایل</td>
                <td class="font-size-13">راه آشنایی</td>
                <td class="font-size-13">کیف پول (تومان)</td>
                <td class="font-size-13">ویرایش وضعیت</td>
            </tr>
            <tbody>
                @foreach($response['users'] as $row => $user)

                    <tr class="text-center">
                        <td class="admin-danger">{{++$row}}</td>
                        <td class="admin-black">{{$user->name}}</td>
                        <td class="admin-info d-block">
                            {{$user->mobile}}
                        </td>

                        <td class="admin-danger">
                            @switch($user->friendShip)
                                @case('instagram')
                                    اینستاگرام
                                @break
                                @case('kaveh')
                                    ایجنت کـاوه
                                @break
                                @case('telegram')
                                    تلگرام
                                @break
                                @case('website')
                                    وب سایت
                                @break
                                @case('aparat')
                                    آپارات
                                @break
                                @case('family')
                                    آشنایی قبلی
                                @break
                                @default
                                    سایر
                                @break
                            @endswitch
                        </td>

                        <td class="admin-success">{{number_format($user->wallet)}}</td>




                        <td>
                            <a class="btn btn-outline-info btn-sm font-weight-bold text-decoration-none font-size-12"
                               href="{{route('admin.user.active.edit',$user->id)}}">
                                ویرایش وضعیت
                            </a>
                        </td>



                    </tr>
                @endforeach
            </tbody>
        </table>


        {{$response['users']->appends(\Illuminate\Support\Facades\Request::all())->render()}}
    </div>

@endsection
