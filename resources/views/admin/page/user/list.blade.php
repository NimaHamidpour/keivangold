@extends('admin.index')

@section('content')
    @include('message.message')

    <!-- SEARCH -->
    <div class="container mb-4">
        <div class="row px-3">
            <div class="admin-form col-12 mx-auto">
                <form class="form direction-rtl" method="post" action="{{route('admin.user.list')}}">
                   @csrf

                    <div class="form-row">

                        <div class="col-md-4 text-right">
                            <input class="form-control font-yekan font-size-14"
                                   name="name" placeholder="نام کاربری"  autocomplete="off"
                                   value="{{isset($response['user_name']) ? $response['user_name'] :''}}">
                        </div>

                        <div class="col-md-4 text-right">
                            <input class="form-control font-yekan font-size-14" autocomplete="off"
                                   name="mobile" placeholder="موبایل"
                                   value="{{isset($response['user_mobile']) ? $response['user_mobile'] :''}}">
                        </div>

                        <div class="col-md-4 text-right">
                            <select class="form-control admin-input"  style="text-align-last: center"
                                    name="friendShip" id="friendShip">
                                <option value="" hidden>راه آشنایی</option>
                                <option value="website" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'website' ? 'selected' : ''}}>وب سایت</option>
                                <option value="telegram" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'telegram' ? 'selected' : ''}}>تلگرام</option>
                                <option value="instagram" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'instagram' ? 'selected' : ''}}>اینستاگرام</option>
                                <option value="kaveh" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'kaveh' ? 'selected' : ''}}>ایجنت کـاوه</option>
                                <option value="aparat" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'aparat' ? 'selected' : ''}}>آپارات</option>
                                <option value="family" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'family' ? 'selected' : ''}}>آشنایی قبلی</option>
                                <option value="other" {{isset($response['user_friendShip']) && $response['user_friendShip'] === 'other' ? 'selected' : ''}}>سایر</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-row mt-5">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="جستجو" >
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">موبایل</td>
                <td class="font-size-13">راه آشنایی</td>
                <td class="font-size-13">کیف پول (تومان)</td>
                <td class="font-size-13">شارژ کیف پول</td>
                <td class="font-size-13">تراکنش</td>
                <td class="font-size-13">جزئیات</td>
                <td class="font-size-13">حذف</td>
            </tr>
            <tbody>
                @foreach($response['users'] as $row => $user)
                    <tr class="text-center">
                        <td class="admin-danger">{{++$row}}</td>
                        <td class="admin-black">{{$user->name}}</td>
                        <td class="admin-info d-block">
                            {{$user->mobile}}
                        </td>

                        <td class="admin-danger">
                            @switch($user->friendShip)
                                @case('instagram')
                                    اینستاگرام
                                @break
                                @case('kaveh')
                                    ایجنت کـاوه
                                @break
                                @case('telegram')
                                    تلگرام
                                @break
                                @case('website')
                                    وب سایت
                                @break
                                @case('aparat')
                                    آپارات
                                @break
                                @case('family')
                                    آشنایی قبلی
                                @break
                                @default
                                    سایر
                                @break
                            @endswitch
                        </td>

                        <td class="admin-success">{{number_format($user->wallet)}}</td>

                        <td>
                            <a class="btn btn-outline-primary btn-sm font-weight-bold text-decoration-none font-size-12"
                               href="{{route('admin.user.wallet.create',$user->id)}}">
                                شارژ
                            </a>
                        </td>

                        <td>
                            <a class="btn btn-outline-success btn-sm font-weight-bold text-decoration-none font-size-12"
                               href="{{route('admin.user.history',$user->id)}}">
                                تراکنش
                            </a>
                        </td>


                        <td>
                            <a class="btn btn-outline-info btn-sm font-weight-bold text-decoration-none font-size-12"
                               href="{{route('admin.user.show',$user->id)}}">
                                جزئیات
                            </a>
                        </td>

                        <td>
                            <a class="btn btn-outline-danger btn-sm font-weight-bold text-decoration-none font-size-12"
                               href="{{route('admin.user.destroy',$user->id)}}">
                                حذف
                            </a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>


        {{$response['users']->appends(\Illuminate\Support\Facades\Request::all())->render()}}
    </div>

    <div class="row text-left direction-ltr">
        <a  href="{{route('admin.user.excell')}}"
            class="btn btn-info col-md-3 btn-sm mx-5">
            <i class="fa fa-download"></i>
            خروجـی اکـسل
        </a>

    </div>
@endsection
