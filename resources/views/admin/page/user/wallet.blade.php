@extends('admin.index')

@section('content')

        <div class="container">

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-right">
                    @include('message.message')
                    <form  action="{{route('admin.user.wallet.store')}}" method="POST" class="form my-3 admin-form">
                        @csrf

                        <input type="hidden" name="user" value="{{$response['user']->id}}">

                        <div class="form-row mt-3">
                            <div class="col-md-6  mb-4">
                                <label for="amount-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                                    مبلغ قابل شارژ
                                    <span class="user-success">(لطفا مبلغ شارژ را وارد کنید)</span>
                                </label>
                                <input type="text" class="form-control text-right user-input @error('amount') is-invalid @enderror"
                                       id="amount-id" name="amount" placeholder="5000000" autocomplete="off"
                                        onkeyup="digitGroup(this,'wallet-amount','تومان')">
                                <div class="text-danger font-size-12 mt-2 font-weight-bold text-left fa-num" id="wallet-amount">

                                </div>
                            </div>
                        </div>

                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="شارژ کیف پول">
                        </div>

                    </form>
                </div>
            </div>

        </div>

@endsection
