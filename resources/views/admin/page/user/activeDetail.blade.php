@extends('admin.index')

@section('content')
    @include('message.message')


    <div class="table-responsive admin-div-table my-3">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">تصویر</td>
                <td class="font-size-13">کارت ملی</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">موبایل</td>
                <td class="font-size-13">کد ملی</td>
                <td class="font-size-13">راه آشنایی</td>
                <td class="font-size-13">تاریخ ثبت نام</td>
            </tr>
            <tbody>
            <tr class="text-center">
                <td>
                    <img
                        src="{{$response['user']->avatar !='' ? url($response['user']->avatar) : url('/app/img/avatar.jpg')}}"
                        class="admin-table-img rounded-circle">
                </td>

                <td>
                    <a href="{{$response['user']->kartmeli !='' ? url($response['user']->kartmeli) : '#'}}">
                        <img
                            src="{{$response['user']->kartmeli !='' ? url($response['user']->kartmeli) : url('/app/img/avatar.jpg')}}"
                            class="admin-table-img rounded-circle">
                    </a>
                </td>

                <td class="admin-black pt-4">{{$response['user']->name}}</td>
                <td class="admin-black pt-4"> {{$response['user']->mobile}} </td>
                <td class="admin-black pt-4"> {{$response['user']->codemeli}} </td>
                <td class="admin-info pt-4">
                    @switch($response['user']->friendShip)
                        @case('instagram')
                        اینستاگرام
                        @break
                        @case('kaveh')
                        ایجنت کـاوه
                        @break
                        @case('telegram')
                        تلگرام
                        @break
                        @case('website')
                        وب سایت
                        @break
                        @case('aparat')
                        آپارات
                        @break
                        @case('family')
                        آشنایی قبلی
                        @break
                        @default
                        سایر
                        @break
                    @endswitch
                </td>
                <td class="admin-secondary pt-4">{{jdate($response['user']->created_at)->format('Y/m/d')}}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="table-responsive admin-div-table mt-3 mb-5">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">موجودی (تومان)</td>
                <td class="font-size-13">شماره شبا</td>
                <td class="font-size-13">نام بانک</td>
            </tr>
            <tbody>
            <tr class="text-center">

                <td class="admin-black">{{number_format($response['user']->wallet)}}</td>
                <td class="admin-danger"> {{$response['user']->sheba}} </td>
                <td class="admin-danger"> {{$response['user']->bank}} </td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-12">
                <form class="admin-form" action="{{route('admin.user.active.update',$response['user']->id)}}" method="post">
                   @csrf
                    <div class="form-row">
                        <div class="col-md-6 mx-auto">
                            <label class="admin-input-label font-size-14" for="active">انتخاب وضعیت کاربر</label>
                            <select name="active" id="active"
                                    class="form-control admin-input font-size-14" style="text-align-last: center">
                                <option value="yes" {{$response['user']->active === 'active' ? 'selected' : ''}}>تایید کاربر</option>
                                <option value="waiting" {{$response['user']->active === 'waiting' ? 'selected' : ''}}>لیست انتظار</option>
                                <option value="no" {{$response['user']->active === 'no' ? 'selected' : ''}}>رد کاربر</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row mt-5">
                        <input type="submit" class="btn col-md-4 admin-submit font-weight-normal" value="تایید وضعیت">
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
