@extends('admin.index')

@section('content')
    <section class=" direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.category.update',$response['category']->id)}}" method="POST" enctype="multipart/form-data"
                   class="form admin-form">
                @csrf

                <div class="form-row mb-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="bold d-block">انتخاب ایکــون</span>
                                <img src="{{$response['category']->icon!='' ? url($response['category']->icon) : url('/app/img/image-add-button.png')}}" id="slideshowfileimg"
                                     class="img-thumbnail mt-2"
                                     height="100px" width="100px" style="border: 1px dashed #cecece"/>
                            </label>
                            <input type="file" name="icon" id="photo" onchange="readURL(this)"
                                   class="d-none">

                            <div class="d-inline-block" id="preview-img"></div>
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="title_id" class="font-size-15 bold"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته </label>
                        <input type="text" class="form-control  fa-num @error('title') is-invalid @enderror" id="title_id" name="title" placeholder="نام دسته" value="{{$response['category']->title}}">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="type_id" class="font-size-15 bold"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;دسته والد مورد نظر را انتخاب کنید</label>

                        <select id="type_id" name="type" class="form-control text-right font-iran"
                                {{$response['category']->type==='product' ? 'readonly' : ''}}>

                           <option  value="blog"
                               {{$response['category']->type==='blog' ? 'selected' : ''}}>
                               بلاگ
                           </option>

                           <option  value="analysis"
                               {{$response['category']->type==='question' ? 'selected' : ''}}>
                                سوالات متداول
                           </option>

                            <!-- PRODUCT CATEGRORY -->
                            @if($response['category']->type==='product')
                                 <option  value="product"
                                     {{$response['category']->type==='product' ? 'selected' : ''}}>
                                     محصولات
                                 </option>
                            @endif

                        </select>
                    </div>
                </div>

                @if($response['category']->type === 'product')
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="description" class="font-size-15 bold"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته </label>
                        <textarea class="form-control admin-input content" id="description" name="description">{{$response['category']->content}}</textarea>
                    </div>
                </div>
                @endif

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit mx-auto " value="ویرایش " >
                </div>

            </form>
        </div>
    </section>
@endsection

@section('script')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        tiny();
        // **** TINY MCE
        function tiny() {
            var editor_config = {
                path_absolute : "/",
                selector: "textarea.content",
                directionality:'rtl',
                height:'350px',
                plugins: [
                    "advlist autolink lists  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],//image media link
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };
            tinymce.init(editor_config);
        }
    </script>
@endsection
