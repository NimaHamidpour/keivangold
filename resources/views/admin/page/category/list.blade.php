@extends('admin.index')

@section('content')
    @include('message.message')

    <div class="table-responsive admin-div-table">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <th class="admin-table-hr">ایکون</th>
                <th class="admin-table-hr">نام</th>
                <th class="admin-table-hr">ویرایش دسته</th>
                <th class="admin-table-hr">حذف دسته</th>
            </tr>
            <tbody>
                @foreach($response['categories'] as $category)
                    <tr class="text-center">
                        <td>
                            <img src="{{$category->icon!='' ? url($category->icon) : url('/front/img/structure/image-add-button.png')}}" class="admin-table-img">
                        </td>
                        <td class="admin-black">{{$category->title}}</td>
                        <td>
                            <a href="{{route('admin.category.edit',$category->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                        </td>
                        <td>
                            <a href="{{route('admin.category.destroy',$category->id)}}" class="btn btn-outline-danger btn-sm admin-a">
                                حذف
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['categories']->links()}}
    </div>

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            <h1 class="admin-h1">ثبت دسته جدید</h1>
            <form  action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data"
                   class="form admin-form">
                @csrf

                <div class="form-row mb-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب ایکــون</span>
                                <img src="{{url('/app/img/image-add-button.png')}}" id="photo-img"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="icon" id="photo" onchange="readURL(this);"
                                   accept="image/x-png,image/gif,image/jpeg" class="d-none">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="title_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته جدید</label>
                        <input type="text" class="form-control admin-input @error('title') is-invalid @enderror" id="title_id" name="title" placeholder="نام دسته" value="{{old('title')}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="type" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته جدید</label>
                        <select class="form-control admin-input" id="type" name="type">
                            <option class="admin-input" value="blog" {{ $response['type']==='blog' ? 'selected' :''}}>بلاگ</option>
                            <option class="admin-input" value="product" {{ $response['type']==='product' ? 'selected' :''}}>محصولات</option>
                            <option class="admin-input" value="question" {{ $response['type']==='question' ? 'selected' :''}}>سوالات متداول</option>
                        </select>
                    </div>
                </div>
                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="افزودن" >
                </div>
            </form>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
