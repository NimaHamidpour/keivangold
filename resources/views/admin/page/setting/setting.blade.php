@extends('admin.index')

@section('content')

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.setting.update')}}" method="POST" enctype="multipart/form-data"
                  class="form admin-form">
            @csrf

                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{isset($response['app_logo']) ? url( $response['app_logo']): url('/front/img/structure/image-add-button.png') }}" id="slideshowfileimg"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="photo" id="photo" onchange="readURL(this);"
                                   class="d-none" value="{{isset($response['app_logo']) ? url( $response['app_logo']) : '' }}">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="title_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;عنوان سایت</label>
                        <input type="text" class="form-control  admin-input @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder="عنوان سایت"
                               value="{{$response['app_name']!='' ? $response['app_name'] : ''}}">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="issue_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;موضوع سایت</label>
                        <input type="text" class="form-control  admin-input @error('issue') is-invalid @enderror"
                               id="issue_id" name="issue" placeholder="موضوع سایت"
                               value="{{$response['app_issue']!='' ? $response['app_issue'] : ''}}">
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-md-6 mb-4">
                        <label for="tell" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;موبایل<span class="font-size-14 text-danger"> (با - از هم جدا کنید) </span></label>
                        <input type="text" class="form-control  admin-input @error('app_tell') is-invalid @enderror"
                               id="tell" name="app_tell" placeholder="موبایل"
                               value="{{$response['app_tell']!='' ? $response['app_tell'] : ''}}">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="email_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;ایمیل<span class="font-size-14 text-danger"> (با - از هم جدا کنید) </span></label>
                        <input type="text" class="form-control  admin-input @error('app_email') is-invalid @enderror"
                               id="email_id" name="app_email" placeholder="ایمیل"
                               value="{{$response['app_email']!='' ? $response['app_email'] : ''}}">
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-12 mb-4">
                        <label for="address" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;آدرس</label>
                        <input type="text" class="form-control  admin-input @error('address') is-invalid @enderror"
                               id="address" name="address" placeholder="آدرس"
                               value="{{$response['app_address']!='' ? $response['app_address'] : ''}}">
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="short_text" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp; توضیحات کوتاه </label>
                        <textarea class="form-control admin-input p-2" id="short_text"  style="height: 150px"
                                  name="short_text" placeholder="درباره ما">{{$response['app_short_text']!='' ? $response['app_short_text'] : ''}}</textarea>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="description_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp; توضیحات بیشتر</label>
                        <textarea class="form-control admin-input-label p-2 content" id="description_id" name="description" placeholder="درباره ما">{{$response['app_description']!='' ? $response['app_description'] : ''}}</textarea>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ثبت اطلاعات">
                </div>

            </form>
        </div>
    </section>

@endsection

@section('script')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>

        tiny();


        // **** TINY MCE
        function tiny() {
            var editor_config = {
                path_absolute : "/",
                selector: "textarea.content",
                directionality:'rtl',
                height:'350px',
                plugins: [
                    "advlist autolink lists  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],//image media link
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };
            tinymce.init(editor_config);
        }



        // **** PRIVIEW IMAGE
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection
