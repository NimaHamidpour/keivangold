<!-- Sidebar -->
<ul class="sidebar navbar-nav p-1 m-0 sidebar-width sidebar-background">


    <!-- ADMIN INFORMATION -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width pr-0 " href="#" id="pagesDropdown"
           role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-angle-down float-left mt-3"></i>

            <img
                src="{{isset(\Illuminate\Support\Facades\Auth::user()->avatar) && \Illuminate\Support\Facades\Auth::user()->avatar !=''? url(\Illuminate\Support\Facades\Auth::user()->avatar) : url('app/img/avatar.jpg')}}"
                style="height: 40px"
                class="rounded-circle mx-2">

            <span class="font-yekan font-size-15 text-black font-weight-bolder">
                     {{\Illuminate\Support\Facades\Auth::user()->name }}
            </span>

            <span class="font-size-12 text-muted font-weight-bold mr-4 mt-2 d-none d-xl-block d-lg-block">
                     {{\Illuminate\Support\Facades\Auth::user()->email }}
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.admin.edit')}}" class="nav-link text-right text-dark">
                <i class="fas fa-user text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">ویرایش مشخصات</span>
            </a>

            <a href="{{route('admin.admin.edit.password')}}" class="nav-link text-right text-dark">
                <i class="fas fa-lock text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">تغییر کلمه عبور</span>
            </a>
        </div>
    </li>


    <!-- DASHBOARD -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-dark menu-padding sidebar-width" href="{{route('admin.dashboard')}}">
            <i class="fas fa-home text-gold"></i>
            <span class="font-yekan font-size-12 font-weight-bold">داشبورد</span>
        </a>
    </li>


    <!-- ONLINE TRADE  -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_portfo') or
        \Illuminate\Support\Facades\Auth::user()->hasPermission('permission_trade') or
        \Illuminate\Support\Facades\Auth::user()->hasPermission('permission_onSale') or
        \Illuminate\Support\Facades\Auth::user()->hasPermission('permission_trade_setting'))

        <li class="nav-item border-bottom">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-trademark text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">
                 خرید و فروش نقدی
                <i class="fas fa-angle-down float-left mt-2"></i>
            </span>
            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_portfo'))
                    <a href="{{route('admin.portfo.list')}}" class="nav-link text-right text-dark">
                        <i class="fas fa-list text-gold"></i>
                        <span class="font-yekan font-size-12 font-weight-bold">لیست پرتفوها</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_trade'))
                    <a href="{{route('admin.trade.list')}}" class="nav-link text-right text-dark">
                        <i class="fas fa-door-open text-gold"></i>
                        <span class="font-yekan font-size-12 font-weight-bold">معاملات باز</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_onSale'))
                    <a href="{{route('admin.trade.onsale')}}" class="nav-link text-right text-dark">
                        <i class="fas fa-store text-gold"></i>
                        <span class="font-yekan font-size-12 font-weight-bold">معاملات حراجی</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_trade_setting'))
                    <a href="{{route('admin.onlinetradeSetting.amount.edit')}}" class="nav-link text-right text-dark">
                        <i class="fa fa-cog text-gold"></i>
                        <span class="font-yekan font-size-12 font-weight-bold">تنظیمات قیمت ها</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_trade_setting'))
                    <a href="{{route('admin.onlinetradeSetting.trade.edit')}}" class="nav-link text-right text-dark">
                        <i class="fa fa-cog text-gold"></i>
                        <span class="font-yekan font-size-12 font-weight-bold">تنظیمات بازار</span>
                    </a>
                @endif
            </div>
        </li>
    @endif


<!-- USER -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_user'))
        <li class="nav-item border-bottom">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"> کاربران <i
                        class="fas fa-angle-down float-left mt-2"></i></span>
            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
                <a href="{{route('admin.user.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-layer-group text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست کاربران </span>
                </a>

                <a href="{{route('admin.user.active.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-check text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">تایید کاربران</span>
                </a>
            </div>
        </li>
    @endif


<!-- PAY -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_pay_transection') or \Illuminate\Support\Facades\Auth::user()->hasPermission('permission_pay_info'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-dollar-sign text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"> امور مالی <i
                        class="fas fa-angle-down float-left mt-2"></i></span>
            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">


                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_pay_info'))
                    <a href="{{route('admin.pay.info')}}" class="nav-link text-right text-dark">
                        <i class="fa fa-chart-bar text-gold"></i>
                        <span class="text-dark font-size-12 font-yekan font-weight-bold">اطلاعات آماری</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_withdrawal'))
                    <a href="{{route('admin.pay.withdrawal.list')}}" class="nav-link text-right text-dark">
                        <i class="fa fa-money-check text-gold"></i>
                        <span class="text-dark font-size-12 font-yekan font-weight-bold">تسویه حساب کاربران</span>
                    </a>
                @endif

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_pay_transection'))
                    <a href="{{route('admin.pay.list','paid')}}" class="nav-link text-right text-dark">
                        <i class="fas fa-check text-gold"></i>
                        <span class="text-dark font-size-12 font-yekan font-weight-bold">پرداخت شده</span>
                    </a>
                    <a href="{{route('admin.pay.list','unpaid')}}" class="nav-link text-right text-dark">
                        <i class="fa fa-times text-gold"></i>
                        <span class="text-dark font-size-12 font-yekan font-weight-bold">پرداخت نشده</span>
                    </a>
                @endif

            </div>
        </li>
    @endif


<!-- Notification -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_notification'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">
                    اطلاعیه ها
                <i class="fas fa-angle-down float-left mt-2"></i>
            </span>

            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

                <a href="{{route('admin.notification.add')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-plus text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">ایجاد اطلاعیه جدید</span>
                </a>
                <a href="{{route('admin.notification.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست اطلاعیه ها</span>
                </a>

            </div>
        </li>
    @endif


<!-- TICKET -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_ticket'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">
                    درخواست ها
                <i class="fas fa-angle-down float-left mt-2"></i>
            </span>

            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

                <a href="{{route('admin.ticket.add')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-mail-bulk text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">ایجاد گفتگو جدید</span>
                </a>
                <a href="{{route('admin.ticket.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-mail-bulk text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست درخواست ها</span>
                </a>

            </div>
        </li>
    @endif


<!-- PORTFOLIO -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_portfolio'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-store text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"> نمونه کارها <i
                        class="fas fa-angle-down float-left mt-2"></i></span>
            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
                <a href="{{route('admin.product.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست نمونه کارا</span>
                </a>

                <a href="{{route('admin.category.list','product')}}" class="nav-link text-right text-dark">
                    <i class="fa fa-layer-group text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">افزودن دسته جدید</span>
                </a>

            </div>
        </li>
    @endif


<!-- BLOG -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_writer'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-blog text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"> بلاگ <i
                        class="fas fa-angle-down float-left mt-2"></i></span>
            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
                <a href="{{route('admin.blog.list','blog')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست بلاگ</span>
                </a>
                <a href="{{route('admin.blog.create','blog')}}" class="nav-link text-right text-dark">
                    <i class="fa fa-plus text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">اضافه کردن بلاگ جدید</span>
                </a>

                <a href="{{route('admin.category.list','blog')}}" class="nav-link text-right text-dark">
                    <i class="fa fa-layer-group text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">افزودن دسته جدید</span>
                </a>

            </div>
        </li>
    @endif


<!-- WEBSITE PAGE -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_writer'))
        <li class="nav-item border-bottom">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="{{route('admin.webpage.list')}}">
                <i class="fab fa-chrome text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">صفحات وب سایت</span>
            </a>
        </li>
    @endif

<!-- SEO -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_writer'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fab fa-google text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"> سئو <i
                        class="fas fa-angle-down float-left mt-2"></i></span>

            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
                <a href="{{route('admin.seo.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">لیست صفحات</span>
                </a>


            </div>
        </li>
    @endif

<!-- QUESTION -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_writer'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-question text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold">
                    <i class="fas fa-angle-down float-left mt-2"></i>
                    سوالات متداول
                </span>

            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
                <a href="{{route('admin.question.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span class="text-dark font-size-12 font-family-yekan font-weight-bold">لیست سوالات</span>
                </a>

                <a href="{{route('admin.question.create')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-plus text-gold"></i>
                    <span class="text-dark font-size-12 font-family-yekan font-weight-bold">افزودن سوال جدید</span>
                </a>

                <a href="{{route('admin.category.list','question')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-list text-gold"></i>
                    <span
                        class="text-dark font-size-12 font-family-yekan font-weight-bold"> دسته بندی سوالات متداول</span>
                </a>
            </div>
        </li>
    @endif

<!-- MANAGE ADMINS -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_manager_admin'))
        <li class="nav-item border-bottom">
            <a class="nav-link text-right text-dark menu-padding sidebar-width position-relative" href="#"
               id="pagesDropdown" role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-shield text-gold"></i>
                <span class="font-family-yekan font-size-12 font-weight-bold">
                   مدیریت ادمین
                <i class="fas fa-angle-down arrow-left mt-2 text-admin"></i>
            </span>

            </a>
            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

                <a href="{{route('admin.manager.list')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-layer-group text-gold"></i>
                    <span class="text-dark font-size-12 font-family-yekan font-weight-bold">لیست ادمین ها </span>
                </a>

                <a href="{{route('admin.manager.create')}}" class="nav-link text-right text-dark">
                    <i class="fas fa-plus text-gold"></i>
                    <span class="text-dark font-size-12 font-family-yekan font-weight-bold">ثبت  ادمین جدید </span>
                </a>
            </div>
        </li>
    @endif


<!-- SETTING -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('permission_setting'))
        <li class="nav-item border-bottom pb-1">
            <a class="nav-link text-right text-dark menu-padding sidebar-width" href="#" id="pagesDropdown"
               role="button"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-cog text-gold"></i>
                <span class="font-yekan font-size-12 font-weight-bold"><i class="fas fa-angle-down float-left mt-2"></i>تنظیمات</span>

            </a>

            <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

                <a href="{{route('admin.setting.edit')}}" class="nav-link text-right text-dark">
                    <i class="fa fa-mobile text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">تنظیمات</span>
                </a>
                <a class="nav-link text-right text-dark" href="{{route('admin.social.list')}}">
                    <i class="fab fa-instagram text-gold"></i>
                    <span class="text-dark font-size-12 font-yekan font-weight-bold">شبکه های اجتماعی</span>
                </a>

            </div>
        </li>
    @endif
</ul>

<!-- Sidebar -->


