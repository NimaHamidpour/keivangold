<nav class="navbar navbar-expand navbar-dark static-top direction-rtl navbar-background">

    <a class="navbar-brand mr-1 font-size-17  font-yekan" href="{{Request::root()}}"> {{$response['app_name']}}</a>&nbsp;&nbsp;

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0 mt-2" id="sidebarToggle">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar -->
    <ul class="navbar-nav mr-auto ml-md-0 mt-2">
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle"
                       href="{{route('admin.ticket.list')}}">
                        <i class="fas fa-envelope fa-fw text-white"></i>
                        <span class="message message-danger fa-num font-weight-bold" style="font-size: 9px">
                            {{\App\Models\Ticket::where('status','admin-answer')->get()->count()}}
                        </span>
                    </a>
                </li>

        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-sign-out-alt text-white"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
                <a class="dropdown-item text-center" href="#" data-toggle="modal" data-target="#logoutModal">خروج</a>
            </div>
        </li>
    </ul>

</nav>
<div id="mainNav"></div>
